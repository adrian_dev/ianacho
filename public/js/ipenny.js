var $keywords = new Array();
var $suppliers = new Array();
var $tags = new Array();

$(document).foundation();

$(function() {
  $('.favbutton ').click(function() {
    $(this).toggleClass('unfav fav');
  });

  $('#vendorblock .thumb img').on('load', function() {
    $(this).css('margin', Math.floor((220 - $(this).height()) / 2)+'px 0px');
  });

  $('.grid a').on('click', function(e) {
    $('#prod-grid').toggleClass("listed unlisted"); //you can list several class names
    $(this).toggleClass("listed"); //you can list several class names
    e.preventDefault();
  });

  $('input[type=checkbox]').change(function() {
    $('#archive-filter').submit();
  });

  $('#sort-dropdown ul li a').click(function() {
    $('#sort-results').val($(this).data('sort'));
    $('#archive-filter').submit();
  });

  $('input[name=green_mark]').change(function() {
    $('#archive-filter').submit();
  });

  $('input[name=lead_time]').change(function() {
    $('#lead-time').val($(this).val());
    $('#archive-filter').submit();
  });

  $('.fav').click(function() {
    $.ajax({
      url: $('base').attr('href')+'/api/favorites/unset/'+$(this).data('id'),
      success: function(response) {
        if (response.hasOwnProperty('error')) {
          console.log(response.error);
        } else {
          getMyProducts();
        }
      }
    });
  });

  $('.unfav').click(function() {
    $.ajax({
      url: $('base').attr('href')+'/api/favorites/set/'+$(this).data('id'),
      success: function(response) {
        if (response.hasOwnProperty('error')) {
          console.log(response.error);
        } else {
          getMyProducts();
        }
      }
    });

  });

  $('.collection a').click(function() {
    var elem = $(this);

    if (elem.find('i').hasClass('fa-plus')) {
      var link = $('base').attr('href')+'/api/requests/set/'+elem.data('id');
      elem.find('i').removeClass('fa-plus').addClass('fa-minus');
    } else {
      var link = $('base').attr('href')+'/api/requests/unset/'+elem.data('id');
      elem.find('i').removeClass('fa-minus').addClass('fa-plus');
    }

    $.ajax({
      url: link,
      success: function(response) {
        if (response.hasOwnProperty('error')) {
          console.log(response.error);
        } else {
          getMyRequests();
        }
      }
    });
});

  $('#toggler a').click(function() {
    var i = $(this).find('i');

    if (i.hasClass('fa-chevron-down')) {
      $('#filters').slideDown();
      i.removeClass('fa-chevron-down');
      i.addClass('fa-chevron-up');
    } else {
      $('#filters').slideUp();
      i.removeClass('fa-chevron-up');
      i.addClass('fa-chevron-down');
    }
  });
});

function toggleFavorite(e) {
  var elem = $(e);
  if (elem.parent().hasClass('on')) {
    var link = $('base').attr('href')+'/api/favorites/unset/'+elem.data('id');
  } else {
    var link = $('base').attr('href')+'/api/favorites/set/'+elem.data('id');
  }

  $.ajax({
    url: link,
    success: function(response) {
      if (response.hasOwnProperty('error')) {
        console.log(response.error);
      } else {
        elem.parent().toggleClass('on');
        getMyProducts();
      }
    }
  });
}

function unFavorite($id) {
  $.ajax({
    url: $('base').attr('href')+'/api/favorites/unset/'+$id,
    success: function(response) {
      if (response.hasOwnProperty('error')) {
        console.log(response.error);
      } else {
        $("#my-collection #product-"+$id).remove();
      }
    }
  });
}

function getMyProducts() {
  $.ajax({
    url: $('base').attr('href')+'/api/favorites/products',
    success: function(response) {
      if (response.hasOwnProperty('error')) {
        console.log(response.error);
      } else {
        $('#collection-kit').html('');

        $.each(response, function(i, item) {
          $('#collection-kit').append('<div class="column">'+
            '<div class="card">'+
              '<div class="thumb">'+
                '<a href="'+$('base').attr('href')+'/product/'+item.id+'">'+
                  '<img src="'+$('base').attr('href')+'/'+item.thumbnail+'" alt="'+item.name+'" />'+
                '</a>'+
              '</div>'+
            '</div>'+
          '</div>');
        });
      }
    }
  });
}

function getMyRequests() {
  $.ajax({
    url: $('base').attr('href')+'/api/requests/products',
    success: function(response) {
      if (response.hasOwnProperty('error')) {
        console.log(response.error);
      } else {
        $('#request-kit').html('');
console.log(response);
        $.each(response, function($i, $product) {
          $('#request-kit').append('<div class="column">'+
            '<div class="card">'+
              '<div class="thumb">'+
                '<a href="'+$('base').attr('href')+'/product/'+$product.id+'">'+
                  '<img src="'+$('base').attr('href')+'/'+$product.thumbnail+'" />'+
                '</a>'+
              '</div>'+
            '</div>'+
          '</div>');
        });
      }
    }
  });
}
