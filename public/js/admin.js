$(document).foundation();

function addContactForm() {
  var contacts = $(".contacts");
  contacts.append('<div class="column large-6">'+
    '<div class="row">'+
      '<div class="column text-right">'+
        '<button class="button small secondary hollow x-contact" onclick="$(this).parent().parent().parent().remove()">REMOVE</button>'+
      '</div>'+
      '<div class="column">'+
        '<label>Name</label>'+
        '<input type="text" name="contact_person[]" placeholder="contact name" value="" required maxlength="150" />'+
      '</div>'+
      '<div class="column large-2 text-right">'+
        '<label>Phone</label>'+
      '</div>'+
      '<div class="column large-10">'+
        '<input type="text" name="contact_number[]" placeholder="000-00-0000" value="" required maxlength="150" />'+
      '</div>'+
      '<div class="column large-2 text-right">'+
        '<label>Email</label>'+
      '</div>'+
      '<div class="column large-10">'+
        '<input type="text" name="contact_email[]" placeholder="example@domain.com" value="" required maxlength="150" />'+
      '</div>'+
    '</div>'+
    '<hr />'+
  '</div>');

  $(".contacts > .large-6").removeClass("end");
  $(".contacts > .large-6:last-of-type").addClass("end");
}

$(function() {
  $('#add-tag input[type=text]').keypress(function (e) {
    var key = e.which;
      if (key == 13) {
        $('#add-tag a').trigger('click');
        return false;
      }
  });

  $('#add-tag a').click(function() {
    var textbox = $('#add-tag input[type=text]');

    if (textbox.val().length > 0) {
      $.ajax({
        url      : addURL,
        type     : 'post',
        dataType : 'json',
        data     : {
          phrase : textbox.val(),
          _token : csrf
        },
        success : function(response) {
          if (response.hasOwnProperty('error')) {
            console.log(response.error);
          } else {
            var li = document.createElement('li');
            li.dataset.id = response.id;
            li.innerText  = response.phrase;

            var a = document.createElement('a');
            a.innerText  = 'x';
            a.href       = 'javascript:void(0);';
            a.dataset.id = response.id;
            a.addEventListener('click', omit);

            li.appendChild(a);

            $('#category-1').prepend(li);
            $('#add-tag input[type=text]').val('');
          }
        }
      });
    }
  });

  $('#tags > li > a').click(omit);

  $("#category-1, #category-2, #category-3, #category-4, #category-5, #category-6").sortable({
    connectWith : ".tags",
    receive     : function(e,ui) {
      var category = ui.item.parent().data('index');
      var tag      = ui.item.data('id');

      $.ajax({
        url  : updateURL,
        type : "post",
        data : {
          _token   : csrf,
          id       : tag,
          category : category
        },
        success : function(response) {
          console.log(response);
        }
      });
    }
  }).disableSelection();

  $('[data-sort]').click(function() {
    sortResults($(this).data('sort'));
  });
});

function omit() {
  var elem = $(this).parent();
  var id   = this.dataset.id;

  $.ajax({
    url      : removeURL,
    type     : 'post',
    dataType : 'json',
    data     : {
      id     : id,
      _token : csrf
    },
    success : function(response) {
      if (response.hasOwnProperty('error')) {
        console.log(response.error);
      } else {
        elem.remove();
      }
    }
  });
}

var sortField = $('#suppliers-table').data('field');
var sortOrder = $('#suppliers-table').data('order');
var page      = $('#suppliers-table').data('page');

function sortResults(via) {
  if (via == sortField) {
    sortOrder = sortOrder == 'DESC' ? 'ASC' : 'DESC';
  } else {
    sortField = via;
    sortOrder = 'ASC';
  }

  var $params = {
    sort  : sortOrder,
    field : sortField,
    page  : 1
  };

  window.location.href = $('base').attr('href')+'?'+$.param($params);
}

function requestPassword(id) {
  $.ajax({
    url: $('base').attr('href')+"/generate-password",
    type: "post",
    data: {
      id: id,
      _token: $('meta[name="csrf-token"]').attr('content')
    },
    success: function(response) {
      if (response.hasOwnProperty('error')) {
        $('.error').text('Error: '+response.error);
      } else {
        $('#make-password').addClass('hide');
        $('#email-step1').addClass('hide');
        $('#send-to-supplier').removeClass('hide').attr('href', response.link);
        $('#email-step2').removeClass('hide');
      }
    }
  });
}
