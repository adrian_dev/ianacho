$(function() {
    $('#image').change(function() {
        var data = new FormData(document.getElementById('upload'));

        $.ajax({
            url         : $('#upload').attr('action'),
            type        : $('#upload').attr('method'),
            data        : data,
            cache       : false,
            dataType    : 'json',
            contentType : false,
            processData : false,
            beforeSend : function() {
                return true;
            },
            success : function(response) {
                if (response.hasOwnProperty('src')) {
                    if ($('#new .product-image img').length > 0) {
                        $('#new .product-image img').attr('src', response.src);
                        $('#new input[name=image]').val(response.src);
                        $('#new input[name=thumbnail]').val(response.thumb);
                    } else {
                        var img = new Image();
                        img.src = $('base').attr('href')+'/'+response.src;
                        $('#new .product-image').prepend(img);
                        $('#new input[name=image]').val(response.src);
                        $('#new input[name=thumbnail]').val(response.thumb);
                    }
                }
            }
        });
    });

    $('.tags input[type=checkbox]').change(function() {
        $(this).parent().toggleClass('on');
    });

    $('.edit').click(function() {
        $('input:disabled, textarea:disabled').removeAttr('disabled');
        $(this).fadeOut();
        $('.save-edit').fadeIn();
    });

    $('.save-edit').click(function() {
        $.ajax({
            url  : $('.info').data('action'),
            type : 'post',
            data : {
                name           : $('input[name=name]').val(),
                contact_person : $('input[name=contact_person]').val(),
                phone          : $('input[name=phone]').val(),
                email          : $('input[name=email]').val(),
                address        : $('textarea[name=address]').val(),
                website        : $('input[name=website]').val(),
                _token         : $('.info').data('token')
            },
            dataType : 'json',
            success  : function(response) {
                if (response.success) {
                    $('input, textarea').attr('disabled', 'true');
                    $('.save-edit').fadeOut();
                    $('.edit').fadeIn();
                }
            }
        });
    });

    $('#logo').change(function() {
        var data = new FormData(document.getElementById('new-logo'));

        $.ajax({
            url         : $('#new-logo').attr('action'),
            type        : 'post',
            data        : data,
            cache       : false,
            contentType : false,
            processData : false,
            dataType    : 'json',
            success : function(response) {
                if (response.hasOwnProperty('src')) {
                    $('label[for=logo] img').attr('src', $('base').attr('href')+'/'+response.src);
                }
            }
        });
    });
});
