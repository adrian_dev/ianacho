function addContactForm() {
  var contacts = $(".contacts");
  contacts.append('<div class="column large-6">'+
    '<div class="row">'+
      '<div class="column text-right">'+
        '<button class="button small secondary hollow x-contact" onclick="$(this).parent().parent().parent().remove()">REMOVE</button>'+
      '</div>'+
      '<div class="column">'+
        '<label>Name</label>'+
        '<input type="text" name="contact_person[]" placeholder="contact name" value="" required maxlength="150" />'+
      '</div>'+
      '<div class="column large-2 text-right small-2">'+
        '<label>Phone</label>'+
      '</div>'+
      '<div class="column large-10">'+
        '<input type="text" name="contact_number[]" placeholder="000-00-0000" value="" required maxlength="150" />'+
      '</div>'+
      '<div class="column large-2 text-right small-2">'+
        '<label>Email</label>'+
      '</div>'+
      '<div class="column large-10">'+
        '<input type="text" name="contact_email[]" placeholder="example@domain.com" value="" required maxlength="150" />'+
      '</div>'+
    '</div>'+
    '<hr />'+
  '</div>');

  $(".contacts > .large-6").removeClass("end");
  $(".contacts > .large-6:last-of-type").addClass("end");
}
