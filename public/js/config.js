CKEDITOR.editorConfig = function( config ) {
  config.toolbar = [
    { name: 'clipboard', items: [ 'Cut', 'Copy', 'Paste', 'PasteText', '-', 'Undo', 'Redo' ] },
    { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline' ] },
    { name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent' ] },
    { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] }
  ];
};
