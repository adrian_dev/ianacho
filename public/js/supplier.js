$(document).foundation();

$(function() {
  $("input[name='contact_person[]'], input[name='contact_number[]'], input[name='contact_email[]']").prop('disabled', true);

  $('#image').change(function() {
    var data = new FormData(document.getElementById('upload'));

    $.ajax({
      url         : $('#upload').attr('action'),
      type        : $('#upload').attr('method'),
      data        : data,
      cache       : false,
      dataType    : 'json',
      contentType : false,
      processData : false,
      beforeSend : function() {
        return true;
      },
      success : function(response) {
        if (response.hasOwnProperty('src')) {
          if ($('#new .product-image img').length > 0) {
            $('#new .product-image img').attr('src', response.src);
            $('#new input[name=image]').val(response.src);
            $('#new input[name=thumbnail]').val(response.thumb);
          } else {
            var img = new Image();
            img.src = $('base').attr('href')+'/'+response.src;
            $('#new .product-image').prepend(img);
            $('#new input[name=image]').val(response.src);
            $('#new input[name=thumbnail]').val(response.thumb);
          }
        }
      }
    });
  });

  $('.tags input[type=checkbox]').change(function() {
    $(this).parent().toggleClass('on');
  });

  $('.edit').click(function() {
    $('input:disabled, textarea:disabled').removeAttr('disabled');
    $("input[name='contact_person[]'], input[name='contact_number[]'], input[name='contact_email[]']").prop('disabled', false);
    $('#add-contact').removeClass('hide');
    $(this).hide();
    $(".x-contact").show();
    $("#add-contact").show();
    $('.save-edit').fadeIn();
  });

  $('.save-edit').click(function() {
    $.ajax({
      url  : $('.info').data('action'),
      type : 'post',
      data : {
        name    : $('input[name=name]').val(),
        address : $('textarea[name=address]').val(),
        website : $('input[name=website]').val(),
        _token  : $('.info').data('token'),
        contact_person: $("input[name='contact_person[]']").map(function(){return $(this).val();}).get(),
        contact_number: $("input[name='contact_number[]']").map(function(){return $(this).val();}).get(),
        contact_email: $("input[name='contact_email[]']").map(function(){return $(this).val();}).get(),
        contact_id: $("input[name='contact_id[]']").map(function(){return $(this).val();}).get()
      },
      dataType : 'json',
      success  : function(response) {
        if (response.success) {
          $('input, textarea').attr('disabled', 'true');
          $('.save-edit').hide();
          $('.edit').fadeIn();
          $('.x-contact').hide();
          $("#add-contact").hide();
        }
      }
    });
  });

  $('#logo').change(function() {
    var data = new FormData(document.getElementById('new-logo'));

    $.ajax({
      url         : $('#new-logo').attr('action'),
      type        : 'post',
      data        : data,
      cache       : false,
      contentType : false,
      processData : false,
      dataType    : 'json',
      success : function(response) {
        if (response.hasOwnProperty('src')) {
          $('label[for=logo] img').attr('src', $('base').attr('href')+'/'+response.src);
        }
      }
    });
  });

  $(".x-contact").hide();
  $("#add-contact").hide();
});

var stickyOffset = $('.sticky').offset().top;

$(window).scroll(function(){
  var sticky = $('.sticky'),
      scroll = $(window).scrollTop();

  if (scroll >= stickyOffset) sticky.addClass('fixed');
  else sticky.removeClass('fixed');
});
