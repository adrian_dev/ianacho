<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductTag extends Model
{
    protected $table = "lib_product_tags";

    protected $fillable = [
        "product_id",
        "supplier_id",
        "status"
    ];

    public function getID()
    {
        return $this->id;
    }

    public function getProductID()
    {
        return $this->product_id;
    }

    public function setProductID($value)
    {
        $this->product_id = $value;
    }

    public function getTagID()
    {
        return $this->tag_id;
    }

    public function setTagID($value)
    {
        $this->tag_id = $value;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($value)
    {
        $this->status = $value;
    }

    public function record()
    {
      return $this->hasOne("App\Tag", "id", "tag_id");
    }
}
