<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserFavorite extends Model
{
    protected $table = "lib_user_favorites";

    protected $fillable = [
      'user_id',
      'label',
      'status'
    ];

    public function getID()
    {
      return $this->id;
    }

    public function getSupplierID()
    {
      return $this->supplier_id;
    }

    public function setSupplierID($value)
    {
      $this->supplier_id = $value;
    }

    public function getLabel()
    {
      return $this->label;
    }

    public function setlabel($value)
    {
      $this->label = $value;
    }

    public function getStatus()
    {
      return $this->status;
    }

    public function setStatus($value)
    {
      $this->status = $value;
    }

  public function products()
  {
    return $this->hasMany('App\FavoriteProduct', 'favorite_id', 'id');
  }
}
