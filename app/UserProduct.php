<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProduct extends Model
{
  protected $table = 'lib_user_products';

  protected $fillable = ['user_id', 'product_id', 'status'];

  public function getID()
  {
    return $this->id;
  }

  public function getUserID()
  {
    return $this->user_id;
  }

  public function setUserID($value)
  {
    $this->user_id = $value;
  }

  public function getProductID()
  {
    return $this->product_id;
  }

  public function setProductID($value)
  {
    $this->product_id = $value;
  }

  public function getStatus()
  {
    return $this->status;
  }

  public function setStatus($value)
  {
    $this->status = $value;
  }

  public function user()
  {
    return $this->hasOne('App\User');
  }

  public function product()
  {
    return $this->hasOne('App\Product', 'id', 'product_id');
  }
}
