<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplierContact extends Model
{
  protected $table = "lib_supplier_contacts";

  protected $fillable = [
    'supplier_id',
    'name',
    'phone',
    'email',
    'status'
  ];

  public function getID()
  {
    return $this->id;
  }

  public function getSupplierID()
  {
    return $this->supplier_id;
  }

  public function setSupplierID($value)
  {
    $this->supplier_id = $value;
  }

  public function getName()
  {
    return $this->name;
  }

  public function setName($value)
  {
    $this->name = $value;
  }

  public function getPhone()
  {
    return $this->phone;
  }

  public function setPhone($value)
  {
    $this->phone = $value;
  }

  public function getEmail()
  {
    return $this->email;
  }

  public function setEmail($value)
  {
    $this->email = $value;
  }

  public function getStatus()
  {
    return $this->status;
  }

  public function setStatus($value)
  {
    $this->status = $value;
  }
}
