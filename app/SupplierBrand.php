<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplierBrand extends Model
{
  protected $table = 'lib_supplier_brands';

  protected $fillable = array('name', 'status');

  public function getID()
  {
    return $this->id;
  }

  public function setSupplierID($value)
  {
    $this->supplier_id = $value;
  }

  public function getName()
  {
    return $this->name;
  }

  public function setName($value)
  {
    $this->name = $value;
  }

  public function getStatus()
  {
    return $this->status;
  }

  public function setStatus($value)
  {
    $this->status = $value;
  }

  public function supplier()
  {
    return $this->hasOne('App\Supplier', 'id', 'supplier_id');
  }
}
