<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "lib_products";

    protected $fillable = [
        'supplier_id',
        'name',
        'image',
        'thumbnail',
        'description',
        'brand',
        'line',
        'price',
        'dimensions',
        'code',
        'green_mark',
        'status'
    ];

    public function getID()
    {
        return $this->id;
    }

    public function getSupplierID()
    {
        return $this->supplier_id;
    }

    public function setSupplierID($value)
    {
        $this->supplier_id = $value;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($value)
    {
        $this->name = $value;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($value)
    {
        $this->image = $value;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($value)
    {
        $this->status = $value;
    }

    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    public function setThumbnail($value)
    {
        $this->thumbnail = $value;
    }

    public function getDescription()
    {
      return $this->description;
    }

    public function setDescription($value)
    {
      $this->description = $value;
    }

  public function getCode()
  {
    return $this->code;
  }

  public function setCode($value)
  {
    $this->code = $value;
  }

  public function getRange()
  {
    return $this->line;
  }

  public function setRange($value)
  {
    $this->line = $value;
  }

  public function getPrice()
  {
    return $this->price;
  }

  public function setPrice($value)
  {
    $this->price = $value;
  }

  public function getDimensions()
  {
    return $this->dimensions;
  }

  public function setDimensions($value)
  {
    $this->dimensions = $value;
  }

  public function getGreenMark()
  {
    return $this->green_mark;
  }

  public function setGreenMark($value)
  {
    $this->green_mark = $value;
  }

  public function getBrand()
  {
    return $this->brand;
  }

  public function setBrand($value)
  {
    $this->brand = $value;
  }

  public function getLeadTime()
  {
    return $this->lead_time;
  }

  public function setLeadTime($value)
  {
    $this->lead_time = $value;
  }

  public function supplier()
  {
    return $this->belongsTo('App\Supplier', 'supplier_id');
  }

  public function tags()
  {
    return $this->belongsToMany('App\Tag', 'lib_product_tags', 'product_id', 'tag_id');
  }

  public function user()
  {
    return $this->belongsToMany('App\User', 'lib_user_products', 'product_id', 'user_id');
  }

  public function isFavorite()
  {
    if (Auth::check()) {
      return $this->user()->where('user_id', Auth::id());
    } else {
      return $this->user()->where('user_id', 0);
    }
  }
}
