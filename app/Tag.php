<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = "lib_tags";

    protected $fillable = ["phrase", 'status'];

    public function getID()
    {
        return $this->id;
    }

    public function getPhrase()
    {
        return $this->phrase;
    }

    public function setPhrase($value)
    {
        $this->phrase = $value;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($value)
    {
        $this->status = $value;
    }

    public function getCategory()
    {
      return $this->category;
    }

    public function setCategory($value)
    {
      $this->category = $value;
    }

    public function product()
    {
        return $this->belongsToMany('App\Products', 'lib_product_tags', 'product_id', 'tag_id');
    }
}
