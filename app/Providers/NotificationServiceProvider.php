<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use App\Supplier;
use App\ProductRange;

class NotificationServiceProvider extends ServiceProvider
{
  /**
   * Bootstrap the application services.
   *
   * @return void
  */
  public function boot()
  {
    view()->composer('*', function($view) {
      if (Auth::check()) {
        $range = array(date('Y-m-d H:i:s', strtotime('-30 days')), date('Y-m-d H:i:s'));

        $suppliers = Supplier::where("status", 1)->count();
        $emptySuppliers = Supplier::where("status", 1)->where("total_products", 0)->count();
        $oldSuppliers = Supplier::where("status", 1)->where("total_products", "<", 5)->whereNotIn("updated_at", $range)->count();
        $recent = ProductRange::where("status", 1)->orderBy("updated_at", "DESC")->limit(5)->get();

        $data = [
          "totalSuppliers" => $suppliers,
          "emptySuppliers" => $emptySuppliers,
          "oldSuppliers" => $oldSuppliers,
          "recentUpdates" => $recent
        ];

        $view->with("sidebar", $data);
      }
    });
  }

  /**
   * Register the application services.
   *
   * @return void
   */
  public function register()
  {
      //
  }
}
