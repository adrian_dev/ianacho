<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\User;
use App\Tag;
use App\Product;

class CreativeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
      view()->composer('*', function($view) {
        $tags = Tag::where('status', 1)->inRandomOrder()->limit(20)->get();
        $view->with('randomTags', $tags);

        //load favorites when logged in
        $favorites = array();

        if (auth()->check()) {
          $IDs = User::find(auth()->id())
            ->products()
            ->where('status', 1)
            ->orderBy('updated_at', 'DESC')
            ->limit(4)
            ->get(['product_id'])
            ->pluck('product_id')
            ->toArray();

          if (!empty($IDs)) {
            foreach ($IDs as $ID) {
              $favorites[] = Product::where('status', 1)
                ->where('id', $ID)
                ->first();
            }
          }
        }

        $view->with('favorites', $favorites);
      });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
