<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplierProduct extends Model
{
  protected $table = "lib_supplier_products";

  protected $fillable = [
    "supplier_id",
    "name",
    "brand",
    "description",
    "gallery",
    "min_price",
    "max_price",
    "category",
    "green_mark",
    "lead_time",
    "status"
  ];

  public function getID()
  {
    return $this->id;
  }

  public function setSupplierID($value)
  {
    $this->supplier_id = $value;
  }

  public function getSupplierID()
  {
    return $this->supplier_id;
  }

  public function setName($value)
  {
    $this->name = $value;
  }

  public function getName()
  {
    return $this->name;
  }

  public function setBrand($value)
  {
    $this->brand = $value;
  }

  public function getBrand()
  {
    return $this->brand;
  }

  public function setDescription($value)
  {
    $this->description = $value;
  }

  public function getDescription()
  {
    return $this->description;
  }

  public function setGallery($value)
  {
    $this->gallery = $value;
  }

  public function getGallery()
  {
    return $this->gallery;
  }

  public function setMinPrice($value)
  {
    $this->min_price = $value;
  }

  public function getMinPrice()
  {
    return $this->min_price;
  }

  public function setMaxPrice($value)
  {
    $this->max_price = $value;
  }

  public function getMaxPrice()
  {
    return $this->max_price;
  }

  public function setCategory($value)
  {
    $this->category = $value;
  }

  public function getCategory()
  {
    return $this->category;
  }

  public function setCategoryValue($value)
  {
    $this->category_value = $value;
  }

  public function getcategoryvalue()
  {
    return $this->category_value;
  }

  public function setGreenMark($value)
  {
    $this->green_mark = $value;
  }

  public function getGreenMark()
  {
    return $this->green_mark;
  }

  public function setLeadTime($value)
  {
    $this->lead_time = $value;
  }

  public function getLeadTime()
  {
    return $this->lead_time;
  }

  public function setStatus($value)
  {
    $this->status = $value;
  }

  public function getStatus()
  {
    return $this->status;
  }

  public function range()
  {
    return $this->hasMany("App\ProductRange", "product_id", "id");
  }

  public function _range()
  {
    return $this->range()->where("status", 1)->get();
  }

  public function tags()
  {
    return $this->hasMany("App\ProductTag", "product_id", "id");
  }

  public function _tags()
  {
    return $this->tags()->where("status", 1)->get();
  }

  public function supplier()
  {
    return $this->belongsTo("App\Supplier", "supplier_id", "id");
  }
}
