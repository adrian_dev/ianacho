<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductRange extends Model
{
  protected $table = "lib_product_range";

  protected $fillable = [
    "product_id",
    "name",
    "code",
    "image",
    "thumbnail",
    "status"
  ];

  public function getID()
  {
    return $this->id;
  }

  public function setProductID($value)
  {
    $this->product_id = $value;
  }

  public function getProductID()
  {
    return $this->product_id;
  }

  public function setName($value)
  {
    $this->name = $value;
  }

  public function getName()
  {
    return $this->name;
  }

  public function setCode($value)
  {
    $this->code = $value;
  }

  public function getCode()
  {
    return $this->code;
  }

  public function setImage($value)
  {
    $this->image = $value;
  }

  public function getImage()
  {
    return $this->image;
  }

  public function setThumbnail($value)
  {
    $this->thumbnail = $value;
  }

  public function getThumbnail()
  {
    return $this->thumbnail;
  }

  public function setStatus($value)
  {
    $this->status = $value;
  }

  public function getStatus()
  {
    return $this->status;
  }

  public function product()
  {
    return $this->belongsTo("App\SupplierProduct", "product_id", "id");
  }
}
