<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticable as AuthenticableTrait;

class Supplier extends Model implements Authenticatable
{
    protected $table = "lib_suppliers";

    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
        'address',
        'website',
        'username',
        'password',
        'logo',
        'remember_token'
    ];

    public function getID()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($value)
    {
        $this->name = $value;
    }

    public function getContactPerson()
    {
        return $this->contact_person;
    }

    public function setContactPerson($value)
    {
        $this->contact_person = $value;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($value)
    {
        $this->email = $value;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone($value)
    {
        $this->phone = $value;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress($value)
    {
        $this->address = $value;
    }

    public function getWebsite()
    {
        return $this->website;
    }

    public function setWebsite($value)
    {
        $this->website = $value;
    }

    public function getAuthIdentifierName()
    {
        return $this->username;
    }

    public function setAuthIdentifierName($value)
    {
        $this->username = $value;
    }

    public function getAuthPassword()
    {
        return $this->password;
    }

    public function setAuthPassword($value)
    {
        $this->password = $value;
    }

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getLogo()
    {
        return $this->logo;
    }

    public function setLogo($value)
    {
        $this->logo = $value;
    }

    public function getAuthIdentifier()
    {
        return $this->id;
    }

    public function getRememberTokenName()
    {
        return 'supplier_token';
    }

    public function getUsername()
    {
      return $this->username;
    }

    public function setUsername($value)
    {
      $this->username = $value;
    }

    public function getPassword()
    {
      return $this->password;
    }

    public function setPassword($value)
    {
      $this->password = $value;
    }

    public function getTotalProducts()
    {
      return $this->total_products;
    }

    public function setTotalProducts($value)
    {
      $this->total_products = $value;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($value)
    {
        $this->status = $value;
    }

    public function products()
    {
        return $this->hasMany('App\SupplierProduct', "supplier_id", "id");
    }

    public function contacts()
    {
      return $this->hasMany('App\SupplierContact');
    }

  public function brands()
  {
    return $this->hasMany('App\SupplierBrand');
  }
}
