<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'lib_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

  public function getID()
  {
    return $this->id;
  }

  public function getName()
  {
    return $this->name;
  }

  public function setName($value)
  {
    $this->name = $value;
  }

  public function getEmail()
  {
    return $this->email;
  }

  public function setEmail($value)
  {
    $this->email = $value;
  }

  public function getPassword()
  {
    return $this->password;
  }

  public function setPassword($value)
  {
    $this->password = $value;
  }

  public function getUsername()
  {
    return $username;
  }

  public function setUsername($value)
  {
    $this->username = $value;
  }

  public function getLevel()
  {
    return $this->level;
  }

  public function setLevel($value)
  {
    $this->level = $value;
  }

  public function getRememberToken()
  {
    return $this->remember_token;
  }

  public function setRememberToken($value)
  {
    $this->remember_token = $value;
  }

  public function favorites()
  {
    return $this->hasMany('App\UserFavorite');
  }

  public function products()
  {
    return $this->hasMany('App\UserProduct');
  }

  public function favoriteProducts()
  {
    return $this->products();
  }
}
