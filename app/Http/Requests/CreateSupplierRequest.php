<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateSupplierRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:lib_suppliers|max:255',
            'website' => 'max:150',
            'address' => 'max:255',
            'contact_person' => 'required|array',
            'contact_number' => 'required|array',
            'contact_email' => 'required|array'
        ];
    }
}
