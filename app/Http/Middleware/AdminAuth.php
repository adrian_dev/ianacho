<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Routing\Middleware;
use App\User;
use Auth;

class AdminAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    //   if(!in_array($request->ip(), ['203.125.60.78', '127.0.0.1', '121.6.25.157', '183.90.37.138', '128.6.0.143', '58.185.70.34', '58.185.70.34'])){
    //     return redirect()->route('profile');
    //   }

      if (Auth::check()) {
          return $next($request);
      } else {
          return redirect()->route('admin.login');
      }

    }
}
