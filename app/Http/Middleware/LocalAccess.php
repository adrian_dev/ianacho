<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Routing\Middleware;

class LocalAccess
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next)
  {
    // if (!in_array($request->ip(), ['127.0.0.1'])) {
    //   return redirect()->route('profile');
    // }

    return $next($request);
  }
}
