<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Routing\Middleware;
use App\Supplier;
use Auth;

class SupplierAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'supplier')
    {
        if (Auth::guard($guard)->check()) {
            return $next($request);
        } else {
            return redirect()->route('login');
        }
    }
}
