<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Supplier;
use App\Tag;
use App\SupplierProduct;
use App\ProductTag;
use App\ProductRange;
use App\SearchIndex;
use Illuminate\Support\Facades\DB;

/**
 * Hopefully this is the most context aware search of all
 */
class FindController extends Controller
{
  protected $fields = array("id", "name", "updated_at");
  protected $limit = 30;
  
  //TODO: needs to check if there is supplier name filter
  //TODO: needs to check if there is brand filter (brand filter will only show when a supplier is ticked)
  //TODO: needs to check if there is price range filter
  //TODO: needs to check if there is lead time filter
  
  public function search(Request $request)
  {
    if ($request->has("phrase")) {
      $phrase = ltrim(rtrim($request->phrase));

      $tags = DB::table("lib_tags")
        ->select(array("id"))
        ->where("status", 1)
        ->whereRaw("MATCH phrase AGAINST (?)", array($phrase))
        ->pluck("id")
        ->toArray();
      
      if (empty($tags)) {
        $products = $this->findProductsByName($phrase);
        
        if (empty($products)) {
          $products = $this->findProductsBySupplierName($phrase);
          
          if (empty($products)) {
            $products = $this->findProductsByBrandName($phrase);
          }
        }
      } else {
        $products = $this->getProductIDsFromTags($tags);
      }

      if (empty($products)) {
        return $this->findResultsByRangeName($phrase);
      } else {
        return $this->getRangeFromProductIDs($products);
      }
    } else {
      return $this->getRange();
    }
  }
  
  public function getProductIDsFromTags($tags)
  {
    return ProductTag::select(array("product_id"))
      ->where("status", 1)
      ->whereIn("tag_id", $tags)
      ->orderBy("updated_at", "DESC")
      ->pluck("product_id")
      ->toArray();
  }
  
  public function getRangeFromProductIDs($products)
  {
    return ProductRange::select($this->fields)
      ->where("status", 1)
      ->whereIn("product_id", $products)
      ->orderBy("updated_at", "DESC")
      ->skip(0)
      ->limit(30)
      ->get();
  }
  
  public function getRange()
  {
    return Productrange::select($this->fields)
      ->where("status", 1)
      ->orderBy("updated_at", "DESC")
      ->skip(0)
      ->limit(30)
      ->get();
  }
  
  public function findProductsByName($phrase)
  {
    $words = explode(" ", $phrase);
    
    $ids = array();
    foreach ($words as $word) {
      $_ids = SupplierProduct::select(array("id"))
        ->where("status", 1)
        ->whereNotIn("id", $ids)
        ->where("name", "like", "{$word}%")
        ->pluck("id")
        ->toArray();
      
      $ids = array_merge($ids, $_ids);
    }
    
    return $ids;
  }
  
  public function findProductsBySupplierName($phrase)
  {
    $words = explode(" ", $phrase);
    
    $suppliers = array();
    foreach ($words as $word) {
      $_suppliers = Supplier::select(array("id"))
        ->where("status", 1)
        ->whereNotIn("id", $suppliers)
        ->where("name", "like", "{$word}%")
        ->pluck("id")
        ->toArray();
      
      $suppliers = array_merge($suppliers, $_suppliers);
    }
    
    return SupplierProduct::select(array("id"))
      ->where("status", 1)
      ->whereIn("supplier_id", $suppliers)
      ->pluck("id")
      ->toArray();
  }
  
  public function findProductsByBrandName($phrase)
  {
    $words = explode(" ", $phrase);
    
    $ids = array();
    foreach ($words as $word) {
      $_ids = SupplierProduct::select(array("id"))
        ->where("status", 1)
        ->whereNotIn("id", $ids)
        ->where("brand", "like", "{$word}%")
        ->pluck("id")
        ->toArray();
      
      $ids = array_merge($ids, $_ids);
    }
    
    return $ids;
  }
  
  public function findResultsByRangeName($phrase)
  {
    $words = explode(" ", $phrase);
    
    $ids = array();
    foreach ($words as $word) {
      $_ids = ProductRange::select(array("id"))
        ->where("status", 1)
        ->whereNotIn("id", $ids)
        ->where("name", "like", "%{$word}%")
        ->pluck("id")
        ->toArray();
      
      $ids = array_merge($ids, $_ids);
    }

    return Productrange::select($this->fields)
      ->where("status", 1)
      ->whereIn("id", $ids)
      ->orderBy("updated_at", "DESC")
      ->skip(0)
      ->limit(30)
      ->get();
  }
}
