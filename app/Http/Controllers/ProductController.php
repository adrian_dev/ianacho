<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Supplier;
use App\Tag;
use App\Product;
use App\ProductTag;
use App\SupplierProduct;
use App\ProductRange;
use App\SearchIndex;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
  protected $categories = array(
    2 => 'finishes',
    3 => 'materials',
    4 => 'colors',
    5 => 'placement',
    6 => 'properties'
  );

  public function add(Request $request)
  {
    $data = array(
      "title" => "Add Product",
      "tags" => $this->tags(),
      "categories" => $this->categories,
      "id" => "",
      "name" => "",
      "range" => array(),
      "currentTags" => array(),
      "tagsList" => array(),
      "gallery" => array(),
      "details" => [
        "brand" => $request->has("brand") ? $request->brand : "",
        "category" => [
          "label" => $request->has("label") ? $request->label : "Category",
          "value" => $request->has("category") ? $request->category : ""
        ],
        "price" => [
          "min" => "",
          "max" => ""
        ],
        "lead_time" => 12,
        "green_mark" => false,
        "description" => ""
      ]
    );

    return view("supplier.forms.product", $data);
  }

  public function get($id)
  {
    $userID = Auth::guard("supplier")->user()->getID();
	  $product = SupplierProduct::where("id", $id)
      ->where("supplier_id", $userID)
      ->first();

    if (empty($product)) {
      abort(404);
    }

    $data = array(
      "title" => "Edit Product",
      "tags" => $this->tags(),
      "categories" => $this->categories,
      "product" => $product,
      "id" => $product->getID(),
      "name" => $product->getName(),
      "range" => $product->range()->where("status", 1)->get(),
      "currentTags" => $product->tags()->where("status", 1)->get()->pluck("tag_id")->toArray(),
      "tagsList" => $product->tags()->where("status", 1)->get(),
      "gallery" => unserialize($product->getGallery()),
      "details" => [
        "brand" => $product->getBrand(),
        "category" => unserialize($product->getCategory()),
        "price" => [
          "min" => $product->getMinPrice(),
          "max" => $product->getMaxPrice()
        ],
        "lead_time" => $product->getLeadTime(),
        "green_mark" => $product->getGreenMark(),
        "description" => $product->getDescription()
      ]
    );

    return view("supplier.forms.product", $data);
  }

  public function tags()
  {
    $tags = array();

    foreach ($this->categories as $i => $category) {
      $_tags = Tag::where('category', $i)
        ->where('status', 1)
        ->orderBy('phrase', 'asc')
        ->get();

      if (!$_tags->isEmpty()) {
        $tags[$i] = $_tags;
      }
    }

    return $tags;
  }

  public function preview($id)
  {
    $product = Product::find($id);
    return view('product.preview', ['product' => $product]);
  }

  public function set(Request $request)
  {
    $id = Auth::guard('supplier')->user()->getID();

    $this->validate($request, [
      "id" => "sometimes|int",
      "name" => "required",
      "range" => "array|required",
      "brand" => "sometimes",
      "category" => "sometimes|array",
      "price" => "sometimes|array",
      "lead_time" => "sometimes",
      "green_mark" => "sometimes|boolean",
      "description" => "sometimes",
      "gallery" => "sometimes|array",
      "tags" => "sometimes|array"
    ]);

    if ($request->has("id")) {
      $product = SupplierProduct::find($request->id);
    } else {
      $product = new SupplierProduct();
    }

    $product->setSupplierID($id);
    $product->setName($request->name);
    $product->setBrand(empty($request->brand) ? "" : $request->brand);
    $product->setCategory(empty($request->category) ? "" : serialize($request->category));
    $product->setCategoryValue(empty($request->category["value"]) ? "" : $request->category["value"]);
    $product->setMinPrice(empty($request->price["min"]) ? 0 : $request->price["min"]);
    $product->setMaxPrice(empty($request->price["max"]) ? 0 : $request->price["max"]);
    $product->setLeadTime(empty($request->lead_time) ? 0 : $request->lead_time);
    $product->setGreenMark(empty($request->green_mark) ? 0 : $request->green_mark);
    $product->setDescription(empty($request->description) ? "" : $request->description);
    $product->setGallery(empty($request->gallery) ? "" : serialize($request->gallery));

    $product->save();

    $this->setRange($product->getID(), $request->range);
    $this->setTags($product->getID(), $request->tags);

    $this->updateTotalProducts();
    $this->updateIndexes($product);

    $request->session()->flash("message", "{$product->getName()} was successfully saved.");

    return route("product.get", ["id" => $product->getID()]);
  }

  public function setRange($id, $range)
  {
    if (empty($range)) {
      return false;
    }

    ProductRange::where("product_id", $id)->where("status", 1)->update(["status" => 0]);

    foreach ($range as $item) {
      if (isset($item["id"])) {
        $record = ProductRange::where("id", $item["id"])->first();
      } else {
        $record = new ProductRange();
      }

      $record->setProductID($id);
      $record->setName($item["name"]);
      $record->setCode(empty($item["code"]) ? "" : $item["code"] );
      $record->setImage($item["image"]);
      $record->setThumbnail($item["thumb"]);
      $record->setStatus(1);
      $record->save();
    }

    return true;
  }

  public function setTags($id, $tags)
  {
    if (empty($tags)) {
      return false;
    }

    ProductTag::where("product_id", $id)
      ->where("status", 1)
      ->update(["status" => 0]);

    foreach ($tags as $tag) {
      $record = ProductTag::where("product_id", $id)
        ->where("tag_id", $tag)
        ->first();

      if (empty($record)) {
        $record = new ProductTag();
      }

      $record->setProductID($id);
      $record->setTagID($tag);
      $record->setStatus(1);
      $record->save();
    }

    return true;
  }

  private function updateTotalProducts()
  {
    $supplier = Auth::guard('supplier')->user();

    $total = $supplier->products()
      ->where("status", 1)
      ->count();
    $supplier->setTotalProducts($total);
    $supplier->save();

    return $total;
  }

  private function updateIndexes($product)
  {
    if (!empty($product->range)) {
      foreach ($product->range as $range) {
        $index = SearchIndex::where("range_id", $range->getID())
            ->first();

        if ($range->getStatus() == 1) {
          if(empty($index)) {
            $index = new SearchIndex();
          }

          $index->setRangeID($range->getID());
          $index->setName($range->getName());
          $index->setSupplierName($product->supplier->getName());
          $index->setProductName($product->getName());
          $index->setBrand($product->getBrand());
          $index->setCategory($product->getCategoryValue());
          $index->setMinPrice($product->getMinPrice());
          $index->setMaxPrice($product->getMaxPrice());
          $index->setLeadTime($product->getLeadTime());

          $tags = ProductTag::where("status", 1)
            ->where("product_id", $product->getID())
            ->pluck("tag_id")
            ->toArray();

          if (!empty($tags)) {
            $tags = Tag::where("status", 1)
              ->whereIn("id", $tags)
              ->pluck("phrase")
              ->toArray();
          }

          $index->setTags(implode(",", $tags));

          $index->save();
        } else {
          if (!empty($index)) {
            $index->delete();
          }
        }
      }
    }
  }

  public function remove($id)
  {
    $product = SupplierProduct::find($id);
    $product->setStatus(0);
    $product->save();

    $ranges = ProductRange::where("product_id", $id)->get()->pluck("id")->toArray();
    $range = ProductRange::where("product_id", $id)->update(["status" => 0]);

    $tags = ProductTag::where("product_id", $id)->update(["status" => 0]);

    SearchIndex::whereIn("range_id", $ranges)->delete();

    return redirect()->route("profile")->with("message", "Product successfully deleted");
  }
}
