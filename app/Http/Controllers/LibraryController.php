<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Supplier;
use App\SupplierProduct;
use App\ProductRange;
use App\Tag;

class LibraryController extends Controller
{
  public function index()
  {
    $data = [
      "latest" => $this->latest(10),
      "tags" => $this->randomTags(),
      "suppliers" => Supplier::where("status", 1)->orderBy("updated_at", "DESC")->limit(10)->get()
    ];

    return view('library.index', $data);
  }

  private function latest($limit = 5)
  {
    $range = array(date('Y-m-d H:i:s', strtotime('-30 days')), date('Y-m-d H:i:s'));

    $result = SupplierProduct::where("updated_at", $range)
      ->where("status", 1)
      ->inRandomOrder()
      ->limit($limit)
      ->get();

    if ($result->isEmpty()) {
      $result = SupplierProduct::where("status", 1)
      ->orderBy("updated_at", "desc")
      ->limit($limit)
      ->get();
    }

    return $result;
  }

  private function randomTags()
  {
    return Tag::where("status", 1)
      ->inRandomOrder()
      ->limit(20)
      ->get();
  }

  public function search(Request $request)
  {
    $suppliers = Supplier::where("status", 1)
      ->get()
      ->pluck("id")
      ->toArray();

    $products = SupplierProduct::where("status", 1)
      ->whereIn("supplier_id", $suppliers)
      ->get()
      ->pluck("id")
      ->toArray();

    $result = ProductRange::where("status", 1)
      ->whereIn("product_id", $products)
      ->orderBy("updated_at", "DESC")
      ->get();

    return view("library.search", ["items" => $result]);
  }

  public function product($id)
  {
    $item = ProductRange::find($id);

    if (empty($item)) {
      abort(404);
    }

    $related = $item
      ->product
      ->range()
      ->where("status", 1)
      ->where("id", "!=", $item->getID())
      ->limit(6)
      ->get();
    
    $latest = SupplierProduct::where("status", 1)
      ->where("supplier_id", $item->product->supplier->getID())
      ->where("id", "!=", $item->product->getID())
      ->orderBy("updated_at", "DESC")
      ->limit(4)
      ->pluck("id")
      ->toArray();
    
    $more = ProductRange::where("status", 1)
      ->whereIn("product_id", $latest)
      ->inRandomOrder()
      ->limit(6)
      ->get();

    return view("library.item", ["item" => $item, "related" => $related, "more" => $more]);
  }

  public function catalog($id)
  {
    $supplier = Supplier::find($id);
    $products = SupplierProduct::where("status", 1)
      ->where("supplier_id", $id)
      ->orderBy("brand", "ASC")
      ->orderBy("category_value", "ASC")
      ->get();

    return view("library.catalog", ["supplier" => $supplier, "products" => $products]);
  }

  public function collection()
  {
    return view('library.collection');
  }

  public function suppliers()
  {
    $suppliers = Supplier::where("status", 1)
      ->orderBy("name", "ASC")
      ->get();
    
    return view("library.suppliers", ["suppliers" => $suppliers]);
  }
}
