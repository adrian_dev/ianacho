<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;
use App\Product;
use App\Supplier;

class TagController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        $categories = [
          1 => 'none',
          2 => 'finishes',
          3 => 'materials',
          4 => 'colors',
          5 => 'placement',
          6 => 'properties'
        ];

        $tags = array();
        foreach ($categories as $i => $category) {
          $_tags = Tag::where('category', $i)->orderBy('updated_at', 'DESC')->get();
          if (!$_tags->isEmpty()) {
            foreach ($_tags as $tag) {
              $tags[$i][] = $tag;
            }
          }
        }

        $data = $this->activity();
        $data['categories'] = $categories;
        $data['tags'] = $tags;

        return view('admin.forms.tag', $data);
    }

    public function set(Request $request)
    {
        $tag = new Tag();
        $result = $tag->where('phrase', $request->phrase)->first();
        if (empty($result)) {
            $tag->setPhrase($request->phrase);
            $tag->save();
            return response()->json(array('id' => $tag->getID(), 'phrase' => $tag->getPhrase()));
        } else {
            if ($result->getStatus() == 0) {
                $result->setStatus(1);
                $result->save();

                return response()->json(array('id' => $result->getID(), 'phrase' => $result->getPhrase()));
            }
            return response()->json(array('error' => 'tag already exists'));
        }
    }

    public function remove(Request $request)
    {
        if ($tag = Tag::find($request->id)) {
            $tag->setStatus(0);
            $tag->save();

            return response()->json(array('id' => $tag->getID()));
        } else {
            return response()->json(array('error' => 'cannot find tag'));
        }
    }

    public function update(Request $request)
    {
      $this->validate($request, [
        'id' => 'required|numeric',
        'category' => 'required|numeric'
      ]);

      return Tag::where('id', $request->id)->update(['category' => $request->category]);
    }

    public function activity()
    {
      $range = array(date('Y-m-d H:i:s', strtotime('-7 days')), date('Y-m-d H:i:s'));

      $productUpdates  = Product::whereBetween('updated_at', $range)->count();
      if ($productUpdates > 0) {
        $data['update']['product'] = $productUpdates;
      }

      $supplierUpdates = Supplier::whereBetween('updated_at', $range)->count();
      if ($supplierUpdates > 0) {
        $data['update']['supplier'] = $supplierUpdates;
      }

      $newProducts = Product::whereBetween('created_at', $range)->count();
      if ($newProducts > 0) {
        $data['new']['product'] = $newProducts;
      }

      $newSuppliers = Supplier::whereBetween('created_at', $range)->count();
      if ($newSuppliers > 0) {
        $data['new']['supplier'] = $newSuppliers;
      }

      $totalProducts = Product::count();
      if ($totalProducts > 0) {
        $data['total']['product'] = $totalProducts;
      }

      $totalSuppliers = Supplier::count();
      if ($totalSuppliers > 0) {
        $data['total']['supplier'] = $totalSuppliers;
      }

      $totalTags = Tag::count();
      if ($totalTags > 0) {
        $data['total']['tag'] = $totalTags;
      }

      return $data;
    }
}
