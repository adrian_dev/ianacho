<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Supplier;
use App\Product;
use App\Tag;
use App\ProductTag;
use App\User;
use App\UserFavorite;
use App\FavoriteProduct;
use App\SupplierBrand;
use App\UserProduct;
use App\UserRequest;
use Illuminate\Support\Facades\Auth;

class CreativeController extends Controller
{
  protected $categories = array(
    2 => 'finishes',
    3 => 'materials',
    4 => 'colors',
    5 => 'placement',
    6 => 'properties'
  );

  public function index()
  {
    $range = array(date('Y-m-d H:i:s', strtotime('-7 days')), date('Y-m-d H:i:s'));

    $newProducts = Product::whereBetween('created_at', $range)
      ->where('status', 1)
      ->orderBy('updated_at', 'DESC')
      ->limit(5)
      ->get();

    $activeSuppliers = Supplier::where('status', 1)
      ->orderBy('updated_at', 'DESC')
      ->limit(5)
      ->get();

    $data = array(
      'newProducts' => $newProducts,
      'activeSuppliers' => $activeSuppliers
    );

    return view('creatives.index', $data);
  }

  public function archive(Request $request)
  {
    $data = array(
      'products' => $this->sift($request),
      'filters' => $this->filters()
    );

    return view('creatives.archive', $data);
  }

  public function product($id)
  {
    $fav = 'unfav';
    $product = Product::find($id);

    if (Auth::check()) {
      $result = UserProduct::where('user_id', Auth::id())
        ->where('product_id', $id)
        ->first();

      if (!empty($result)) {
        $fav = 'fav';
      }
    }

    $supplierProducts = Product::where('supplier_id', $product->getSupplierID())
      ->where('id', '!=', $product->getID())
      ->where('status', 1)
      ->orderBy('updated_at', 'DESC')
      ->limit(5)
      ->get();

    $tagged = ProductTag::whereIn('tag_id', $product->tags->pluck('id'))
      ->where('product_id', '!=', $product->getID())
      ->where('status', 1)
      ->get()
      ->pluck('product_id');

    $relatedProducts = Product::whereIn('id', $tagged)
      ->where('status', 1)
      ->orderBy('updated_at', 'DESC')
      ->limit(5)
      ->get();
    
    if (Auth::check()) {
      $result = UserRequest::where('status', 1)
        ->where('product_id', $id)
        ->first();
        
      if (empty($result)) {
        $inRequest = false;
      } else {
        $inRequest = true;
      }
    } else {
      $inRequest = false;
    }

    $data = array(
      'product' => $product,
      'supplierProducts' => $supplierProducts,
      'relatedProducts' => $relatedProducts,
      'inRequest' => $inRequest,
      'fav' => $fav
    );

    return view('creatives.product', $data);
  }

  public function login()
  {
    return view('creatives.forms.login');
  }

  public function logout()
  {
    Auth::logout();
    return view('creatives.forms.login');
  }

  public function authenticate(Request $request)
  {
    $this->validate($request, [
        'username' => 'required',
        'password' => 'required'
    ]);

    if (Auth::attempt(['username' => $request->username, 'password' => $request->password])) {
        return redirect()->route('home');
    } else {
        return redirect()->back()->with('message', 'username password mismatch');
    }
  }

  public function preview($id)
  {
    $product = Product::find($id);
    return view('creatives.preview', ['product' => $product]);
  }

  private function filters()
  {
    $suppliers = Supplier::where('status', 1)
      ->orderBy('name', 'ASC')
      ->get();

    $tags = array();

    foreach ($this->categories as $i => $category) {
      $tags[$category] = Tag::where('category', $i)
      ->where('status', 1)
      ->orderBy('phrase', 'ASC')
      ->get();
    }

    return array(
      'suppliers' => $suppliers,
      'tags' => $tags,
      'green_mark' => [0, 1]
    );
  }

  private function sift($request)
  {
    $IDs = array();
    $sort = 'updated_at';
    $order = 'DESC';
    $page = $request->has('page') ? $request->page : 1;
    $limit = 40;
    $green = $request->has('green_mark') ? [$request->green_mark] : [0, 1];
    $lead = $request->has('lead_time') ? $request->lead_time : 12;
    $q = $request->has('q') ? $request->q : '';
    $priceMin = $request->has('price_min') ? $request->price_min : 0;
    $priceMax = $request->has('price_max') ? $request->price_max : 10000;


    if ($request->has('sort')) {
      switch ($request->sort) {
        case 'name':
          $sort = 'name';
          $order = 'ASC';
          break;
        case 'price-desc':
          $sort = 'price';
          $order = 'DESC';
          break;
        case 'price-asc':
          $sort = 'price';
          $order = 'ASC';
          break;
        default:
          $sort = 'updated_at';
          $order = 'DESC';
          break;
      }
    }

    if ($request->has('supplier')) {
      $IDs = Product::whereIn('supplier_id', $request->supplier)
        ->where('status', 1)
        ->get(['id'])
        ->pluck('id')
        ->toArray();
    }

    if ($request->has('tag')) {
      if (empty($IDs)) {
        $IDs = ProductTag::whereIn('tag_id', $request->tag)
          ->where('status', 1)
          ->get(['product_id'])
          ->pluck('product_id')
          ->toArray();
      } else {
        $IDs = ProductTag::whereIn('tag_id', $request->tag)
          ->whereIn('product_id', $IDs)
          ->where('status', 1)
          ->get(['product_id'])
          ->pluck('product_id')
          ->toArray();
      }
    }

    if (!empty($q)) {
      $stopWords = array(
        'a', 'about', 'above', 'after', 'again', 'against', 'all', 'am', 'an',
        'and', 'any', 'are', "aren't", 'as', 'at', 'be', 'because', 'been',
        'before', 'being', 'below', 'between', 'both', 'but', 'by', "can't",
        'cannot', 'could', "couldn't", 'did', "didn't", 'do', 'does', "doesn't",
        'doing', "don't", 'down', 'during', 'each', 'few', 'for', 'from',
        'further', 'had', "hadn't", 'has', "hasn't", 'have', "haven't", 'having',
        'he', "he'd", "he'll", "he's", 'her', 'here', "here's", 'hers',
        'herself', 'him', 'himself', 'his', 'how', "how's", 'i', "i'd", "i'll",
        "i'm", "i've", 'if', 'in', 'into', 'is', "isn't", 'it', "it's", 'its',
        'itself', "let's", 'me', 'more', 'most', "mustn't", 'my', 'myself', 'no',
        'nor', 'not', 'of', 'off', 'on', 'once', 'only', 'or', 'other', 'ought',
        'our', 'ours', 'ourselves', 'out', 'over', 'own', 'same', "shan't",
        'she', "she'd", "she'll", "she's", 'should', "shouldn't", 'so', 'some',
        'such', 'than', 'that', "that's", 'the', 'their', 'theirs', 'them',
        'themselves', 'then', 'there', "there's", 'these', 'they', "they'd",
        "they'll", "they're", "they've", 'this', 'those', 'through', 'to', 'too',
        'under', 'until', 'up', 'very', 'was', "wasn't", 'we', "we'd", "we'll",
        "we're", "we've", 'were', "weren't", 'what', "what's", 'when', "when's",
        'where', "where's", 'which', 'while', 'who', "who's", 'whom', 'why',
        "why's", 'with', "won't", 'would', "wouldn't", 'you', "you'd", "you'll",
        "you're", "you've", 'your', 'yours', 'yourself', 'yourselves', 'zero'
      );

      $phrase = preg_replace('/\b('.implode('|', $stopWords).')\b/','', $q);
      $phrase = preg_replace('/\s+/', ' ', $phrase);
      $phrase = rtrim(ltrim($phrase, ' '), ' ');
      $queries = explode(' ', $phrase);

        //find product name
      if (empty($IDs)) {
        foreach ($queries as $query) {
          $found = Product::where('name', 'LIKE', '%'.$query.'%')
            ->where('status', 1)
            ->get(['id'])
            ->pluck('id')
            ->toArray();

            if (!empty($found)) {
              $IDs = array_unique(array_merge($IDs, $found));
            }
        }
      } else {
        $_IDs = array();
        foreach ($queries as $query) {
          $found = Product::where('name', 'LIKE', '%'.$query.'%')
            ->where('id', $IDs)
            ->where('status', 1)
            ->get(['id'])
            ->pluck('id')
            ->toArray();

            if (!empty($found)) {
              $_IDs = array_unique(array_merge($_IDs, $found));
            }
        }

        $IDs = $_IDs;
      }

      foreach ($queries as $query) {
        $result = Tag::where('phrase', $query)
          ->where('status', 1)
          ->first();

          //find tag
        if (!empty($result)) {
          $result = ProductTag::where('tag_id', $result->getID())
            ->where('status', 1)
            ->get(['product_id'])
            ->pluck('product_id')
            ->toArray();

          if (!empty($result)) {
            $IDs = array_unique(array_merge($IDs, $result));
          }
        }

        $result = Supplier::where('name', $query)
          ->where('status', 1)
          ->first();

          //find supplier
        if (!empty($result)) {
          $result = Product::whereIn('supplier_id', $result->getID())
            ->where('status', 1)
            ->get(['id'])
            ->pluck('id')
            ->toArray();

          if (!empty($result)) {
            $IDs = array_unique(array_merge($IDs, $result));
          }
        }
      }
    }

    $exceptions = [
      'page',
      'green_mark',
      'lead_time',
      'sort',
      'q',
      'price_min',
      'price_max'
    ];

    if (empty($IDs) && empty($request->except($exceptions)) && empty($q)) {
      //no params
      $products = Product::where('status', 1)
        ->whereIn('green_mark', $green)
        ->where('lead_time', '<=', $lead)
        ->whereBetween('price', [$priceMin, $priceMax])
        ->orderBy($sort, $order)
        ->paginate($limit);
    } else {
      //with params
      $products = Product::where('status', 1)
        ->whereIn('id', $IDs)
        ->whereIn('green_mark', $green)
        ->where('lead_time', '<=', $lead)
        ->whereBetween('price', [$priceMin, $priceMax])
        ->orderBy($sort, $order)
        ->paginate($limit);
    }

    return $products;
  }

  // public function collection()
  // {
  //   $collection = User::find(Auth::id())
  //     ->products()
  //     ->where('status', 1)
  //     ->orderBy('updated_at', 'DESC')
  //     ->get();

  //   $products = array();

  //   foreach ($collection as $item) {
  //     $products[] = $item->product;
  //   }

  //   return view('creatives.collection', ['products' => $products]);
  // }

  public function collection()
  {
    return view('creatives.collection');
  }
}
