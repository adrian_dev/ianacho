<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Supplier;
use App\Tag;
use App\Product;
use App\SupplierContact;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\CreateSupplierRequest;
use Illuminate\Support\Facades\File;
use App\SupplierProduct;
use App\ProductRange;
use App\ProductTag;

class AdminController extends Controller
{
  private $salt = 'penny';

  protected $categories = array(
    2 => 'finishes',
    3 => 'materials',
    4 => 'colors',
    5 => 'placement',
    6 => 'properties'
  );

  protected $limit = 15;

  private $range;

  public function __construct()
  {
    $this->range = array(date('Y-m-d H:i:s', strtotime('-7 days')), date('Y-m-d H:i:s'));
  }

  public function index(Request $request)
  {
    $page = $request->has('page') ? $page = $request->page : 1;
    $sort = $request->has('sort') ? $request->sort : 'DESC';
    $field = $request->has('field') ? $request->field : 'updated_at';

    $data = array();

    if ($request->has('name')) {
      $suppliers = Supplier::where("name", "like", "%{$request->name}%")
        ->where("status", 1)
        ->orderBy($field, $sort)
        ->skip($this->limit * ($page - 1))
        ->take($this->limit)
        ->get();

      $pagination = Supplier::where("name", "like", "%{$request->name}%")
        ->where("status", 1)
        ->orderBy($field, $sort)
        ->skip($this->limit * ($page - 1))
        ->paginate($this->limit)->appends([
          'sort' => $sort,
          'field' => $field]);
      
      $pagination = $pagination->appends(['name' => $request->name])->links();
    } else {
      $suppliers = Supplier::where("status", 1)
        ->orderBy($field, $sort)
        ->skip($this->limit * ($page - 1))
        ->take($this->limit)
        ->get();

      $pagination = Supplier::where("status", 1)
        ->orderBy($field, $sort)
        ->skip($this->limit * ($page - 1))
        ->paginate($this->limit)->appends([
          'sort' => $sort,
          'field' => $field]);
    }

    if (!$suppliers->isEmpty()) {
      $data['suppliers'] = $suppliers;
      $data['pagination'] = $pagination;
      $data['order'] = $sort;
      $data['field'] = $field;

      $tagged = [];
      foreach ($suppliers as $supplier) {
        $IDs = $supplier->products()->where("status", 1)->pluck("id")->toArray();
        $tagged[$supplier->getID()] = ProductTag::where("status", 1)->whereIn("product_id", $IDs)->count();
      }

      $data["tagged"] = $tagged;
    }

    $labels = array(
      'name' => 'NAME',
      'logo' => 'LOGO',
      'total_products' => 'TOTAL PRODUCTS',
      'updated_at' => 'LAST UPDATE'
    );

    foreach ($labels as $item => $label) {
      $order  = $item == $field ? strtolower($sort) : 'asc';
      $active = $item == $field ? 'active' : '';

      if ($item == 'logo') {
        $data['thead'][] = '<th>LOGO</th>';
      } else {
        $data['thead'][] = '<th><a href="javascript:void(0);" data-sort="'.$item.'">'.$label.'</a><i class="'.$order.' '.$active.'"></i></th>';
      }
    }

    $data["activity"] = [
      "updated" => $this->updatedProducts(),
      "old" => $this->oldestProducts(),
      "stagnant" => $this->stagnantSuppliers(),
      "empty" => $this->emptySuppliers()
    ];

    $data["names"] = Supplier::where("status", 1)->pluck("name")->toArray();

    return view('admin.dashboard', array_merge($data, $this->activity()));
  }

  public function login()
  {
    return view('admin.login', ['title' => 'Login']);
  }

  public function authenticate(Request $request)
  {
    $this->validate($request, [
      'username' => 'required',
      'password' => 'required',
    ]);

    if (Auth::attempt(['username' => $request->username, 'password' => $request->password])) {
      if (Auth::user()->level > 0) {
        return redirect()->route('home');
      } else {
        return redirect()->route('admin.dashboard')->with('message', 'Welcome back '.Auth::user()->getName());
      }

    } else {
      return redirect()->back()->with('error', 'Email and password do not match. Please try again.');
    }
  }

  public function logout()
  {
    Auth::logout();
    return redirect()->route('admin.login')->with('message', 'Successfully logged out.');
  }

  public function add(Validator $validator)
  {
    $data = $this->activity();
    $data['title'] = 'Add New';
    return view('admin.forms.supplier', $data);
  }

  public function edit($id)
  {
    $data = array(
      'supplier' => Supplier::find($id),
      'title'    => 'Edit'
    );

    return view('admin.forms.supplier', $data);
  }

  public function view($id)
  {
    $data = array(
      'supplier' => Supplier::find($id),
      'title'    => "Suplier Information"
    );

    return view('admin.view_supplier', $data);
  }

  public function save(CreateSupplierRequest $request)
  {
    $this->validate($request, [
      "name" => "required|unique:lib_suppliers|max:150",
      "address" => "sometimes",
      "website" => "sometimes|max:150",
      "phone" => "sometimes|unique:lib_suppliers|max:50"
    ]);

    $supplier = new Supplier();

    $supplier->setName($request->name);
    $supplier->setAddress($request->address ?: "");
    $supplier->setWebsite($request->website ?: "");
    $supplier->setPhone($request->phone ?: "");
    $supplier->setUsername($this->makeUsername($request->name));
    $supplier->setPassword("");
    $supplier->setLogo("");
    $supplier->setRememberToken("");
    $supplier->setStatus(1);
    $supplier->save();

    $id = $supplier->getID();

    foreach ($request->contact_person as $i => $person) {
      if (isset($request->contact_id[$i])) {
        $contact = SupplierContact::find($request->contact_id[$i]);
      } else {
        $contact = new SupplierContact();
      }

      $contact->setSupplierID($id);
      $contact->setName($person);
      $contact->setPhone($request->contact_number[$i]);
      $contact->setEmail($request->contact_email[$i]);
      $contact->save();
    }

    return redirect()->route('admin.email', ['id' => $id])
      ->with('message', 'Successfully saved!');
  }

  public function update(Request $request)
  {
    $this->validate($request, [
      'id'             => 'required|numeric',
      'name'           => 'required|max:150',
      'contact_person' => 'required|array'
    ]);

    $supplier = Supplier::find($request->id);
    $supplier->setName($request->name);
    $supplier->setWebsite($request->website ?: "");
    $supplier->setAddress($request->address ?: "");
    $supplier->setPhone($request->phone ?: "");
    $supplier->save();

    SupplierContact::where("supplier_id", $supplier->getID())->update(["status" => 0]);

    foreach ($request->contact_person as $i => $person) {
      if (isset($request->contact_id[$i])) {
        $contact = SupplierContact::find($request->contact_id[$i]);
      } else {
        $contact = new SupplierContact();
        $contact->setSupplierID($supplier->getID());
      }

      $contact->setName($person);
      $contact->setPhone($request->contact_number[$i]);
      $contact->setEmail($request->contact_email[$i]);
      $contact->setStatus(1);
      $contact->save();
    }

    return redirect()->route('admin.dashboard')
      ->with('message', "<b>{$request->name}</b> successfully updated.");
  }

  public function email($id)
  {
    $supplier = Supplier::find($id);
    $data = $this->activity();
    $data['supplier'] = $supplier;

    $contacts = $supplier->contacts;
    $link = '';
    $email = array();
    if (!$contacts->isEmpty()) {
      $template = file_get_contents(base_path().'/email_template.txt');
      $template = str_replace('!!supplier!!', $supplier->getName(), $template);
      $template = str_replace('!!link!!', route('login'), $template);

      if ($contacts->count() > 1) {
        foreach ($contacts as $contact) {
          if (!array_key_exists('mailto', $email)) {
            $email['mailto'] = $contact->getEmail();
            $template = str_replace('!!name!!', $contact->getName(), $template);
          } else {
            $email['items']['cc'][] = $contact->getEmail();
          }
        }
        $email['items']['cc'] = implode(';', $email['items']['cc']);
      } else {
        $contact = $contacts->first();
        $email['mailto'] = $contact->getEmail();
      }

      $email['items']['subject'] = "Supplier Profile Access";
      $email['items']['body'] = $template;

      $items = array();
      foreach ($email['items'] as $key => $value) {
        $items[] = $key.'='.rawurlencode($value);
      }
      $link = 'mailto:'.$email['mailto'].'&'.implode('&', $items);
    }

    $data['link'] = $link;

    return view('admin.email_supplier', $data);
  }

  public function send($id)
  {
    //insert mailsend code here
    return redirect()->route('admin.dashboard')
      ->with('message', 'Email successfully sent!');
  }

  function generateRandomString($length = 10)
  {
    $characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString     = '';

    for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
    }

    return $randomString;
  }

  public function logoForm($id)
  {
    $supplier  = Supplier::find($id);
    $suppliers = Supplier::all();

    return view('admin.forms.logo', ['id' => $id, 'logo' => $supplier->getLogo()]);
  }

  public function logoSave(Request $request)
  {
    $folder = "images/".$request->id;

    if (!File::exists($folder)) {
      File::makeDirectory($folder, 755, true);
    }

    $filename = 'supplier_logo.'.$request->file('logo')->getClientOriginalExtension();

    $request->file('logo')->move($folder, $filename);

    $src = $folder.'/'.$filename.'?flag='.time();

    $supplier = Supplier::find($request->id);
    $supplier->setLogo($src);
    $supplier->save();

    return array('src' => $folder.'/'.$filename.'?flag='.time());
  }

  public function activity()
  {
    $range = array(date('Y-m-d H:i:s', strtotime('-7 days')), date('Y-m-d H:i:s'));

    $productUpdates  = Product::whereBetween('updated_at', $range)->count();
    if ($productUpdates > 0) {
      $data['update']['product'] = $productUpdates;
    }

    $supplierUpdates = Supplier::whereBetween('updated_at', $range)->count();
    if ($supplierUpdates > 0) {
      $data['update']['supplier'] = $supplierUpdates;
    }

    $newProducts = Product::whereBetween('created_at', $range)->count();
    if ($newProducts > 0) {
      $data['new']['product'] = $newProducts;
    }

    $newSuppliers = Supplier::whereBetween('created_at', $range)->count();
    if ($newSuppliers > 0) {
      $data['new']['supplier'] = $newSuppliers;
    }

    $totalProducts = Product::count();
    if ($totalProducts > 0) {
      $data['total']['product'] = $totalProducts;
    }

    $totalSuppliers = Supplier::count();
    if ($totalSuppliers > 0) {
      $data['total']['supplier'] = $totalSuppliers;
    }

    $totalTags = Tag::count();
    if ($totalTags > 0) {
      $data['total']['tag'] = $totalTags;
    }

    return $data;
  }

  public function productNew($id)
  {
    $tags = array();

    foreach ($this->categories as $i => $category) {
      $_tags = Tag::where('category', $i)
        ->where('status', 1)
        ->get();

      if (!$_tags->isEmpty()) {
        $tags[$i] = $_tags;
      }
    }

    $data = array(
      "title" => "Add Product",
      "supplier_id" => $id,
      "tags" => $tags,
      "categories" => $this->categories
    );

    return view("admin.forms.product", array_merge($data, $this->activity()));
  }

  public function requestPassword(Request $request)
  {
    $password = $this->generateRandomString();
    $supplier = Supplier::find($request->id);
    $hash = Hash::make($password);
    $supplier->setPassword($hash);
    $supplier->save();

    $contacts = $supplier->contacts;
    $link = '';
    $email = array();
    if (!$contacts->isEmpty()) {
      $template = file_get_contents(base_path().'/email_template.txt');
      $template = str_replace('!!supplier!!', $supplier->getName(), $template);
      $template = str_replace('!!link!!', route('login'), $template);
      if (empty($supplier->getUsername())) {
        $supplier->setUsername($this->makeUsername($supplier->getName()));
      }
      $template = str_replace('!!username!!', $supplier->getUsername(), $template);
      $template = str_replace('!!password!!', $password, $template);

      if ($contacts->count() > 1) {
        foreach ($contacts as $contact) {
          if (!array_key_exists('mailto', $email)) {
            $email['mailto'] = $contact->getEmail();
            $template = str_replace('!!name!!', $contact->getName(), $template);
          } else {
            $email['items']['cc'][] = $contact->getEmail();
          }
        }

        $email['items']['cc'][] = 'adrianv@mmoser.com';
        $email['items']['cc'] = implode(';', $email['items']['cc']);
      } else {
        $contact = $contacts->first();
        $email['mailto'] = $contact->getEmail();
        $template = str_replace('!!name!!', $contact->getName(), $template);
      }

      $email['items']['subject'] = "Supplier Profile Access";
      $email['items']['body'] = $template;

      $items = array();
      foreach ($email['items'] as $key => $value) {
        $items[] = $key.'='.rawurlencode($value);
      }

      //final mailto link
      $link = 'mailto:'.$email['mailto'].'&'.implode('&', $items);

      return array('link' => $link);
    } else {
      return array('error' => 'No contacts for supplier');
    }
  }

  public function productDetail($id)
  {
    $product = Product::find($id);
    return view('supplier.product', ['product' => $product]);
  }

  public function makeUsername($name)
  {
    return preg_replace("/[^A-Za-z0-9 ]/", '', str_replace(' ', '', strtolower($name)));
  }

  public function registration(Request $request)
  {
    $this->validate($request, [
      'name' => 'required|max:50',
      'email' => 'email|unique:lib_users',
      'username' => 'required|unique:lib_users|max:50',
      'password' => 'required|confirmed'
    ]);

    $user = new User();
    $user->setName($request->name);
    $user->setEmail($request->email);
    $user->setUsername($request->username);
    $user->setPassword(Hash::make($request->password));
    $user->setLevel(1);
    $user->setRememberToken(1);
    $user->save();

    return view('admin.register')->with('message', 'Registration Saved!');
  }

  private function newestProducts()
  {
    return SupplierProduct::where("status", 1)
      ->where("created_at", $this->range)
      ->orderBy("updated_at", "desc")
      ->get();
  }

  private function updatedProducts()
  {
    return SupplierProduct::where("status", 1)
      ->where("updated_at", $this->range)
      ->orderBy("updated_at", "desc")
      ->get();
  }

  private function oldestProducts()
  {
    return SupplierProduct::where("status", 1)
      ->where("updated_at", "<", date('Y-m-d H:i:s', strtotime('-60 days')))
      ->orderBy("updated_at", "desc")
      ->get();
  }

  private function emptySuppliers()
  {
    return Supplier::where("status", 1)
      ->where("total_products", 0)
      ->get();
  }

  private function stagnantSuppliers()
  {
    return Supplier::where("status", 1)
      ->where("updated_at", "<", date('Y-m-d H:i:s', strtotime('-60 days')))
      ->orderBy("updated_at", "asc")
      ->get();
  }

  private function totalSuppliers()
  {
    return Supplier::where("status", 1)->count();
  }

  private function totalProducts()
  {
    return SupplierProduct::where("status", 1)->count();
  }

  public function deactivate($id)
  {
    $record = Supplier::findOrFail($id);
    $record->setStatus(0);
    $record->save();

    return redirect()
      ->route('admin.dashboard')
      ->with('message', "Successfully deactivated ".$record->getName());
  }

  public function set(Request $request)
  {
    $this->validate($request, [
      "id" => "sometimes|int|unique:lib_suppliers",
      "name" => "required|unique:lib_suppliers|max:255",
      "logo" => "sometimes|nullable|max:255",
      "address" => "required|max:255",
      "website" => "sometimes|nullable|unique:lib_suppliers|url",
      "contact" => "required|array"
    ]);

    if ($request->has("id")) {
      $supplier = Supplier::find($request->id);
    } else {
      $supplier = new Supplier();
    }

    $supplier->setName($request->name);
    $supplier->setLogo($request->logo);
    $supplier->setAddress($request->address);
    $supplier->setWebsite($request->website ?: '');
    $supplier->setStatus(1);
    $supplier->save();

    foreach ($request->contact as $contact) {
      if (isset($contact["id"])) {
        $person = SupplierContact::find($contact["id"]);
      } else {
        $person = SupplierContact::where("name", $person["name"])
          ->where("supplier_id", $supplier->getID())
          ->first();
        
        if (empty($person)) {
          $person = new SupplierContact();
        } else if ($person->getStatus() == 1) {
          return redirect()->back()->withErrors(["Contact already exists."])->withInput();
        }
      }

      if (empty($person["name"])) {
        return redirect()->back()->withErrors(["Contact name must not be empty."])->withInput();
      } else {
        $person->setName($person["name"]);
      }

      if (empty($person["phone"])) {
        return redirect()->back()->withErrors(["Contact phone must not be empty."])->withInput();
      } else {
        $person->setPhone($person["phone"]);
      }

      if (empty($person["email"])) {
        return redirect()->back()->withErrors(["Contact email must not be empty."])->withInput();
      } else {
        $person->setEmail($person["email"]);
      }

      $person->setStatus(1);
      $person->setSupplierID($supplier->getID());
      $person->save();
    }

    return redirect()->back()->with("message", $supplier->getName()." has been successfully saved.");
  }

  public function get(Request $request)
  {
    if ($request->has("id")) {
      $supplier = Supplier::find($request->id);
      $title = "Update Supplier";
    } else {
      $supplier = [];
      $title = "Add New Supplier";
    }

    return view("admin.supplier", ["supplier" => $supplier, "title" => $title]);
  }
}
