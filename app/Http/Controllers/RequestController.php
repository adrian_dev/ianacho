<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\UserRequest;
use App\Product;
use App\Supplier;
use App\SupplierProduct;

class RequestController extends Controller
{
  public function supplierIndex()
  {
    $auth = Auth::guard('supplier');

    if ($auth->check()) {
      $IDs = Product::where('supplier_id', $auth->id())
        ->where('status', 1)
        ->get(['id'])
        ->pluck('id')
        ->toArray();

      $requests = UserRequest::where('status', 1)
        ->whereIn('product_id', $IDs)
        ->orderBy('user_id', 'ASC')
        ->get();

      $products = array();
      if (!$requests->isEmpty()) {
        foreach ($requests as $request) {
          $products[] = $request->product;
        }
      }
    }

    return view('supplier.requests', ['products' => $products]);
  }

  public function userIndex()
  {
    $requests = UserRequest::where('status', 1)
      ->where('user_id', Auth::id())
      ->get(['product_id'])
      ->toArray();

    $suppliers = array();
    $products = array();
    if (!empty($requests)) {
      $products = Product::where('status', 1)
        ->whereIn('id', $requests)
        ->orderBy('supplier_id', 'ASC')
        ->get();

      if (!$products->isEmpty()) {
        $supplier = 0;
        $_products = array();

        foreach ($products as $product) {
          if ($supplier != $product->getSupplierID()) {
            $supplier = $product->getSupplierID();
            $suppliers[] = $product->supplier;
          }

          $_products[$supplier][] = $product;
        }

        if (!empty($_products)) {
          $products = $_products;
        }
      }
    }

    return view('creatives.requests', ['products' => $products, 'suppliers' => $suppliers]);
  }

  public function products()
  {
    $requests = UserRequest::where('status', 1)
      ->where('user_id', Auth::id())
      ->limit(8)
      ->get(['product_id'])
      ->toArray();

    $products = array();

    if (!empty($requests)) {
      $products = Product::where('status', 1)
        ->whereIn('id', $requests)
        ->orderBy('supplier_id', 'ASC')
        ->get();
    }

    return $products;
  }

  public function supplierPerspective($id)
  {
    $IDs = Product::where('supplier_id', auth('supplier')->id())
      ->where('status', 1)
      ->get(['id'])
      ->pluck('id')
      ->toArray();

    $requests = UserRequest::where('status', 1)
      ->whereIn('product_id', $IDs)
      ->where('user_id', $id)
      ->orderBy('user_id', 'ASC')
      ->get();

    $products = array();
    if (!$requests->isEmpty()) {
      foreach ($requests as $request) {
        $products[] = $request->product;
      }
    }

    return $products;
  }

  public function creativePerspective($id)
  {
    $requests = UserRequest::where('status', 1)
      ->where('user_id', auth()->id())
      ->get(['product_id'])
      ->toArray();

    $products = array();
    if (!empty($requests)) {
      $products = Product::where('status', 1)
        ->where('supplier_id', $id)
        ->whereIn('id', $requests)
        ->get();
    }

    return $products;
  }

  public function set($id)
  {
    $request = UserRequest::where('user_id', Auth::id())
      ->where('product_id', $id)
      ->first();

    if (empty($request)) {
      $request = new UserRequest();
      $request->setUserID(Auth::id());
      $request->setProductID($id);
      $request->setStatus(1);
      $request->save();
    } else {
      $request->setStatus(1);
      $request->save();
    }

    $requests = UserRequest::where('status', 1)
      ->where('user_id', Auth::id())
      ->get();

    return $requests;
  }

  public function remove($id)
  {
    $request = UserRequest::where('user_id', Auth::id())
      ->where('product_id', $id)
      ->first();

    if (empty($request)) {
      return ['error' => 'missing'];
    } else {
      $request->setStatus(0);
      $request->save();
      $requests = UserRequest::where('status', 1)
        ->where('user_id', Auth::id())
        ->orderBy('updated_at', 'DESC')
        ->get();

      return $requests;
    }
  }

  public function getBrands()
  {
    $id = Auth::guard("supplier")->user()->getID();

    $brands = SupplierProduct::where("status", 1)
      ->where("supplier_id", $id)
      ->orderBy("brand", "asc")
      ->groupBy("brand")
      ->get(["brand"])
      ->pluck("brand")
      ->toArray();

    return $brands;
  }

  public function getCategories()
  {
    $id = Auth::guard("supplier")->user()->getID();

    $result = SupplierProduct::where("status", 1)
      ->where("supplier_id", $id)
      ->orderBy("category_value", "asc")
      ->groupBy("category_value")
      ->get(["category_value"])
      ->pluck("category_value")
      ->toArray();

    return $result;
  }
}
