<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManager as Image;

class UploadController extends Controller
{
  public function range(Request $request)
  {
    $ID = Auth::guard('supplier')->user()->getID();

    $this->validate($request, ['image' => 'required|image']);

    $dir = "images/{$ID}/range";

    if ($pic = $this->saveImage($request, $dir)) {
      if ($pic["ext"] == 'png') {
        $this->redimesionImage($dir.'/'.$pic['file'], 250, 250, $dir.'/thumbs/'.$pic['file'], 'png');
        
        return [
          'name' => $pic['name'],
          'raw' => $pic['src'],
          'thumb' => $dir.'/thumbs/'.$pic['file']
        ];
      } else {
        if ($thumb = $this->makeThumb($dir, $pic['file'])) {
          return [
            'name' => $pic['name'],
            'raw' => $pic['src'],
            'thumb' => $thumb
          ];
        } else {
          return response(['error' => 'cannot save thumbnail'], 500);
        }
      }
    } else {
      return response(['error' => 'cannot save image'], 500);
    }
  }

  public function gallery(Request $request)
  {
    $ID = Auth::guard('supplier')->user()->getID();

    $this->validate($request, ['image' => 'required|image']);

    $dir = "images/{$ID}/gallery";

    if ($pic = $this->saveImage($request, $dir)) {
      if ($pic["ext"] == 'png') {
        $this->redimesionImage($dir.'/'.$pic['file'], 250, 250, $dir.'/thumbs/'.$pic['file'], 'png');
        
        return [
          'name' => $pic['name'],
          'raw' => $pic['src'],
          'thumb' => $dir.'/thumbs/'.$pic['file']
        ];
      } else {
        if ($thumb = $this->makeThumb($dir, $pic['file'])) {
          return [
            'name' => $pic['name'],
            'raw' => $pic['src'],
            'thumb' => $thumb
          ];
        } else {
          return response(['error' => 'cannot save thumbnail'], 500);
        }
      }
    } else {
      return response(['error' => 'cannot save image'], 500);
    }
  }

  private function hasFolder($path)
  {
    if (empty($path)) {
      return false;
    } else if (!File::exists($path)) {
      File::makeDirectory($path, 755, true);
    }

    return true;
  }

  private function makeThumb($dir, $name)
  {
    $src = $dir.'/'.$name;
    $thumbDir = $dir."/thumbs";

    if ($this->hasFolder($thumbDir)) {
      $thumb = $thumbDir.'/'.$name;

      list($width, $height) = getimagesize($src);

      $myImage = imagecreatefromjpeg($src);

      if ($width > $height) {
        $y = 0;
        $x = ($width - $height) / 2;
        $smallestSide = $height;
      } else {
        $x = 0;
        $y = ($height - $width) / 2;
        $smallestSide = $width;
      }

      $thumbSize = 250;
      $th = imagecreatetruecolor($thumbSize, $thumbSize);

      imagecopyresampled($th, $myImage, 0, 0, $x, $y, $thumbSize, $thumbSize, $smallestSide, $smallestSide);

      imagejpeg($th, $thumb);

      return $thumb;
    } else {
      return false;
    }
  }

  private function saveImage($request, $dir)
  {
    if ($this->hasFolder($dir)) {
      $image = $request->file('image');
      $name = $image->getClientOriginalName();
      $_name = pathinfo($name, PATHINFO_FILENAME);
      $_ext = pathinfo($name, PATHINFO_EXTENSION);
      $name = preg_replace('/[^A-Za-z0-9\-]/', '', $_name)."-".time().'.'.$_ext;
      $request->file('image')->move($dir, $name);
      return [
        'name' => $_name,
        'src' => $dir.'/'.$name,
        'file' => $name,
        'ext' => $_ext
        ];
    } else {
      return false;
    }
  }

  private function set($id = 0)
  {
    if ($id < 1) {
      return false;
    } else {

    }
  }

  public function redimesionImage($endThu,$newX,$newY,$endImg,$fileType)
  {
    // Copy the image to be resized
    copy($endThu, $endImg);

    // Retrieves the image data
    list($width, $height) = getimagesize($endImg);

    // If the width is greater ...
    if($width >= $height) {
      // I set the width of the image to the desired size ...
      $newXimage = $newX;

      // And calculate the size of the time to not stretch the image
      $newYimage = ($height / $width) * $newXimage;
    } else {
        // Define the desired height ...
        $newYimage = $newY;

        // And calculate the width to not stretch the image
        $newXimage = ($width / $height) * $newYimage;
    }

    // Creates an initial image in memory with calculated measures
    $imageInicial = imagecreatetruecolor(ceil($newXimage), ceil($newYimage));

    // I check the extension of the image and create their respective image
    if ($fileType == 'jpeg')   $endereco = imagecreatefromjpeg($endImg);
    if ($fileType == 'jpg')  $endereco = imagecreatefromjpeg($endImg);      
    if ($fileType == 'png')    {
        $endereco = imagecreatefrompng($endImg);
        imagealphablending($imageInicial, false);
        imagesavealpha($imageInicial,true);
        $transparent = imagecolorallocatealpha($imageInicial, 255, 255, 255, 127);
        imagefilledrectangle($endereco, 0, 0, $newXimage, $newYimage, $transparent);
    }
    if ($fileType == 'gif')  {
        $endereco = imagecreatefromgif($endImg);
        $this->setTransparency($imageInicial,$endereco);
    }

    // I merge the image to be resized with created in memory
    imagecopyresampled($imageInicial, $endereco, 0, 0, 0, 0, ceil($newXimage), ceil($newYimage), ceil($width), ceil($height));

    // Creates the image in its final lacal, according to its extension
    if ($fileType == 'jpeg') imagejpeg($imageInicial, $endImg, 100);
    if ($fileType == 'jpg') imagejpeg($imageInicial, $endImg, 100);
    if ($fileType == 'png') imagepng($imageInicial, $endImg, 9);
    if ($fileType == 'gif') imagegif($imageInicial, $endImg, 100);
}

// Function to assist the PNG images, sets the transparency in the image
private function setTransparency($new_image,$image_source){ 
    $transparencyIndex = imagecolortransparent($image_source); 
    $transparencyColor = array('red' => 255, 'green' => 255, 'blue' => 255);     
    if($transparencyIndex >= 0){
        $transparencyColor = imagecolorsforindex($image_source, $transparencyIndex);    
    }
    $transparencyIndex = imagecolorallocate($new_image, $transparencyColor['red'], $transparencyColor['green'], $transparencyColor['blue']); 
    imagefill($new_image, 0, 0, $transparencyIndex); 
    imagecolortransparent($new_image, $transparencyIndex);        
}
}
