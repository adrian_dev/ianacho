<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Supplier;
use App\Tag;
use App\SupplierProduct;
use App\ProductTag;
use App\ProductRange;
use App\SearchIndex;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
  public function index(Request $request)
  {
    $page = $request->has("page") ? $request->page : 1;
    $lead = $request->has("lead_time") ? $request->lead_time : 12;
    $min = $request->has("price_min") ? $request->price_min : 0;
    $max = $request->has("price_max") ? $request->price_max : 1000;

    $green  = [0, 1];
    if ($request->has("green_mark")) {
      $green = [$request->green_mark];
    }

    $sort = [];
    if ($request->has('sort')) {
      switch ($request->sort) {
      case "name":
        $sort["field"] = "name";
        $sort["order"] = "ASC";
        break;
      default:
        $sort["field"] = "updated_at";
        $sort["order"] = "DESC";
        break;
      }
    } else {
      $sort["field"] = "updated_at";
      $sort["order"] = "DESC";
    }

    $words = explode(" ", $request->q);
    $ids = [];
    $hits = [];
    $suppliers = [];
    $tags = [];

    if (!empty($words)) {
      foreach ($words as $i => $word) {
        $_suppliers = Db::table("lib_suppliers")
          ->where("status", 1)
          ->where("name", "like", "%{$word}%")
          ->orderBy("name", "ASC")
          ->pluck("id")
          ->toArray();

        $suppliers = array_unique(array_merge($suppliers, $_suppliers));

        $_tags = Db::table("lib_tags")
          ->where("status", 1)
          ->where("phrase", "like", "%{$word}%")
          ->orderBy("phrase", "ASC")
          ->pluck("id")
          ->toArray();

        $tags = array_unique(array_merge($tags, $_tags));

        $_ids = Db::table("lib_search_indexes")
          ->where("name", "like", "%{$word}%")
          ->orWhere("supplier_name", "like", "%{$word}%")
          ->orWhere("product_name", "like", "%{$word}%")
          ->orWhere("brand", "like", "%{$word}%")
          ->orWhere("category", "like", "%{$word}%")
          ->orWhere("tags", "like", "%{$word}%")
          ->pluck("range_id")
          ->toArray();

        $_ids = Db::table("lib_search_indexes")
          ->whereIn("range_id", $_ids)
          ->where("min_price", ">=", $min)
          ->where("min_price", "<=", $max)
          ->where("max_price", "<=", $max)
          ->where("lead_time", "<=", $lead)
          ->pluck("range_id")
          ->toArray();

        if (empty($ids)) {
          $ids = $_ids;
        } else {
          $tmp = array_intersect($ids, $_ids);
          if (empty($tmp)) {
            $ids = array_unique(array_merge($ids, $_ids));
          } else {
            $ids = $tmp;
          }
        }
      }
    } else {
      $ids = Db::table("lib_search_indexes")
        ->pluck("range_id")
        ->toArray();
    }

    if (!empty($sort)) {
      $items = ProductRange::whereIn("id", $ids)
        ->orderBy($sort["field"], $sort["order"])
        ->skip(35 * ($page - 1))
        ->limit(35)
        ->get();
    } else {
      $items = ProductRange::whereIn("id", $ids)
      ->skip(35 * ($page - 1))
      ->limit(35)
      ->get();
    }

    $pagination = ProductRange::whereIn("id", $ids)
      ->paginate(35);

    $suppliers = Supplier::whereIn("id", $suppliers)->get();
    $tags = Tag::whereIn("id", $tags)->get();

    $data = [
      "query" => $request->q,
      "words" => $words,
      "suppliers" => $suppliers,
      "tags" => $tags,
      "items" => $items,
      "pagination" => $pagination
    ];

    return view("library.search", $data);
  }

  private function siftItems($list, $results)
  {
    if (!empty($results)) {
      if (!empty($list)) {
        $tmp = $list;
        //$list = array_intersect($list, $results);
        $list = array_merge($list, $results);

        if (empty($list)) {
          $list = $tmp;
        }
      } else {
        $list = $results;
      }
    }

    return $list;
  }

  public function build()
  {
    $suppliers = Supplier::where("status", 1)->get();
    foreach ($suppliers as $supplier) {
      $products = SupplierProduct::where("status", 1)
        ->where("supplier_id", $supplier->getID())
        ->get();
      foreach ($products as $product)
      {
        $ranges = ProductRange::where("status", 1)
          ->where("product_id", $product->getID())
          ->get();
        foreach ($ranges as $range)
        {
          $index = SearchIndex::where("range_id", $range->getID())->first();
          if (empty($index)) {
            $index = new SearchIndex();
          }

          $index->setRangeID($range->getID());
          $index->setName($range->getName());
          $index->setSupplierName($supplier->getName());
          $index->setProductName($product->getName());
          $index->setBrand($product->getBrand());
          $index->setCategory($product->getCategoryValue());
          $index->setMinPrice($product->getMinPrice());
          $index->setMaxPrice($product->getMaxPrice());
          $index->setLeadTime($product->getLeadTime());
          if (!empty($product->tags)) {
            $tags = $product->tags()->where("status", 1)->pluck("tag_id")->toArray();
            $tags = Tag::where("status", 1)->whereIn("id", $tags)->pluck("phrase")->toArray();
            $index->setTags(implode(",", $tags));
          } else {
            $index->setTags("");
          }
          $index->save();
        }
      }
    }
  }
}
