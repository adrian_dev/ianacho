<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Supplier;
use App\Product;
use App\Tag;
use App\ProductTag;
use App\SupplierContact;
use App\SupplierProduct;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManager as Image;

class SupplierController extends Controller
{
  protected $guard = 'supplier';

  protected $auth;

  protected $categories = array(
    2 => 'finishes',
    3 => 'materials',
    4 => 'colors',
    5 => 'placement',
    6 => 'properties'
  );

  public function __construct()
  {
    $this->auth = Auth::guard($this->guard);
  }

  public function index()
  {
    $supplier = Supplier::find($this->auth->id());
    $products = $supplier->products()
      ->where('status', 1)
      ->orderBy('updated_at', 'DESC')
      ->get();

    $data = array(
      'supplier' => $supplier,
      'products' => $products,
      'list' => $this->products()
    );

    return view('supplier.profile', $data);
  }

  public function login()
  {
      return view('supplier.login');
  }

  public function authenticate(Request $request)
  {
    $this->validate($request, [
      'username' => 'required',
      'password' => 'required',
    ]);

    $fields = array(
      'username' => $request->username,
      'password' => $request->password
    );

    if ($this->auth->attempt($fields)) {
      return redirect()->route('profile');
    } else {
      return back()->withErrors(['message' => 'Username and password do not match']);
    }
  }

  public function upload(Request $request)
  {
    $this->validate($request, ['image' => 'required|image']);

    $folder = "images/".$this->auth->id();

    if (!File::exists($folder)) {
      File::makeDirectory($folder, 755, true);
    }

    $image = $request->file('image');
    $name = $image->getClientOriginalName();

    $request->file('image')->move($folder, $name);

    $src = $folder.'/'.$name;
    $thumb = $folder.'/thumb_'.$image->getClientOriginalName();

    list($width, $height) = getimagesize($src);

    $myImage = imagecreatefromjpeg($src);

    if ($width > $height) {
      $y = 0;
      $x = ($width - $height) / 2;
      $smallestSide = $height;
    } else {
      $x = 0;
      $y = ($height - $width) / 2;
      $smallestSide = $width;
    }

    $thumbSize = 250;
    $th = imagecreatetruecolor($thumbSize, $thumbSize);

    imagecopyresampled($th, $myImage, 0, 0, $x, $y, $thumbSize, $thumbSize, $smallestSide, $smallestSide);

    imagejpeg($th, $thumb);

    return ['src' => $src, 'thumb' => $thumb];
  }

  public function update(Request $request)
  {
    $this->validate($request, [
      'name' => 'required',
      'contact_person' => 'required|array',
      'contact_number' => 'required|array',
      'contact_email' => 'required|array'
    ]);

    $id = $this->auth->id();
    $supplier = Supplier::find($id);

    if ($supplier) {
      $supplier->setName($request->name);
      $supplier->setAddress($request->address);
      $supplier->setWebsite($request->website);
      $supplier->save();

      SupplierContact::where("supplier_id", $supplier->getID())->update(["status" => 0]);

      foreach ($request->contact_person as $i => $person) {
        if (isset($request->contact_id[$i])) {
          $contact = SupplierContact::find($request->contact_id[$i]);
        } else {
          $contact = new SupplierContact();
          $contact->setSupplierID($supplier->getID());
        }

        $contact->setName($person);
        $contact->setPhone($request->contact_number[$i]);
        $contact->setEmail($request->contact_email[$i]);
        $contact->setStatus(1);
        $contact->save();
      }

      return array('success' => true);
    }

    return array('error' => 'save failed');
  }

  public function set(Request $request)
  {
    if (empty($request->price)) {
      $request->request->set('price', 0);
    }

    $this->validate($request, [
      'image' => 'required',
      'name'  => 'required',
      'description' => 'required',
      'price' => 'numeric'
    ]);

    if (!$request->has('green_mark')) {
      $request->request->add(['green_mark' => 0]);
    }

    if (isset($request->id)) {
      $product = Product::find($request->id);

      if (!empty($product)) {
        $product->setImage($request->image);
        $product->setThumbnail($request->thumbnail);
        $product->setName($request->name);
        $product->setCode($request->code);
        $product->setRange($request->range);
        $product->setLeadTime($request->lead_time);
        $product->setPrice(empty($request->price) ? 0 : $request->price);
        $product->setDimensions($request->dimensions);
        $product->setGreenMark($request->green_mark);
        $product->setDescription($request->description);
        $product->setBrand($request->brand);
        $product->save();

        Supplier::find($this->auth->id())->touch();

        if (isset($request->tags) && !empty($request->tags)) {
          foreach ($request->tags as $tag) {
            $pt = ProductTag::where('product_id', $request->id)
              ->where('tag_id', $tag)
              ->first();

            if (empty($pt)) {
              $pt = new ProductTag();
              $pt->setProductID($request->id);
              $pt->setTagID($tag);
              $pt->save();
            } else {
              $pt->setStatus(1);
              $pt->save();
            }
          }

          ProductTag::where('product_id', $request->id)
            ->whereNotIn('tag_id', $request->tags)
            ->update(['status' => 0]);
        }

        return redirect()->route('profile')
          ->with('message', "<b>{$product->getName()}</b> successfully updated!");
      }
    } else {
      $product = new Product();

      $result = $product->where('name', $request->name)
        ->where('supplier_id', $this->auth->id())
        ->first();

      if (empty($result)) {
        $product->setSupplierID($this->auth->id());
        $product->setImage($request->image);
        $product->setThumbnail($request->thumbnail);
        $product->setDescription($request->description);
        $product->setName($request->name);
        $product->setCode($request->code);
        $product->setRange($request->range);
        $product->setLeadTime($request->lead_time);
        $product->setPrice(empty($request->price) ? 0 : $request->price);
        $product->setDimensions($request->dimensions);
        $product->setGreenMark(isset($request->green_mark) ? $request->green_mark : 0);
        $product->setBrand($request->brand);
        $product->save();

        Supplier::find($this->auth->id())->touch();

        if (isset($request->tags) && !empty($request->tags)) {
          foreach ($request->tags  as $tag) {
            $pt = new ProductTag();
            $pt->setProductID($product->getID());
            $pt->setTagID($tag);
            $pt->save();
          }
        }

        return redirect()
          ->route('profile')
          ->with('message', 'new product added');
      } else {
        return redirect()
          ->back()
          ->withErrors(['product name already taken']);
      }
    }
  }

  public function getProducts()
  {

  }

  public function edit()
  {

  }

  public function add()
  {
    $tags = array();

    foreach ($this->categories as $i => $category) {
      $_tags = Tag::where('category', $i)
        ->where('status', 1)
        ->get();

      if (!$_tags->isEmpty()) {
        $tags[$i] = $_tags;
      }
    }

    $data = array(
      'tags'       => $tags,
      'categories' => $this->categories
    );

    return view('supplier.new', $data);
  }

  public function productView($id)
  {
    $product = Product::find($id);
    return view('supplier.product', ['product' => $product]);
  }

  public function productEdit($id)
  {
    $product = Product::find($id);
    $pt      = ProductTag::where('product_id', $id)
      ->where('status', 1)
      ->get();
    $tagIDs  = array();

    foreach ($pt as $tag) {
      $tagIDs[] = $tag->getTagID();
    }

    $tags = array();

    foreach ($this->categories as $i => $category) {
      $_tags = Tag::where('category', $i)
        ->where('status', 1)
        ->get();

      if (!$_tags->isEmpty()) {
        $tags[$i] = $_tags;
      }
    }

    $data = array(
      'product'       => $product,
      'productTagIDs' => $tagIDs,
      'tags'          => $tags,
      'categories'    => $this->categories
    );

    return view('supplier.new', $data);
  }

  public function updateLogo(Request $request)
  {
      $folder = "images/".$this->auth->id();

      if (!File::exists($folder)) {
          File::makeDirectory($folder, 755, true);
      }

      $filename = 'supplier_logo.'.$request->file('logo')->getClientOriginalExtension();

      $request->file('logo')->move($folder, $filename);

      $src = $folder.'/'.$filename.'?flag='.time();

      $supplier = Supplier::find($this->auth->id());
      $supplier->setLogo($src);
      $supplier->save();

      return array('src' => $folder.'/'.$filename.'?flag='.time());
  }

  public function logout()
  {
    $this->auth->logout();
    return view('supplier.login')->with('Message', 'successfully logged out');
  }

  public function product()
  {
    $tags = array();
    foreach ($this->categories as $i => $category) {
      $_tags = Tag::where('category', $i)
        ->where('status', 1)
        ->orderBy('phrase', 'ASC')
        ->get();

      if (!$_tags->isEmpty()) {
        $tags[$i] = $_tags;
      }
    }

    $data = array(
      'tags'       => $tags,
      'categories' => $this->categories
    );
    return view('supplier.forms.product', $data);
  }

  private function products()
  {
    $id = $this->auth->user()->getID();

    $records = SupplierProduct::where("supplier_id", $id)
      ->where("status", 1)
      ->orderBy("updated_at", "desc")
      ->get();

    $products = array();
    $brands = array("");
    $categories = array("");
    $grouping = [["name" => "", "categories" => [""]]];

    if (!$records->isEmpty()) {
      foreach($records as $record) {
        $brand = $record->getBrand();
        if (!in_array($brand, $brands)) {
          $brands[] = $brand;
        }

        $brandKey = array_search($brand, $brands);
        $grouping[$brandKey]["name"] = $brand;

        $category = unserialize($record->getCategory());
        if (!in_array($category["value"], $categories)) {
          $categories[] = $category["value"];
        }

        $catKey = array_search($category["value"], $categories);
        $grouping[$brandKey]["categories"][$catKey] = $category["value"];

        $gallery = unserialize($record->getGallery());

        if (!empty($gallery)) {
          $thumbnail = reset($gallery);
          $thumbnail = $thumbnail["thumbnail"];
        } else {
          $thumbnail = $record->range()
            ->where("status", 1)
            ->orderBy("updated_at", "desc")
            ->first();

          if (!empty($thumbnail)) {
            $thumbnail = $thumbnail->getThumbnail();
          } else {
            $thumbnail = "http://via.placeholder.com/170x170";
          }
        }

        $products[$brandKey][$catKey][] = array(
          "name" => $record->getName(),
          "thumbnail" => $thumbnail,
          "link" => route("product.get", ["id" => $record->getID()])
        );
      }
    }

    return ["labels" => $grouping, "products" => $products];
  }

  public function removeContact($id)
  {
    $contact = SupplierContact::find($id);
    $contact->delete();

    return redirect()->route("profile")->with("message", "Contact successfully deleted");
  }
}
