<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\UserProduct;
use App\UserFavorite;
use App\FavoriteProduct;
use App\Product;

class FavoritesController extends Controller
{
  public function index()
  {
    $IDs = UserProduct::where('user_id', Auth::id())
      ->where('status', 1)
      ->get(['product_id'])
      ->pluck('product_id')
      ->toArray();

    if (!empty($IDs)) {
      return Product::whereIn('id', $IDs)
        ->where('status', 1)
        ->get();
    } else {
      return ['error' => 'empty'];
    }
  }

  public function products()
  {
    $products = $this->getMyProducts();
    if (empty($products)) {
      return ['error' => 'empty'];
    } else {
      return $products;
    }
  }

  public function set($id)
  {
    $myProduct = UserProduct::where('user_id', Auth::id())
      ->where('product_id', $id)
      ->first();

    if (empty($myProduct)) {
      $myProduct = new UserProduct();
      $myProduct->setUserID(Auth::id());
      $myProduct->setProductID($id);
      $myProduct->save();

      return $this->getMyProducts();
    } else if ($myProduct->getStatus() == 0) {
      $myProduct->setStatus(1);
      $myProduct->save();

      return $this->getMyProducts();
    } else {
      return ['error' => 'existing'];
    }
  }

  public function delete($id)
  {
    $myProduct = UserProduct::where('user_id', Auth::id())
      ->where('product_id', $id)
      ->first();

    if (empty($myProduct)) {
      return ['error' => 'missing record'];
    } else {
      $myProduct->setStatus(0);
      $myProduct->save();

      return $this->getMyProducts();
    }
  }

  public function getFavorites()
  {
    return UserFavorite::where('status', 1)
      ->where('user', Auth::id())
      ->get();
  }

  public function getFavoriteProducts($id)
  {
    return FavoriteProduct::where('status', 1)
      ->where('favorite_id', $id)
      ->get();
  }

  private function getMyProducts()
  {
    $collection = UserProduct::where('user_id', Auth::id())
      ->where('status', 1)
      ->orderBy('updated_at', 'DESC')
      ->limit(8)
      ->get();

    $products = array();

    foreach ($collection as $item) {
      $products[] = $item->product;
    }

    return $products;
  }
}
