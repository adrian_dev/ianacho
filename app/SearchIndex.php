<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SearchIndex extends Model
{
  protected $table = 'lib_search_indexes';

  public $timestamps = false;

  protected $fillable = array(
    'range_id',
    'name',
    'supplier_name',
    'product_name',
    'brand',
    'category',
    'min_price',
    'max_price',
    'lead_time',
    'tags'
  );

  public function getID()
  {
    return $this->id;
  }

  public function getRangeID()
  {
    return $this->range_id;
  }

  public function setRangeID($value)
  {
    $this->range_id = $value;
  }

  public function getName()
  {
    return $this->name;
  }

  public function setName($value)
  {
    $this->name = $value;
  }

  public function getSupplierName()
  {
    return $this->supplier_name;
  }

  public function setSupplierName($value)
  {
    $this->supplier_name = $value;
  }

  public function getProductrName()
  {
    return $this->product_name;
  }

  public function setProductName($value)
  {
    $this->product_name = $value;
  }

  public function getBrand()
  {
    return $this->brand;
  }

  public function setBrand($value)
  {
    $this->brand = $value;
  }

  public function getCategory()
  {
    return $this->category;
  }

  public function setCategory($value)
  {
    $this->category = $value;
  }

  public function getMinPrice()
  {
    return $this->min_price;
  }

  public function setMinPrice($value)
  {
    $this->min_price = $value;
  }

  public function getMaxPrice()
  {
    return $this->max_price;
  }

  public function setMaxPrice($value)
  {
    $this->max_price = $value;
  }

  public function getLeadTime()
  {
    return $this->lead_time;
  }

  public function setLeadTime($value)
  {
    $this->lead_time = $value;
  }

  public function getTags()
  {
    return $this->tags;
  }

  public function setTags($value)
  {
    $this->tags = $value;
  }
}
