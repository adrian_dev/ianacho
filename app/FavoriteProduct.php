<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FavoriteProduct extends Model
{
    protected $table = "lib_favorite_products";

    protected $fillable = [
      'favorite_id',
      'user_id',
      'product_id',
      'status'
    ];

    public function getID()
    {
      return $this->id;
    }

    public function getFacoriteID()
    {
      return $this->favorite_id;
    }

    public function setFavoriteID($value)
    {
      $this->favorite_id = $value;
    }

    public function getUserID()
    {
      return $this->user_id;
    }

    public function setUserID($value)
    {
      $this->user_id = $value;
    }

    public function getProductID()
    {
      return $this->product_id;
    }

    public function setProductID($value)
    {
      $this->product_id = $value;
    }

  public function getStatus()
  {
    return $this->status;
  }

  public function setStatus($value)
  {
    $this->status = $value;
  }

  public function product()
  {
    return $this->hasOne('App\Product', 'id', 'product_id');
  }
}
