var $name, $range = [], $brand, $category = [], $price = [], $lead, $green, $desc, $tags = [], $gallery = [];
var $brands, $categories;
var $galleryCount = 0;

var $ck = CKEDITOR.replace('product-desc', {
  customConfig: '',
  toolbar: [
    { name: 'clipboard', items: [ 'Cut', 'Copy', 'Paste', 'PasteText', '-', 'Undo', 'Redo' ] },
    { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline' ] },
    { name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent' ] },
    { name: 'links', items: [ 'Link', 'Unlink' ] }
  ]
});

Dropzone.options.dropzone = {
  paramName: "image", // The name that will be used to transfer the file
  maxFilesize: 1, // MB
  acceptedFiles: 'image/jpeg, image/png',
  dictDefaultMessage: $('#drop-message').html(),
  accept: function(file, done) { done(); },
  success: appendToRange,
  complete: function(file) { this.removeFile(file); },
  queuecomplete: function() {
    $("#dropzone").slideUp();
    $("#product-range").append($("#range-add").html());
    $(".open-range-uploader a").click(function() {
    $("#dropzone").slideDown();
    $(this).parent().remove();

    window.scrollTo(0, 350);
  });
  }
};

Dropzone.options.dropgallery = {
  paramName: "image", // The name that will be used to transfer the file
  maxFilesize: 1, // MB
  acceptedFiles: 'image/jpeg, image/png',
  dictDefaultMessage: $('#dropgallery-message').html(),
  maxFiles: 3,
  accept: function(file, done) { done(); },
  success: function(file, response) {
    $galleryCount++;

    appendToGallery(file, response);

    if ($galleryCount >= 3) {
      $("#dropgallery").hide();
      $("#product-gallery").show();
    }
  },
  thumbnailWidth: 80,
  thumbnailHeight: 80,
  queuecomplete: function() {
    var $inst = this;
    setTimeout(function() {
      $inst.removeAllFiles();
    }, 3000);
  }
};

function submitForm(btn) {
  var $params;

  if ($(btn).hasClass("disabled")) {
    return false;
  }

  $name = $("[name=name]").val();

  $.each($("#product-range .column:not(.remove-range)"), function($i, $e) {
    $range.push({
      id: $($e).find("[name=range_id]").val(),
      name: $($e).find(".range-name").val(),
      code: $($e).find(".range-code").val(),
      image: $($e).find("[name=image]").val(),
      tags: $($e).find("[name=tags]").val(),
      thumb: $($e).find("img").attr("src").replace($("base").attr("href")+"/", "")
    });
  });

  if ($name.length < 3 || $range.length <= 0) {
    return false;
  }

  $brand = $("[name=brand]").val();
  $category = {
    label: $("#details-category").text(),
    value: $("[name=category]").val()
  };

  $price = {
    min: $("[name=min]").val(),
    max: $("[name=max]").val()
  };

  $lead = $("#lead-time").val();
  $green = $("[name=green]").is(":checked") ? 1 : 0;
  $desc = $ck.getData();

  $.each($("#product-gallery .column"), function($i, $e) {
    if ($i > 2) {
      return true;
    }

    $gallery.push({
      image: $($e).find("[type=hidden]").val(),
      thumbnail: $($e).find("img").attr("src").replace($("base").attr("href")+"/", "")
    });
  });

  $params = {
    name: $name,
    range: $range,
    brand: $brand,
    category: $category,
    price: $price,
    lead_time: $lead,
    green_mark: $green,
    description: $desc,
    gallery: $gallery,
    tags: $tags,
    '_token': $('meta[name="csrf-token"]').attr('content'),
    id: $("[name=product]").val()
  };

  $.ajax({
    url: postURL,
    type: "post",
    data: $params,
    beforeSend: function() {
      $("#product-submit").parent().addClass("hide");
      $(".loader-img").removeClass("hide");
    },
    success: function(response) {
      top.location.href = response;
    },
    error: function(response) {
      $("#product-submit").parent().removeClass("hide");
      $(".loader-img").addClass("hide");
      $(".product-error").tml(response.error).removeClass("hide");
      console.log(response);
    }
  });

  return true;
}

$(function() {
  if ($("[name=name]").val().length == 0) {
    $("#product-submit").addClass("disabled");
  }

  if ($("#product-range").children().length == 0) {
    $("#product-submit").addClass("disabled");
  }

  var $form = $("form[name=product]");
  $form.children("input").focus(function() {
    $(this).removeClass("error").next().removeClass("visible");
  });

  $("[name=name]").keyup(function() {
    if ($(this).val().length > 2 && $("#product-range").children().length > 0) {
      $("#product-submit").removeClass("disabled");
    } else {
      $("#product-submit").addClass("disabled");
    }
  });

  $("#add-detail-btn").click(function() {
    $('#details-box').slideDown();
    $(this).slideUp('fast');
  });

  $("#detail-box-toggler").click(function() {
    $('#details-sub-box').slideToggle('fast');
    $(this).find('i').toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
  });

  $.each($("#current-tags a"), function(i, e) {
    $tags.push(parseInt($(e).data("id")));
    bindTagRemove(e);
  });

  $.ajax({
    url: $("base").attr("href")+"/api/supplier/brands",
    type: "get",
    success: function(data) {
      $brands = data;
    }
  });

  $.ajax({
    url: $("base").attr("href")+"/api/supplier/categories",
    type: "get",
    success: function(data) {
      $categories = data;
    }
  });

  /*$("[name=brand]").keyup(function() {
    var $tmp = $(this).val();
    $("#suggestor").html("");

    if ($tmp.length > 3) {
      $.each($brands, function(i, v) {
        if (v.indexOf($tmp) >= 0) {
          $("#suggestor").append('<a onclick="$(\'[name=brand]\').val($(this).text());$(this).parent().hide();">'+v+'</a>');
        }
      });
    }
  });*/

  $("#product-gallery .column .card").click(function() {$(this).parent().remove();});
});

function changeCategoryLabel($i) {
  var $cat = ["Other", "Category", "Collection", "Series", "Family", "Line", "Group"];

  $("#details-category").text($cat[$i]);
  $("form[name=product]").children("[name=parent]").val($i);
  $("#details-category").click();
}

function appendToRange(file, response) {
  var $item = $("#range-item");
  $("#product-range").next().addClass("hide");
  $("#product-range").append($item.html());
  $("#product-range .column").last().find("img").attr("src", $("base").attr("href")+"/"+response.thumb);
  $("#product-range .column").last().find(".range-name").val(response.name);
  $("#product-range .column").last().find("[name=image]").val(response.raw);

  if ($("#product-range").children().not(".remove-range").length > 0 && $("[name=name]").val().length > 2) {
    $("#product-submit").removeClass("disabled");
  } else {
    $("#product-submit").addClass("disabled");
  }
}

function appendToGallery(file, response) {
  $("#product-gallery").append('<div class="column"><div class="card" onclick="$(this).parent().remove();"><input type="hidden" value="'+response.raw+'"><img src="'+$("base").attr("href")+"/"+response.thumb+'"></div></div>');
}

function toggleTag(e) {
  var $id = $(e).attr("id").replace("tag-", "");

  if ($.inArray($id, $tags) >= 0) {
    if ($(e).is(":checked")) {
      return true;
    } else {
      var $i = $tags.indexOf($id);
      $tags.splice($i, 1);
    }
  } else {
    $tags.push(parseInt($id));
  }

  buildCurrentTags();
}

function buildCurrentTags() {
  $("#current-tags").html("");

  $.each($tags, function($e, $n) {
    $("#current-tags").append('<a src="javascript:void(0);" data-id="'+$n+'">'+$("#tags-selection label[for=tag-"+$n+"]").text()+' <i class="fa fa-times"></i></a>');
    bindTagRemove($("#current-tags a[data-id="+$n+"]"));
  });
}

function bindTagRemove(e) {
  $(e).click(function() {
    var $i = $tags.indexOf(parseInt($(this).data("id")));
    $tags.splice($i, 1);
    $("#tag-"+$(this).data("id")).attr("checked", false);
    buildCurrentTags();
  });
}

function removeGrandpa(e) {
  $(e).parent().parent().remove();

  if ($("#product-range").children().not(".remove-range").length > 0 && $("[name=name]").val().length > 2) {
    $("#product-submit").removeClass("disabled");
  } else {
    $("#product-submit").addClass("disabled");

    if ($("#product-range").children().not(".remove-range").length <= 0) {
      $("#dropzone").slideDown();
      $(".remove-range").remove();
    }
  }
}

