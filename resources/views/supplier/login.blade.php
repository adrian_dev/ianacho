@extends('template')
@section('title', 'Supplier Login')
@section('content')
<div class="row">
  <div class="column medium-4 medium-offset-2">
    <br/><br />
    <video width="100%" height="auto" controls style="border: 1px solid #000;">
      <source src="{{ asset('videos/tutorial.mp4') }}" type="video/mp4">
      Your browser does not support the video tag.
    </video>
  </div>
    <div class="column large-4 end">
        <br /><br />
        <h2 class="text-center">Supplier Portal</h2>

        @if (isset($errors))
          <ul class="error">
            @foreach($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        @endif

        <form method="post" action="{{ route('authorize') }}">
            {{ csrf_field() }}
            <input type="text" name="username" placeholder="username" />
            <input type="password" name="password" />

            <input type="submit" value="LOGIN" class="button expanded" />
            <hr />
        </form>
    </div>
</div>
@endsection
