<?php
if (isset($product)) {
  $image       = $product->getImage();
  $thumbnail   = $product->getThumbnail();
  $name        = $product->getName();
  $range       = $product->getRange();
  $code        = $product->getCode();
  $description = $product->getDescription();
  $dimensions  = $product->getDimensions();
  $price       = $product->getPrice();
  $green       = $product->getGreenMark();
  $brand       = $product->getBrand();
  $lead_time   = $product->getLeadTime();
} else {
  $image         = old('image');
  $thumbnail     = old('thumbnail');
  $name          = old('name');
  $range         = old('range');
  $code          = old('code');
  $description   = old('description');
  $dimensions    = old('dimensions');
  $price         = old('price');
  $green         = old('green_mark');
  $brand         = old('brand');
  $lead_time     = old('lead_time') ? old('lead_time') : 12;
  $productTagIDs = array();
}
?>
@extends('template')
@section('title', 'Add New')
@section('content')
<div class="row">
  <h4 class="column">Product</h4>
  <div class="column end">
    <ul class="error">
      @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
</div>

<form id="new" method="post" action="{{ route('save') }}">
  <div class="row">
    {{ csrf_field() }}

    @if(isset($product))
      <input type="hidden" name="id" value="{{ $product->id }}" />
    @endif

    <input type="hidden" name="image" value="{{ $image }}" />
    <input type="hidden" name="thumbnail" value="{{ $thumbnail }}" />

    <div class="column large-1 large-offset-9">
      <a href="{{ route('profile') }}" class="button hollow secondary">CANCEL</a>
    </div>
    <div class="column large-2 submit sticky">
      <input type="submit" class="button expanded" value="SAVE" />
    </div>

    <div class="columns large-3 product-image">
      @if(!empty($image))
        <img src="{{ asset($image) }}" />
      @endif
      <label for="image" class="button expanded secondary">Upload New Image</label>
    </div>

    <div class="columns large-9">
      <div class="row">
      <div class="column large-6">
        <h6>Brand:</h6>
          <input type="text" name="brand" value="{{ $brand }}" />
        </div>
        <div class="column large-6">
          <h6>Product Name:</h6>
          <input type="text" name="name" placeholder="product name/code" value="{{ $name }}" required maxlength="50" />
        </div>
        <div class="column large-6">
          <h6>Range/Color/Pattern:</h6>
          <input type="text" name="range" value="{{ $range }}" />
        </div>
        <div class="column large-6">
          <h6>Code:</h6>
          <input type="text" name="code" value="{{ $code }}" />
        </div>
        <div class="column large-6">
          <h6>Price:</h6>
          <input type="number" name="price" value="{{ $price }}" placeholder="0000" />
        </div>
        <div class="column large-6">
          <h6>Lead Time:</h6>
          <div class="row">
            <div class="large-7 columns">
              <div class="slider" data-slider data-initial-start="{{ $lead_time }}" data-end="12" data-step="1">
                <span class="slider-handle"  data-slider-handle role="slider" tabindex="1" aria-controls="sliderOutput2"></span>
                <span class="slider-fill" data-slider-fill></span>
              </div>
            </div>
            <div class="large-5 columns">
              <div class="input-group">
                <input class="input-group-field" name="lead_time" type="text" value="{{ $lead_time }}" id="sliderOutput2" />
                <span class="input-group-label">Weeks</span>
              </div>
            </div>
          </div>
        </div>
        <div class="column">
          <h6>Description:</h6>
          <textarea name="description" rows="5" required>{{ $description }}</textarea>
        </div>
        <div class="column large-6">
          <h6>Dimensions:</h6>
          <input type="text" name="dimensions" value="{{ $dimensions }}" />
        </div>
        <div class="column large-6 end">
          <label for="green">
            <input type="checkbox" name="green_mark" value="1" id="green" {{ $green == 1 ? 'checked' : '' }} />
            Green Product
          </label>
        </div>
      </div>
    </div>
  </div>
  <br />
  <br />
  <div class="row">
    <div class="column large-10 large-offset-1 tags">
      <br />
      <h5>Tags</h5>

      <div class="row">
        @foreach ($categories as $i => $category)
          <div class="button-group column large-6 {{ $i == 6 ? 'end' : '' }}">
            <h6>{{ ucfirst($category) }}:</h6>
            <div class="contain-tags">
              @if (isset($tags[$i]))
                @foreach($tags[$i] as $tag)
                  <label for="tag-{{ $tag->getID() }}" class="button hollow secondary {{ in_array($tag->getID(), $productTagIDs) ? 'on' : '' }}">
                    <input type="checkbox" name="tags[]" value="{{ $tag->getID() }}" id="tag-{{ $tag->getID() }}" {{ in_array($tag->getID(), $productTagIDs) ? 'checked' : '' }} />
                    {{ $tag->getPhrase() }}
                  </label>
                @endforeach
              @endif
            </div>
          </div>
        @endforeach
      </div>
      <br />
    </div>
  </div>
</form>

<form id="upload" method="post" action="{{ route('upload') }}" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="file" name="image" id="image" />
</form>
@endsection
