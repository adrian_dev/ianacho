@extends('template')

@section('title', 'Profile')

@section('content')
  <div class="info" data-action="{{ route('update') }}" data-token="{{ csrf_token() }}">
    <div class="row expanded" style="margin-top:1rem;">
      <div class="column large-9">
        <h2>Supplier Profile</h2>
      </div>
      <div class="column large-3">
        <a href="{{ route('supplier.product') }}" class="button expanded large">
          <i class="fa fa-plus"></i> Add New Product
        </a>
      </div>

      <div class="large-3 columns">
        <aside id="sidebar" class="logo">
          <label for="logo">
            <img src="{{ empty($supplier->getLogo()) ? asset('images/logo_placeholder.png') : asset($supplier->getLogo()) }}" width="100%" />
            <span class="button expanded secondary">UPDATE</span>
          </label>
        </aside>
      </div>

      <div class="large-9 columns">
        <table id="supplier-form" class="hover stack unstriped" cellspacing="0">
            <tbody>
              <tr>
                <td colspan="2" class="supplier-form-buttons">
                  <a href="{{ route('logout') }}" class="button hollow secondary float-right">Logout</a>
                  <a href="javascript:void(0);" class="edit button float-left">EDIT INFORMATION</a>
                  <a href="javascript:void(0);" class="save-edit button float-left">SAVE</a>
                </td>
              </tr>
              <tr>
                <th>Name</th>
                <td><input type="text" name="name" value="{{ $supplier->getName() }}" disabled /></td>
              </tr>
              <tr>
                <th>Address</th>
                <td><textarea name="address" disabled rows="4">{{ $supplier->getAddress() }}</textarea></td>
              </tr>
              <tr>
                <th>Website</th>
                <td><input type="text" name="website" value="{{ $supplier->getWebsite() }}" disabled /></td>
              </tr>
            </tbody>
        </table>
        <table class="unstriped" cellspacing="0">
          <tbody>
            <tr>
              <td colspan="2">
                <h5>Contact Person</h5>
              </td>
            </tr>
            <tr>
              <td colspan="2">
                <section class="contacts row">
                  @include('admin.forms.contact', ['contacts' => isset($supplier) ? $supplier->contacts()->where('status', 1)->get() : []])
                </section>
                <div class="row">
                  <div class="column large-6">
                    <a href="javascript:void(0);" onclick="addContactForm();" id="add-contact" class="button hollow secondary expanded hide">+ADD CONTACT</a>
                  </div>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <form id="new-logo" class="hide" method="post" action="{{ route('logo.update') }}" enctype="multipart/form-data">
      {{ csrf_field() }}
      <input type="file" name="logo" id="logo" />
  </form>

  @include("supplier.widgets.products", ["list" => $list])
@endsection
@section('scripts')
  @parent
  <script type="text/javascript" src="{{ asset('js/contact.js') }}"></script>
@endsection
