@extends('product')

@section('title', $title)

@section('css')
  @parent
  <link rel="stylesheet" href="{{ asset('css/vendor/dropzone.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/vendor/jquery.fancybox.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/product-form.css') }}">
@endsection

@section('js')
  @parent
  <script type="text/javascript" src="{{ asset('js/dropzone.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/ckeditor.js') }}"></script>
  <script src="{{ asset('js/vendor/jquery.fancybox.min.js') }}"></script>
  @include('supplier.widgets.script_templates')
@endsection

@section('content')
  <div class="row hide product-error">
    <div class="column">
      <div class="callout alert">
        Something went wrong with saving the product please try again.
      </div>
    </div>
  </div>
  <div class="row">
    <div class="column row">
      <div class="column large-2">
        <a href="{{ route('profile') }}" class="button hollow secondary">
          <i class="fa fa-chevron-left"></i> Back
        </a>
      </div>
      <div class="column large-8 text-center">
        <h3>Add Product</h3>
      </div>
      <div class="column large-2 text-right">
        @if (!empty($id))
          <a href="{{ route('supplier.product', ['brand' => $details['brand'], 'label' => $details['category']['label'], 'category' => $details['category']['value']]) }}" class="button secondary hollow">
            <i class="fa fa-plus"></i> Add New
          </a>
        @endif
      </div>
    </div>

    <div class="column large-8" id="product-left-side">
      <div class="row">
        <div class="column">
          <div class="card">
            <div class="card-divider">
              <h6><strong>BASIC INFORMATION</strong></h6>
            </div>
            <div class="card-section">
              @if (!empty($id))
                <input type="hidden" name="product" value="{{ $id }}">
              @endif
              <label>Name: <span class="required">(required)</span></label>
              <input type="text" name="name" maxlength="50" placeholder="Wavy Wavy Carpet Name" required autofocus value="{{ $name }}">
              <div class="error-message">Product name must be at least 3 characters.</div>

              <label>Range Images: <span class="required">(required)</span></label>
              <form action="{{ route('upload.range') }}" id="dropzone" class="dropzone">
                {{ csrf_field() }}
              </form>
              <div id="product-range" class="row large-up-3">
                @if (!empty($range))
                  @foreach ($range as $item)
                    <div class="column text-center">
                      <input type="hidden" name="range_id" value="{{ $item->getID() }}">
                      <input type="hidden" name="image" value="{{ $item->getImage() }}">
                      <input type="hidden" name="tags">
                      <img src="{{ asset($item->getThumbnail()) }}">
                      <div class="range-btns">
                        <a href="javascript:void(0);" onclick="removeGrandpa(this);">
                          <i class="fa fa-times"></i> Remove
                        </a>
                      </div>
                      <input type="text" class="range-name" placeholder="Range Name" maxlength="100" value="{{ $item->getName() }}">
                      <input type="text" class="range-code" placeholder="Range Code" maxlength="50" value="{{ $item->getCode() }}">
                    </div>
                  @endforeach
                @endif
              </div>
            </div>
          </div>
        </div>

        @if (empty($name))
          <div class="column large-4 large-offset-4">
            <a class="button secondary expanded hollow" href="javascript:void(0);" id="add-detail-btn">
              <i class="fa fa-file-text"></i> Add Details
            </a>
          </div>
        @endif

        @include('supplier.widgets.details', ["details" => $details])
      </div>
    </div>

    <div class="column large-4">
      <div class="row">
        <div class="column">
          <button class="button expanded large {{ empty($name) || empty($range) ? 'disabled' : '' }}" id="product-submit" onclick="submitForm(this);">
            <i class="fa fa-save"></i> SAVE
          </button>
        </div>

        <div class="column text-center hide loader-img">
          <img src="{{ asset('img/loader.gif') }}">
        </div>

        <div class="column">
          <div class="card">
            <div class="card-divider"><h6><b>Gallery</b></h6></div>

            <form id="dropgallery" class="dropzone" action="{{ route('upload.gallery') }}" method="post">
              {{ csrf_field() }}
            </form>

            <div id="product-gallery" class="row large-up-3" {{ empty($gallery) ? '"product_id" => $id, "tag_id" => $tag' : "" }}>
              @if (!empty($gallery))
                @foreach ($gallery as $pic)
                  <div class="column">
                    <div class="card">
                      <input type="hidden" value="{{ $pic['image'] }}">
                      <img src="{{ asset($pic['thumbnail']) }}">
                    </div>
                  </div>
                @endforeach
              @endif
            </div>

            <div class="card-section">
              <div class="guide">
                Gallery images are pictures of the product applied in a room or scene. You can upload a maximum of 3 photos.
              </div>
            </div>
          </div>
        </div>

        <div class="column">
          <div class="card">
            <div class="card-divider"><h6><b>Tags</b></h6></div>

            <div class="text-center">
              <a href="javascript:void(0);" id="tag-toggler" class="button secondary hollow" data-fancybox data-src="#tags-selection">
                <i class="fa fa-tags"></i> Add Tags
              </a>
            </div>
            <div class="card-section" id="current-tags">
              @foreach ($tagsList as $tag)
                <a src="javascript:void(0);" data-id="{{ $tag->record->getID() }}">
                  {{ $tag->record->getPhrase() }} <i class="fa fa-times"></i>
                </a>
              @endforeach
            </div>
          </div>
        </div>

        <div class="column">
          @if (!empty($id))
            <button class="button secondary expanded" id="confirm-delete">DELETE PRODUCT</button>
          @endif
        </div>
      </div>
    </div>
  </div>

  <div class="dropdown-pane" id="drop-category" data-dropdown data-auto-focus="true">
    <ul class="lists">
      <li><a href="javascript:void(0);" onclick="changeCategoryLabel(1);">Category</a></li>
      <li><a href="javascript:void(0);" onclick="changeCategoryLabel(2);">Collection</a></li>
      <li><a href="javascript:void(0);" onclick="changeCategoryLabel(3);">Series</a></li>
      <li><a href="javascript:void(0);" onclick="changeCategoryLabel(4);">Family</a></li>
      <li><a href="javascript:void(0);" onclick="changeCategoryLabel(5);">Line</a></li>
      <li><a href="javascript:void(0);" onclick="changeCategoryLabel(6);">Group</a></li>
      <li><a href="javascript:void(0);" onclick="changeCategoryLabel(0);">Other</a></li>
    </ul>
  </div>

  @if (isset($tags) && isset($categories))
    @include('supplier.widgets.tags', ['tags' => $tags, 'categories' => $categories, "current" => $currentTags])
  @endif

  @if (isset($tags) && isset($categories))
    @include('supplier.widgets.colorpattern', ['tags' => $tags, 'categories' => $categories])
  @endif

  <!--div id="suggestor"></div-->
@endsection

@section('scripts')
  @parent
  <script>var postURL = "{{ route('product.add') }}";</script>
  <script type="text/javascript" src="{{ asset('js/product-form.js') }}"></script>
  @if (!empty($id))
    <script>
      $(function() {
        $("#confirm-delete").click(function() {
          if (confirm('Are you sure you want to delete?')) {
            window.location.replace('<?php echo route('product.delete', ['id'=>$id]); ?>');
          }
        });
      });
    </script>
  @endif
@endsection
