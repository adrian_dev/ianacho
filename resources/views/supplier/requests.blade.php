@extends('template')
@section('title', 'Requests')
@section('content')
  <div class="row">
    <div class="column">
      <h1>Requests</h1>
    </div>
  </div>
  <div id="requests"
    class="row small-up-2 medium-up-3 large-up-5"
    data-equalizer
    data-equalize-on="large">
    @if(!empty($products))
      @foreach ($products as $product)
        @include('supplier.widgets.card', ['product' => $product])
      @endforeach
    @endif
  </div>
@endsection
