<div class="row">
  <div class="column"><h3>Products</h3></div>

  @if (isset($list["products"]) && !empty($list["products"]))
    @foreach ($list["labels"] as $i => $label)
      <div class="column">
        <h4>{{ $label["name"] }}</h4>
        @if (isset($label["categories"]) && !empty($label["categories"]))
          @foreach ($label["categories"] as $a => $category)
            <h5>{{ $category }}</h5>
            <div class="row small-up-2 medium-up-5 large-up-7" data-equalizer="a">
              @if (isset($list["products"][$i][$a]))
                @foreach ($list["products"][$i][$a] as $product)
                  <div class="column">
                    <div class="card" data-equalizer-watch="a">
                      <a href="{{ $product['link'] }}" class="text-center" style="display:block;">
                        <img src="{{ asset($product['thumbnail']) }}">
                      </a>
                      <div class="card-section">
                        <a href="{{ $product['link'] }}">
                          <small>{{ $product['name'] }}</small>
                        </a>
                      </div>
                    </div>
                  </div>
                @endforeach
                <div class="column">
                  <div class="card text-center" data-equalizer-watch="a">
                    <a href="{{ route('supplier.product', ['brand' => $label['name'], 'category' => $category]) }}">
                      <i class="fa fa-plus" style="font-size:4rem;margin:2.5rem;"></i>
                    </a>
                    <div class="card-section">
                      <small>Add New Product</small>
                    </div>
                  </div>
                </div>
              @endif
            </div>
          @endforeach
        @endif
      </div>
    @endforeach
  @endif
</div>
