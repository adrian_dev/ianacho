<div id="tags-colorpattern" style="display:none;" data-equalizer data-equalize-on="large">
  <div class="guide">
    Tags will help make the product become more searcheable. Please click to higlight all of the words that are applicable to the product.
  </div>
  @if (isset($tags) && !empty($tags))
    <div class="row large-up-2">
      @foreach ($categories as $c => $category)
        <?php if (!in_array($category, ["finishes", "colors"])) { continue; } ?>
        <div class="column column-block">
          <div class="card">
            <h6 class="card-divider">{{ ucfirst($category) }}</h6>
            <div class="card-section text-center" style="max-height:350px;overflow-y:auto;" data-equalizer-watch>
              @foreach ($tags[$c] as $i => $tag)
                <input type="checkbox" id="tag-{{ $tag->getID() }}">
                <label for="tag-{{ $tag->getID() }}">{{ $tag->getPhrase() }}</label>
              @endforeach
            </div>
          </div>
        </div>
      @endforeach
      </div>
  @else
    <h5>No Tags Found</h5>
  @endif
</div>
