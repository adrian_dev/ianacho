<script type="text/template" id="drop-message">
  <i class="fa fa-image"></i>
  <h5>Drop images here to upload.</h5>
  <div><small>(Maximum file size is 2MB per image)</small></div>
  <br />
  <div class="row">
    <div class="column large-6 large-offset-3">
      <div class="explain text-left">
        <div><b>Instructions:</b></div>
        <ol>
          <li>Upload all range images of the product.</li>
          <li>Name the images with their color or pattern code.</li>
        </ol>
      </div>
    </div>
  </div>
</script>

<script type="text/template" id="dropgallery-message">
  <div class="text-center">
    <i class="fa fa-file-image-o"></i>
    <i class="fa fa-file-image-o"></i>
    <i class="fa fa-file-image-o"></i>
  </div>
  <h6>Drop images here to upload.</h6>
</script>

<script type="text/template" id="range-item">
  <div class="column text-center">
    <input type="hidden" name="image">
    <input type="hidden" name="tags">
    <img src="">
    <div class="range-btns">
      <!--a href="javascript:void(0);" data-fancybox data-src="#tags-colorpattern">
        <i class="fa fa-tag"></i>
      </a>
      |
      <a href="javascript:void(0);" onclick="removeGrandpa(this);">
        <i class="fa fa-times"></i>
      </a-->
      <a href="javascript:void(0);" onclick="removeGrandpa(this);">
        <i class="fa fa-times"></i> Remove
      </a>
    </div>
    <input type="text" class="range-name" placeholder="Range Name" maxlength="100">
    <input type="text" class="range-code" placeholder="Range Code" maxlength="50">
  </div>
</script>

<script type="text/template" id="range-add">
  <div class="column open-range-uploader text-center remove-range">
    <a href="javscript:void(0);">
      <img src="{{ asset('img/plus-symbol.svg') }}" width="250" height="250" />
    </a>
  </div>
</script>
