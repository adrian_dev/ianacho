<div class="column" style="{{ $details['price']['min'] === '' ? 'display:none;' : '' }}" id="details-box">
  <div class="card">
    <div class="card-divider">
      <div class="row">
        <div class="column large-11 small-11 medium-11">
          <h6><strong>DETAILS</strong></h6>
        </div>
        <div class="column large-1 small-1 medium-1 end">
          <a href="javascript:void(0);" class="float-right" id="detail-box-toggler">
            <i class="fa fa-chevron-up"></i>
          </a>
        </div>
      </div>
    </div>
    <div class="card-section" id="details-sub-box">
      <div class="row">
        <div class="row column">
          <div class="column large-4">
            <label>Brand</label>
            <p class="explain">
              Leave this blank if you are the supplier is also the manufacturer.
            </p>
          </div>
          <div class="column large-8">
            <input type="text" name="brand" placeholder="Manufacturer's Name" value="{{ $details["brand"] }}">
          </div>
        </div>
        <div class="row column">
          <div class="column large-4">
            <label>
              <a href="javascript:void(0);" data-toggle="drop-category" id="details-category">{{ $details["category"]["label"] }}</a>
              <i class="fa fa-pencil"></i>
            </label>
            <p class="explain">
              Fill in this field if the product is part of a collection or series. You can change the label by clicking the down arrow.
            </p>
          </div>
          <div class="column large-8">
            <input type="text" name="category" placeholder="Tropical Evening Breeze" maxlength="100" value="{{ $details["category"]["value"] }}">
          </div>
        </div>

        <div class="row column">
          <div class="column large-4">
            <label>Indicative Price</label>
            <p class="explain">
              For a base price please fill in the first box only. And for a price range fill in the first box as the minimum and the second box as the maximum value.
            </p>
          </div>
          <div class="column large-8">
            <div class="row">
              <!--div class="column small-1">$</div-->
              <div class="column large-3">
                <small class="explain">min</small>
                <input type="number" name="min" step="0.01" placeholder="1.00" value="{{ $details["price"]["min"] }}">
              </div>
              <!--div class="column small-1">-</div-->
              <div class="column large-3 end">
                <small class="explain">max</small>
                <input type="number" name="max" step="0.01" placeholder="100" value="{{ $details["price"]["max"] }}">
              </div>
            </div>
          </div>
        </div>

        <div class="row column">
          <div class="column large-4">
            <label>Lead Time</label>
          </div>
          <div class="column large-8">
            <div class="row">
              <div class="large-7 columns">
                <div class="slider" data-slider data-initial-start="{{ $details["lead_time"] }}" data-end="12" data-step="1">
                  <span class="slider-handle"  data-slider-handle role="slider" tabindex="1" aria-controls="lead-time"></span>
                  <span class="slider-fill" data-slider-fill></span>
                </div>
              </div>
              <div class="large-5 columns">
                <div class="input-group">
                  <input class="input-group-field text-center" name="lead" type="text" value="{{ $details["lead_time"] }}" id="lead-time" />
                  <span class="input-group-label">Weeks</span>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row column">
          <div class="column large-4">
            <p class="explain">
              This pertains to product ratings like green label, ISO:4000 and the likes.
            </p>
          </div>
          <div class="column large-8">
            <label>
              <input type="checkbox" name="green" {{ $details['green_mark'] ? 'checked' : '' }}>
              Green Product
            </label>
          </div>
        </div>

        <div class="row column">
          <div class="column large-4">
            <label>Description</label>
            <p class="explain">
              This may either be specifications of the product, minimum order quantity and/or external links to brochures.
            </p>
            <p class="explain">Tip: Click the <em>Link</em> icon in the description toolbar to add a URL to your own product page.</p>
          </div>
          <div class="column large-8">
            <textarea id="product-desc">{{ $details["description"] }}</textarea>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
