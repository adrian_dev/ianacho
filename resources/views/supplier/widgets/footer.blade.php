<div class="row">
  <div class="large-7 columns">
    <div class="ipennylogo">
      <img src="{{ asset('img/ipenny-logo-holes.svg') }}">
    </div>

    <div class="ipennyinfo">
      <img class="ipenny-small-text" src="{{ asset('img/ipenny-small-text.png') }}">
      <p>
        The Materials Library is a digital catalog for supplier products.
        It allows for easy viewing of products just by searching for terms.
      </p>

      <img src="{{ asset('img/sg-map.gif') }}">
    </div>
  </div>
</div>

<div class="footer-bottom row">
  <div class="column">
    <hr>
    <p>&copy; M Moser Associates Singapore</p>
    <p><a href="http://www.mmoser.com">mmoser.com</a>
    </p>
  </div>
</div>
