<div class="column large-2 medium-2">
  <div class="card">
    <div class="thumb">
      <a href="{{ route('product.edit', ['id' => $product->getID()]) }}">
        <img src="{{ asset($product->getThumbnail()) }}" />
      </a>
    </div>

    <div class="details">
      <div class="hiddenInListview" data-equalizer-watch>
        <div class="cards-title">
          <a href="{{ route('product.edit', ['id' => $product->getID()]) }}">
            {{ $product->getName() }}
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
