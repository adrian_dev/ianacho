<div id="upperblock">
  <header id="topheader">
    <div class="row expanded">
      <div class="medium-4 columns small-4">
        <div class="mainmenu">
          <a id="togglenav" data-toggle="maindropmenu">
            <span class="menutext hide-for-small-only">MENU</span>
            <span class="menuicon"><span class="navlines"></span></span>
          </a>

          <div class="dropdown-pane bottom"
            id="maindropmenu"
            data-dropdown
            data-v-offset="10"
            data-h-offset="0"
            data-close-on-click="true">
            <ul class="lists">
              <li><a href="{{ route('profile') }}">Profile</a></li>
              <li><a href="{{ route('supplier.product') }}">Add New Product</a></li>
              <li><a href="{{ route('supplier.requests') }}">Requests</a></li>
              @if (auth('supplier')->check())
                <li><a href="{{ route('logout') }}">Logout</a></li>
              @else
                <li><a href="{{ route('login') }}">Login</a></li>
              @endif
            </ul>
          </div>
        </div>
      </div>

      <div class="medium-4 columns small-5">
        <div class="ipennylogo">
          <a href="{{ route('profile') }}">
            <img src="{{ asset('img/ipenny-text.svg') }}" />
          </a>
        </div>
      </div>

      <div class="medium-4 columns hide-for-small-only">
        <div class="mmoserlogo">
          <img src="{{ asset('img/mmoser-logo.svg') }}" />
        </div>
      </div>
    </div>
  </header>
</div>
