<div class="row">
  @if (isset($list["products"]) && !empty($list["products"]))
    @foreach ($list["labels"] as $i => $label)
      <div class="column">
        <h4>{{ $label["name"] }}</h4>
        @if (isset($label["categories"]) && !empty($label["categories"]))
          @foreach ($label["categories"] as $a => $category)
            <h5>{{ $category }}</h5>
            <div class="row large-up-7">
              @if (isset($list["products"][$i][$a]))
                @foreach ($list["products"][$i][$a] as $product)
                  <div class="column">
                    <div class="card">
                      <a href="{{ $product['link'] }}">
                        <img src="{{ $product['thumbnail'] }}">
                      </a>
                      <div class="card-section">
                        <a href="{{ $product['link'] }}">
                          <small>{{ $product['name'] }}</small>
                        </a>
                      </div>
                    </div>
                  </div>
                @endforeach
              @endif
            </div>
          @endforeach
        @endif
      </div>
    @endforeach
  @endif
</div>
