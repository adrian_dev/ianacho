@extends('template')

@section('title', 'Edit '.$product->getName())

@section('content')
<table>
    <tr>
        <td>Image:</td>
        <td><img src="{{ asset($product->getImage()) }}" /></td>
    </tr>
    <tr>
        <td>Name:</td>
        <td>{{ $product->getName() }}</td>
    </tr>
    <tr>
        <td>Tags:</td>
        <td>
            <ul>
                @foreach($product->tags as $tag)
                <li>{{ $tag->getPhrase() }}</li>
                @endforeach
            </ul>
        </td>
    </tr>
</table>
<a href="{{ route('product.edit', ['id' => $product->getID()]) }}">Edit</a>
@endsection
