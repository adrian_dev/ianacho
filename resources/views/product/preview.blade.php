<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title></title>
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/foundation.css') }}" />
  </head>
  <body>
    <div class="row">
      @if (isset($product))
        <div class="column text-center">
          <img src="{{ asset($product->getImage()) }}" height="300" />
        </div>
        <div class="column large-6">
          Name:
        </div>
        <div class="column large-6">
          {{ $product->getName() }}
        </div>
        <div class="column">
          Description:
        </div>
        <div class="column">
          {{ $product->getDescription() }}
        </div>
        <div class="column large-6">
          Code:
        </div>
        <div class="column large-6">
          {{ $product->getCode() }}
        </div>
        <div class="column large-6">
          Price:
        </div>
        <div class="column large-6">
          {{ $product->getPrice() }}
        </div>
        <div class="column large-6">
          Brand:
        </div>
        <div class="column large-6">
          {{ $product->getBrand() }}
        </div>
        <div class="column large-6">
          Range:
        </div>
        <div class="column large-6">
          {{ $product->getRange() }}
        </div>
        <div class="column large-6">
          Dimensions:
        </div>
        <div class="column large-6">
          {{ $product->getDimensions() }}
        </div>
        <div class="column large-6">
          Green Mark:
        </div>
        <div class="column large-6">
          {{ $product->getGreenMark() }}
        </div>
      @else
        <div class="column">
          There was a problem loading the Product Details
        </div>
      @endif
    </div>
  </body>
</html>
