<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
    <title>iPenny - @yield('title')</title>
    @yield('meta')
    <base href="{{ url('/') }}" />
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/fonts.css') }}">
    <link rel="stylesheet" href="{{ asset('css/creatives.css') }}">
    <script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
  </head>
  <body class="pages">
    <div id="upperblock">
      @include('creatives.widgets.header')
      @include('creatives.widgets.searchbar')
    </div>
    <div id="content">
      @yield('content')
    </div>
    <!-- end #content -->
    <div id="footer">
      @include('creatives.widgets.footer')
    </div>
    <!-- #footer -->
    <script src="{{ asset('js/foundation.js') }}"></script>
    <script src="{{ asset('js/what-input.js') }}"></script>
    <script src="{{ asset('js/ipenny.js') }}"></script>
  </body>
</html>
