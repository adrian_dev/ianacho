@extends("library")

@section("content")
  <section>
    <div class="row">
      <div class="column medium-4">
        <div class="row">
          <div class="column">
            <a href="{{ route('admin.dashboard') }}" class="button hollow secondary" style="vertical-align:initial;">
              <i class="fa fa-chevron-left" aria-hidden="true"></i> BACK
            </a>
          </div>
          <div class="column">
            @include('admin.widgets.updates')
          </div>
        </div>
      </div>
      <div class="column medium-8 end">
        <form method="post" action="{{ isset($supplier) ? route('admin.update') : route('admin.save') }}" style="background:transparent;border:none;padding:0;">
          {{ csrf_field() }}
          <input type="hidden" name="id" value="{{ isset($supplier) ? $supplier->getID() : '' }}" />

          <div class="card">
            <div class="card-divider">
              <div class="row expanded">
                <div class="column medium-7">
                  <strong style="font-size:1.4rem;text-transform:uppercase;vertical-align:middle;">{{ $title }}</strong>
                </div>

                <div class="column medium-5 end text-right">
                  <a href="" class="button" style="margin-bottom:0;">
                    <i class="fa fa-save"></i> <b>SAVE</b>
                  </a>
                </div>
              </div>
            </div>

            <div class="card-section">
              <div class="row expanded">
                <div class="column medium-4">
                  <iframe class="iframe-logo" style="margin-bottom:1rem;width:100%;height:12.5rem;border:none;" src="{{ route('admin.logo.form', ['id' => $supplier->getID()]) }}" frameborder="none"></iframe>

                  <a href="" class="button hollow expanded" title="email supplier their login details small">
                    <i class="fa fa-envelope-o"></i>
                    SEND SUPPLIER ACCESS
                  </a>
                  <a href="javascript:void(0);" class="button hollow secondary small expanded" onclick="if(window.confirm('Do you really want to deactivate supplier?')) {window.top.location.href='{{ route('admin.desupplier', ['id' => $supplier->getID()]) }}'};">
                    <i class="fa fa-times"></i> DEACTIVATE SUPPLIER
                  </a>
                </div>
                <div class="column medium-8">
                  <label for="supplier-name"><b>NAME:</b> <small>(required)</small></label>
                  <input type="text" name="name" id="supplier-name" required value="{{ isset($supplier) ? $supplier->getName() : old('name') }}" />

                  <label for="supplier-address"><b>ADDRESS:</b> <small>(required)</small></label>
                  <textarea name="address" rows="6" required id="supplier-address">{{ isset($supplier) ? $supplier->getAddress() : old('address') }}</textarea>

                  <label for="supplier-website"><b>WEBSITE:</b> <small>(optional)</small></label>
                  <input type="text" name="website" id="supplier-website" required value="{{ isset($supplier) ? $supplier->getWebsite() : old('website') }}" />
                </div>
                <div class="column">
                  <div class="row expanded">
                    <div class="column">
                      <hr />
                      <h6><b>CONTACT PERSON:</b> <small>(At least one entry is required.)</small></h6>
                    </div>

                    @include('admin.forms.contact', ['contacts' => isset($supplier) ? $supplier->contacts : []])

                    <div class="column medium-6 end">
                      <a href="javascript:void(0);" onclick="addContactForm();" class="button hollow secondary expanded">
                        <i class="fa fa-plus"></i> ADD NEW CONTACT
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </section>
@endsection