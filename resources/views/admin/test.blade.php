<script src="{{ asset('js/jquery.js') }}"></script>
<button>Generate supplier login</button>
<select>
  @foreach ($suppliers as $key => $value)
    <option value="{{ $value->getID() }}">{{ $value->getName() }}</option>
  @endforeach
</select>
<div class="username"></div>
<div class="password"></div>
<script>
$(function() {
  $('button').click(function() {
    $.ajax({
      url: '{{ url('test') }}',
      type: 'post',
      data: {
        _token: '{{ csrf_token() }}',
        id: $('select').val()
      },
      success: function(response) {
        $('.username').text(response.username);
        $('.password').text(response.password);
      }
    });
  });
});
</script>
