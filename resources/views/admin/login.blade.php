@extends('cms')
@section('title', $title)
@section('meta')
    @parent
@endsection
@section('content')
<form method="post" action="{{ route('admin.auth') }}">
  <div class="row login expanded">
    <div class="column large-4 large-offset-4">
      <h2>{{ $title }}</h2>
      <hr />
    </div>

    {{ csrf_field() }}

    <div class="column large-4 large-offset-4">
      <label>Username:</label>
      <input type="text" name="username" value="{{ old('username') }}" required />
    </div>

    <div class="column large-4 large-offset-4">
      <label>Password:</label>
      <input type="password" name="password" />
    </div>

    <div class="column large-4 large-offset-4 end">
      <input type="submit" value="LOGIN" class="button expanded" />
    </div>
    <!--div class="column large-4 large-offset-4 end">
      <a href="{{ url('register') }}">Register</a>
    </div-->
  </div>
</form>
<br /><br />
@endsection
