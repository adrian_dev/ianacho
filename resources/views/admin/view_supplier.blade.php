@extends('cms')
@section('title', 'Edit')
@section('content')
<table cellspacing="0">
    <tr>
        <th>ID</th>
        <td>{{ $supplier->id }}</td>
    </tr>
    <tr>
        <th>NAME</th>
        <td>{{ $supplier->name }}</td>
    </tr>
    <tr>
        <th>SALES/CONTACT PERSON</th>
        <td>{{ $supplier->contact_person }}</td>
    </tr>
    <tr>
        <th>EMAIL</th>
        <td>{{ $supplier->email }}</td>
    </tr>
    <tr>
        <th>PHONE NUMBER</th>
        <td>{{ $supplier->phone }}</td>
    </tr>
    <tr>
        <th>WEBSITE</th>
        <td>{{ $supplier->website }}</td>
    </tr>
    <tr>
        <th>ADDRESS</th>
        <td>{{ $supplier->address }}</td>
    </tr>
</table>

<a href="{{ route('admin.dashboard') }}" class="button hollow">Back</a>
<a href="{{ route('admin.edit', ['id' => $supplier->id]) }}" class="button hollow">Edit</a>
<a href="{{ route('admin.email', ['id' => $supplier->id]) }}" class="button hollow">Send Email</a>
@endsection