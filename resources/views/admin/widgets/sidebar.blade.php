<section id="admin-sidebar">
  @if (isset($sidebar))
    <div class="row">
      <div class="column">
        {{ $sidebar["totalSuppliers"] }} Suppliers
      </div>
      <div class="column">
        {{ $sidebar["emptySuppliers"] }} Suppliers with no products yet
      </div>
      <div class="column">
        {{ $sidebar["oldSuppliers"] }} Suppliers with no updates in the last 30 days
      </div>
      <div class="column">
        @foreach ($sidebar["recentUpdates"] as $item)
          <a href="{{ route('library.product', $item->getID()) }}">
            <img src="{{ asset($item->getThumbnail()) }}" width="100" height="100" />
          </a>
        @endforeach
      </div>
    </div>
  @endif
</section>
