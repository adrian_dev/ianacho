<table cellspacing="0" class="stack" id="suppliers-table" data-field="{{ isset($field) ? $field : 'updated_at' }}" data-page="{{ isset($page) ? $page : 1 }}" data-order="{{ isset($order) ? $order : 'DESC' }}">
  <thead>
    <tr>
      <td colspan="2">
        <form autocomplete="off" style="background:none;border:none;padding:0;margin:0;position:relative;" method="get" action="{{ route('admin.dashboard') }}" class="input-group">
          <input type="search" name="name" placeholder="Search Supplier Name" class="input-group-field" required value="{{ request()->input('name') }}">
          {{-- <a href="{{ route('admin.dashboard') }}" style="position:absolute;top:0.5rem;right:6rem;color:#aaa;"><i class="fa fa-times"></i></a> --}}
          <div class="input-group-field">
            <button type="submit" class="button secondary" style="margin-bottom:0;">
              <i class="fa fa-search"></i>
            </button>
          </div>
        </form>
      </td>
      <td colspan="4" class="text-right">{!! $pagination !!}</td>
    </tr>
    <tr>
      @foreach($thead as $th)
        {!! $th !!}
      @endforeach
      <th colspan="3"></th>
    </tr>
  </thead>

  <tbody>
    @foreach($suppliers as $supplier)
      <tr>
        <td>
          <a class="edit-supplier" href="{{ route('admin.edit', ['id' => $supplier->id]) }}">
            {{ $supplier->getName() }}

            @if(isset($tagged[$supplier->getID()]) && $tagged[$supplier->getID()] < 1)
            <span style="float:right;font-size:0.9rem;padding:0.3rem 0.6rem;background-color:#ab6f47;color:#fff;">no tags</span>
            @endif
          </a>
        </td>
        <td class="center">
          <a href="{{ route('admin.edit', ['id' => $supplier->id]) }}">
            @if(!empty($supplier->getLogo()))
              <img src="{{ asset($supplier->getLogo()) }}" width="130" />
            @else
              <img src="http://via.placeholder.com/130x65" />
            @endif
          </a>
        </td>
        <td class="center">{{ $supplier->getTotalProducts() }}</td>
        <td class="center">{{ date('m/d/Y h:ia', strtotime($supplier->updated_at)) }}</td>
        <td class="text-center">
          <?php $contact = $supplier->contacts()->first(); ?>
          @if (!empty($contact))
            <a href="mailto:{{ $contact->getEmail() }}?subject=iPenny%20Reminder&body=Hi%20{{ $contact->getName() }};" title="Edit Details"><i class="fa fa-envelope"></i></a>
          @endif
        </td>
        <td class="text-center">
          <a href="{{ route('admin.edit', ['id' => $supplier->id]) }}" title="Edit Details"><i class="fa fa-pencil"></i></a>
        </td>
        <td class="text-center">
          <a href="{{ url('catalog', ['id' => $supplier->getID()]) }}" title="View Products"><i class="fa fa-folder-open"></i></a>
        </td>
      </tr>
    @endforeach
  </tbody>

  <tfoot>
    <tr><td colspan="6" class="text-right">{!! $pagination !!}</td></tr>
  </tfoot>
</table>

@if (isset($names) && !empty($names))
<ul class="no-bullet" id="search-hint" style="position:absolute;background-color:#fff;display:none;"></ul>
<script>
var $names = {!! json_encode($names) !!};

$(function() {
  $("[type=search]").keyup(function() {
    $("#search-hint").html("");

    if ($(this).val().length > 0) {
      $.each($names, function(index, value) {
        if (value.toLowerCase().indexOf($("[type=search]").val().toLowerCase()) >= 0) {
          $("#search-hint").append('<li style="padding:0.3rem;"><a href="{{ route('admin.dashboard') }}?name='+value+'">'+value+'</a></li>');
        }
      });

      $("#search-hint").css({
        top: ($("[type=search]").offset().top + 40) + "px",
        left: $("[type=search]").offset().left + "px",
        width: $("[type=search]").width() + "px"
      }).show();
    } else {
      $("#search-hint").hide();
    }
    
  });
});
</script>
@endif
