<div class="row">
  <div class="column">
    <h2 class="prods">Supplier Products</h2>
  </div>
</div>

<div id="prod-grid" class="row small-up-2 medium-up-3 large-up-6" data-equalizer>
  @foreach ($supplier->products()->where('status', 1)->orderBy('updated_at', 'DESC')->get() as $product)
    <div class="column">
      <div class="card">
        <?php $range = $product->range()->where('status', 1)->orderBy('updated_at', 'DESC')->first(); ?>
        <a href="{{ route('library.product', ['id' => $range->getID()]) }}">
          <img src="{{ !empty($range) ? asset($range->getThumbnail()) : 'http://via.placeholder.com/350x350' }}" alt="">
        </a>

        <div class="card-section" data-equalizer-watch>
          <div class="cards-title"><b>{{ $product->getName() }}</b></div>
          <small>{{ count($product->range) }} item/s</small>
          <br />
          <small>{{ count($product->tags) }} tag/s</small>
        </div>
      </div>
    </div>
  @endforeach
</div>
