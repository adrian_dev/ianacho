<div class="column large-4 large-offset-8">
    <form method="get" action="{{ $action }}">
        <input type="text" name="term" placeholder="search" value="{{ isset($_GET['term']) ? $_GET['term'] : '' }}" required maxlength="150" />
        <input type="submit" class="button secondary" value="go" />
    </form>
</div>