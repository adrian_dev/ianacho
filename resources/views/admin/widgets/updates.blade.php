<!--admin overview section-->
@if (Auth::user())
  <aside id="sidebar">
    <section>
      <h4><span>In a Glance</span></h4>

      <div class="sidecard">
        <h4>Welcome back, <strong>{{ ucfirst(Auth::user()->name) }}!</strong></h4>

        <ul class="message">
          @if (isset($total['product']))
            <li><b>{{ $total['product'] }}</b> Products</li>
          @endif
          @if (isset($total['supplier']))
            <li><b>{{ $total['supplier'] }}</b> Suppliers</li>
          @endif
          @if (isset($total['tag']))
            <li>
              <a href="{{ route('admin.tag') }}"><b>{{ $total['tag'] }}</b> Tags</a>
            </li>
          @endif
        </ul>
      </div>
    </section>

    <section>
       <h4><span>Recent activites</span></h4>
       <div class="sidecard">
          <ul class="message">
            @if (isset($update['product']))
              <li><b>{{ $update['product'] }}</b> product updates this week</li>
            @endif
            @if (isset($update['supplier']))
              <li><b>{{ $update['supplier'] }}</b> supplier updates this week</li>
            @endif
            @if (isset($new['product']))
              <li><b>{{ $new['product'] }} </b> new products this week</li>
            @endif
            @if (isset($new['supplier']))
              <li><b>{{ $new['supplier'] }} </b> new suppliers this week</li>
            @endif
          </ul>
      </div>
    </section>
  </aside>
@endif
<!--admin overview section-->
