<header id="topheader">
  <div class="row expanded">
    <div class="medium-4 columns small-4 large-4">
      <div class="mainmenu">
        <a id="togglenav" data-toggle="maindropmenu">
          <span class="menutext">MENU</span>
          <span class="menuicon">
            <span class="navlines"></span>
          </span>
        </a>

        <div class="dropdown-pane bottom" id="maindropmenu"
        data-dropdown
        data-v-offset="10"
        data-h-offset="0"
        data-close-on-click="true"
        >
            @if (auth()->check())
          <ul class="lists">
            <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('admin.add') }}">Add New Supplier</a></li>
            <li><a href="{{ route('admin.tag') }}">Tags</a></li>
            <li><a href="{{ url('register') }}">Add New User</a></li>
          </ul>

          <hr>
            @endif

          <ul class="lists">
            <li><a href="{{ route('home') }}">Visit iPenny</a></li>
            @if (auth()->check())
              <li><a href="{{ route('admin.logout') }}">Logout</a></li>
            @else
              <li><a href="{{ route('admin.login') }}">Login</a></li>
            @endif

          </ul>
        </div>
      </div>
    </div>

    <div class="medium-4 columns small-4 small-offset-4 large-4 large-offset-0">
      <div class="ipennylogo">
        <a href="{{ route('home') }}"><img src="{{ asset('img/ipenny-text.svg') }}"></a>
      </div>
    </div>

    <div class="medium-4 columns hide-for-small-only large-4">
      <div class="mmoserlogo">
        <img src="{{ asset('img/mmoser-logo.svg') }}">
      </div>
    </div>
  </div>
</header>
