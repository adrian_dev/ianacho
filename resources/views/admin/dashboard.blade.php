@extends('cms')
@section('title', 'Dashboard')
@section('content')
  <section id="newproducts" class="sections">
    <div class="row expanded">
      <div class="large-3 column">
        @include('admin.widgets.updates')
      </div>

      <div class="large-9 column">
        <div class="row">
          <div class="large-6 column"><h2>Suppliers</h2></div>

          <div class="large-6 column">
            <div class="viewall">
              <div class="button-group">
                <a href="{{ route('admin.add') }}" class="button">
                  <i class="fa fa-plus" aria-hidden="true"></i> Add Supplier
                </a>
                <a href="{{ route('admin.tag') }}" class="button">
                  <i class="fa fa-tags" aria-hidden="true"></i> Add Tags
                </a>
              </div>
            </div>
          </div>
        </div>

        <div class="column large-12">
          @if (isset($suppliers))
            @include('admin.widgets.suppliers', ['suppliers' => $suppliers])
          @else
            <h4 class="row column text-center">EMPTY</h4>
            <a href="{{ route('admin.dashboard') }}" class="button">BACK <i class="fa fa-chevron-left"></i></a>
          @endif
        </div>
      </div>
    </div>
  </section> <!-- end new prod -->

  @include('admin.widgets.sidebar')
@endsection
