@extends('cms')
@section('title', $title)
@section('content')
  <section id="newproducts" class="sections">
    <div class="row expanded">
      <div class="large-3 column">
        @include('admin.widgets.updates')
      </div>

      <div class="column medium-2">
            <a href="{{ route('admin.dashboard') }}" class="button hollow secondary" style="vertical-align:initial;">
              <i class="fa fa-chevron-left" aria-hidden="true"></i> BACK
            </a>
          </div>

      <div class="large-9 column">
        <div class="supplier-form">
          @if(isset($supplier))
            <form method="post" class="row" action="{{ route('admin.update') }}">
          @else
            <form method="post" class="row" action="{{ route('admin.save') }}">
          @endif

            {{ csrf_field() }}

            @if($errors->any())
              <ul class="column large-12">
                @foreach($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            @endif

            @if (isset($supplier))
              <div class="column text-right">
                <a href="{{ route('admin.email', ['id' => $supplier->id]) }}" class="button hollow">
                  <i class="fa fa-envelope-o" aria-hidden="true"></i>
                  Email details to Supplier
                </a>
                <button type="submit" class="button">
                  <i class="fa fa-floppy-o" aria-hidden="true"></i>
                  UPDATE
                </button>
              </div>
            @else
              <div class="column large-3 large-offset-9">
                <input type="submit" class="button expanded" value="SAVE" />
              </div>
            @endif

            @if (isset($supplier))
              <div class="column large-3">
                <input type="hidden" name="id" value="{{ $supplier->id }}" />
                <iframe class="iframe-logo" src="{{ route('admin.logo.form', ['id' => $supplier->getID()]) }}" frameborder="none"></iframe>
              </div>
            @endif

            <div class="column large-9">
              <label>Supplier Name <span>(Required)</span></label>
              <input type="text" name="name" placeholder="Example Company" value="{{ old('name', isset($supplier->name) ? $supplier->name : '') }}" required maxlength="255" />
            </div>

            <div class="column large-9">
              <label>Address</label>
              <textarea name="address" rows="4" placeholder="Address">{{ old('address', isset($supplier->address) ? $supplier->address : '') }}</textarea>
            </div>

            <div class="column large-4 text-right">
              <label>Website</label>
            </div>
            <div class="column large-5">
              <input type="text" name="website" placeholder="website" value="{{ old('website', isset($supplier->website) ? $supplier->website : '') }}" max="150" />
            </div>

            <div class="column large-4 text-right">
              <label>Phone</label>
            </div>
            <div class="column large-5">
              <input type="text" name="phone" placeholder="phone" value="{{ old('phone', isset($supplier->phone) ? $supplier->phone : '') }}" max="50" />
            </div>

            <div class="column">
              <h6>Contact Person/s</h6>
              <hr />
            </div>

            <div class="column">
              <section class="contacts row">
                @include('admin.forms.contact', ['contacts' => isset($supplier) ? $supplier->contacts()->where("status", 1)->orderBy("name", "ASC")->get() : []])
              </section>
              <div class="row">
                <div class="column large-6">
                  <a href="javascript:void(0);" onclick="addContactForm();" class="button hollow secondary expanded">+add another contact</a>
                </div>
              </div>
            </div>
            </form>
        </div>

        <script type="text/javascript" src="{{ asset('js/admin.js') }}" async></script>
      </div>

      @if (isset($supplier))
        <div class="column text-right" style="margin-top:1rem;">
          <a href="javascript:void(0);" class="button hollow secondary" onclick="if(window.confirm('Do you really want to deactivate supplier?')) {window.top.location.href='{{ route('admin.desupplier', ['id' => $supplier->getID()]) }}'};">Deactivate Supplier</a>
        </div>
      @endif
    </div>
  </section> <!-- end new prod -->

  @if (isset($supplier))
    @include('admin.widgets.products', ['products' => $supplier->products, 'id' => $supplier->getID()])
  @endif

  <link rel="stylesheet" href="{{ asset('css/jquery.fancybox.min.css') }}" />
  <script type="text/javascript" src="{{ asset('js/jquery.fancybox.min.js') }}"></script>
@endsection
