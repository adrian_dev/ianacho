@if (!empty($contacts))
  @foreach ($contacts as $key => $contact)
    <div class="column medium-6 {{ $key+1 >= $contacts->count() ? 'end' : '' }}">
      <div class="row expanded">
        <input type="hidden" name="contact_id[]" value="{{ $contact->getID() }}" />
        <div class="column text-right">
          <button class="button small secondary hollow x-contact" onclick="$(this).parent().parent().parent().remove()">REMOVE</button>
        </div>
        <div class="column">
          <label><b>NAME:</b> <small>(required)</small></label>
            <input type="text" name="contact_person[]" placeholder="contact name" value="{{ $contact->getName() }}" required maxlength="150" />
        </div>
        <div class="column large-2 text-right small-2">
          <label><b>PHONE:</b> <small>(required)</small></label>
        </div>
        <div class="column large-10">
          <input type="text" name="contact_number[]" placeholder="000-00-0000" value="{{ $contact->getPhone() }}" required maxlength="150" />
        </div>
        <div class="column large-2 text-right small-2">
          <label><b>EMAIL:</b> <small>(required)</small></label>
        </div>
        <div class="column large-10">
          <input type="text" name="contact_email[]" placeholder="example@domain.com" value="{{ $contact->getEmail() }}" required maxlength="150" />
        </div>
      </div>
      <hr />
    </div>
  @endforeach
@else
  <div class="column large-6">
    <div class="row">
      <div class="column text-right">
        <button class="button small secondary hollow x-contact" onclick="$(this).parent().parent().parent().remove()">REMOVE</button>
      </div>
      <div class="column">
        <label>Name</label>
        <input type="text" name="contact_person[]" placeholder="contact name" value="" required maxlength="150" />
      </div>
      <div class="column large-2 text-right">
        <label>Phone</label>
      </div>
      <div class="column large-10">
        <input type="text" name="contact_number[]" placeholder="000-00-0000" value="" required maxlength="150" />
      </div>
      <div class="column large-2 text-right">
        <label>Email</label>
      </div>
      <div class="column large-10">
        <input type="text" name="contact_email[]" placeholder="example@domain.com" value="" required maxlength="150" />
      </div>
    </div>
    <hr />
  </div>
@endif
