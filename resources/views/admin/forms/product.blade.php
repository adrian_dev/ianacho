@extends('cms')
@section('title', $title)
@section('content')
  <section id="newproducts" class="sections">
    <div class="row expanded">
      <div class="large-3 column">
        @include('admin.widgets.updates')
      </div>

      <div class="large-9 column">
        <div class="row">
          <div class="large-6 column"><h2>{{ $title }}</h2></div>

          <div class="column">
            @if(isset($product))
              <form method="post" action="#">
                <input type="hidden" name="id" value="{{ $product->id }}" />
            @else
              <form method="post" action="#">
            @endif
              {{ csrf_field() }}

              @if(old('image') || isset($product->image))
                <label for="img"><img src="{{ old('image', $product->image) }}"></label>
              @else
                <label for="img">+ ADD IMAGE</label>
              @endif

              <input type="hidden" name="supplier_id" value="{{ old('supplier_id', isset($product->supplier_id) ? $product->supplier_id : $supplier_id) }}" />
              <input type="hidden" name="image" value="{{ old('image', isset($product->image) ? $product->image : '') }}" />
              <input type="text" name="name" value="{{ old('name', isset($product->name) ? $product->name : '') }}" required maxlength="150" />
            </form>
            <form method="post" action="{{ route('upload') }}" enctype="multipart/form-data">
              <input type="file" name="file" id="img" />
            </form>



            <form id="new" method="post" action="{{ route('save') }}">
              {{ csrf_field() }}

              @if(isset($product))
                <input type="hidden" name="id" value="{{ $product->id }}" />
              @endif

              <input type="hidden" name="image" value="" />
              <input type="hidden" name="thumbnail" value="" />

              <div class="column large-4 product-image">
                @if(!empty($image))
                  <img src="{{ asset($image) }}" />
                @endif
                <label for="image" class="button expanded secondary">Upload New Image</label>
              </div>

              <div class="column large-8">
                <h6>Product Name:</h6>
                <input type="text" name="name" placeholder="product name/code" value="" required maxlength="50" />
              </div>

              <div class="column large-8">
                <h6>Description:</h6>
                <textarea name="description" rows="5"></textarea>
              </div>

              <div class="column large-4">
                <h6>Brand:</h6>
                <input type="text" name="brand" value="" />
              </div>

              <div class="column large-4">
                <h6>Range:</h6>
                <input type="text" name="range" value="" />
              </div>

              <div class="column large-4">
                <h6>Price:</h6>
                <input type="text" name="price" value="" />
              </div>

              <div class="column large-4">
                <h6>Dimensions:</h6>
                <input type="text" name="dimensions" value="" />
              </div>

              <div class="column large-4">
                <h6>Code:</h6>
                <input type="text" name="code" value="" />
              </div>

              <div class="column large-4">
                <label for="green">
                  <input type="checkbox" name="green_mark" value="1" id="green" />
                  Green Product
                </label>
              </div>

              <div class="column tags">
                <h5>Tags:</h5>

                @foreach ($categories as $i => $category)
                  <div class="button-group">
                    <h6>{{ ucfirst($category) }}:</h6>
                    @if (isset($tags[$i]))
                      @foreach($tags[$i] as $tag)
                        <label for="tag-{{ $tag->getID() }}" class="button hollow secondary">
                          <input type="checkbox" name="tags[]" value="{{ $tag->getID() }}" id="tag-{{ $tag->getID() }}" />
                          {{ $tag->getPhrase() }}
                        </label>
                      @endforeach
                    @endif
                  </div>
                @endforeach
              </div>

              <div class="column large-8 submit">
                <a href="{{ route('profile') }}" class="button hollow secondary">CANCEL</a>
                <input type="submit" class="button" value="SAVE" />
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
