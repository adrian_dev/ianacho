<!DOCTYPE html>
<html>
  <head>
    <script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
  </head>
  <body>
    <form id="add-logo-form" method="post" action="{{ route('admin.logo.save') }}" enctype="multipart/form-data">
      {{ csrf_field() }}
      <input type="hidden" name="id" value="{{ $id }}" />
      <input type="file" name="logo" id="add-logo" />
      <input type="submit">
      <a href="javascript:void(0);" onclick="$('#add-logo').trigger('click');"></a>
      <label for="add-logo">Upload Logo</label>
    </form>
    <style>
    html, body {
      width: 100%;
      height: 100%;
      margin: 0px;
    }
    #add-logo-form {
      width: 100%;
      height: 100%;
      @if (!empty($logo))
        background: url({{ asset($logo) }}) no-repeat center;
        background-size: contain;
      @endif
    }
    #add-logo-form.is-dragover {
      background-color: #efefef;
    }
    #add-logo-form input[type=submit], #add-logo-form input[type=file] {
      display: none;
    }
    #add-logo-form label {
      position: absolute;
      bottom: 0px;
      width: 100%;
      color: #fff;
      text-align: center;
      font-family: helvetica;
      padding: 12px 0px;
      background-color: rgba(0, 141, 238, 0.25);
      color: #fff;
    }
    a {
      display: inline-block;
      width: 100%;
      height: 100%;
      padding: 0px;
      margin: 0px;
      position: absolute;
    }
    a:hover {
      background-color: rgba(0,0,0,0.2);
      transition: background-color 220ms;
    }
    </style>
    <script>
      $(function() {
        var $form  = $('#add-logo-form');
        var $input = $form.find('input[type=file]');

        $("#add-logo").change(function() {
          $form.trigger('submit');
        });

        $form.on('submit', function(e) {
          if ($form.hasClass('is-uploading')) return false;

          $form.addClass('is-uploading').removeClass('is-error');

          //if (isAdvancedUpload) {
            e.preventDefault();

            var ajaxData = new FormData($form.get(0));

//            if (droppedFiles) {
//              $.each(droppedFiles, function(i, file) {
//                ajaxData.append($input.attr('name'), file);
//              });
//            }

            $.ajax({
              url         : $form.attr('action'),
              type        : $form.attr('method'),
              data        : ajaxData,
              dataType    : 'json',
              cache       : false,
              contentType : false,
              processData : false,
              complete    : function() {
                $form.removeClass('is-uploading');
              },
              success : function(data) {
                $form.css({
                  background     : "url({{ url('/') }}/"+data.src+") no-repeat center",
                  backgroundSize : "contain"});
              },
              error : function(e) {
                console.log(e);
              }
            });
          //}
        });
      });
    </script>
  </body>
</html>
