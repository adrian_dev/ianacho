@extends('cms')
@section('title', 'Tags')
@section('content')
  <section id="newproducts" class="sections">
    <div class="row expanded">
      <div class="large-3 column">
        @include('admin.widgets.updates')
      </div>

      <div class="large-9 column">
        <div class="row">
          <div class="large-6 column"><h2>Tags</h2></div>

          <div class="large-6 column">
            <div class="viewall">
              <div class="button-group">
                <a href="{{ route('admin.dashboard') }}" class="button">
                  <i class="fa fa-chevron-left" aria-hidden="true"></i> Back
                </a>
              </div>
            </div>
          </div>
        </div>

        <div id="add-tag" class="row">
          <div class="column large-6">
            <input type="text" name="tag" placeholder="enter new tag" autofocus />
          </div>
          <div class="column large-2 end">
            <a href="javascript:void(0);" class="button expanded">+ADD</a>
          </div>
        </div>

        <div class="row">
          @foreach ($categories as $key => $value)
            <div class="column {{ $value != 'none' ? 'large-6' : '' }}">
              @if ($value != 'none')
                <h6>{{ ucfirst($value) }}:</h6>
              @endif

              <ul id="category-{{ $key }}" data-index="{{ $key }}" class="tags">
                @if (isset($tags[$key]))
                  @foreach ($tags[$key] as $tag)
                    <li data-id="{{ $tag->getID() }}" class="button secondary hollow">
                      {{ $tag->getPhrase() }}
                      <a href="javascript:void(0);" data-id="{{ $tag->getID() }}" class="tag-remove">x</a>
                    </li>
                  @endforeach
                @endif
              </ul>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </section>

  <script>
    var addURL = "{{ route('admin.tag.add') }}";
    var removeURL = "{{ route('admin.tag.remove') }}";
    var csrf = "{{ csrf_token() }}";
    var updateURL = "{{ route('admin.tag.update') }}";
  </script>
  <script type="text/javascript" src="{{ asset('js/jquery-ui.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/admin.js') }}" async></script>
@endsection
