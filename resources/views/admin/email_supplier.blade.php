@extends('cms')
@section('title', 'Email')
@section('content')
  <section id="newproducts" class="sections">
    <div class="row expanded">
      <div class="large-3 column">
        @include('admin.widgets.updates')
      </div>

      <div class="large-9 column">
        <div class="row">
          <div class="column large-6"><h3>Email Supplier</h3></div>
          <div class="large-6 column">
            <div class="viewall">
              <div class="button-group">
                <a href="{{ route('admin.dashboard') }}" class="button">
                  <i class="fa fa-chevron-left" aria-hidden="true"></i> BACK
                </a>
              </div>
            </div>
          </div>
        </div>

        <div class="row email-step">
          <div class="column" id="email-step1">
            <p>Before you email their profile details to {{ strtoupper($supplier->getName()) }} please make a new password for their login.</p>
          </div>
          <div class="column hide" id="email-step2">
            <h5>Do you want to send an email to {{ strtoupper($supplier->getName()) }} containing their profile login details?</h5>
            <p>The supplier can update their information and catalog through the profile page.</p>
          </div>
          <div class="column">
            <a href="{{ route('admin.dashboard') }}" class="button hollow">Maybe Later</a>
            <a href="javascript:void(0);" id="make-password" onclick="requestPassword('{{ $supplier->getID() }}');" class="button">
              Make a New Password
            </a>
            <a href="javascript:void(0);" class="button hide" id="send-to-supplier">
              <i class="fa fa-envelope-o" aria-hidden="true"></i>
              Send Email
            </a>
          </div>
          <div class="column">
            <i class="error"></i>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
