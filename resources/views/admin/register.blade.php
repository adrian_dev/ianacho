@extends('cms')
@section('title', 'Registration')
@section('content')
  <div class="row">
    <div class="column">
      <h1>User Registration</h1>
    </div>
    <div class="column">
      <ul>
        @foreach($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
        @if(isset($message))
          <li>{{ $message }}</li>
        @endif
      </ul>
    </div>
  </div>



  <form method="post" action="{{ route('registration') }}">
    {{ csrf_field() }}
    <input type="hidden" name="level" value="1" />
    <div class="row">
      <div class="column large-3 large-offset-2 text-right">Name:</div>
      <div class="column large-4">
        <input type="text" required class="text-center" name="name" value="{{ old('name') }}" />
      </div>

      <div class="column large-3 large-offset-2 text-right">Email:</div>
      <div class="column large-4">
        <input type="email" class="text-center" name="email" value="{{ old('email') }}" />
      </div>

      <div class="column"><hr /></div>

      <div class="column large-3 large-offset-2 text-right">Username:</div>
      <div class="column large-4">
        <input type="text" required class="text-center" name="username" value="{{ old('username') }}" />
      </div>

      <div class="column large-3 large-offset-2 text-right">Password:</div>
      <div class="column large-4">
        <input type="password" required class="text-center" name="password" value="{{ old('password') }}" />
      </div>

      <div class="column large-3 large-offset-2 text-right">Confirm Password:</div>
      <div class="column large-4">
        <input type="password" required class="text-center" name="password_confirmation" value="{{ old('password_confirmation') }}" />
      </div>

      <div class="column large-4 large-offset-5 end">
        <input type="submit" class="button expanded" value="Register" />
      </div>
    </div>
  </form>
@endsection
