@extends('library')
@section('title', 'My Collection')
@section('content')
  @include("library.partials.search-form")
  <h2 class="row column">My Collection</h2>
  <div id="mi-collezione" class="row small-up-2 medium-up-8" style="padding-bottom:5rem"></div>
  <script type="text/template" id="collezione-item">
    <div class="column colleziones">
      <div class="card">
        <a href="%src%"><img src="%img%" /></a>
        <button onclick="uncollect(%id%);location.reload();"><i class="fa fa-times"></i></button>
        <div class="card-content">
          <a href="%src%">%name%</a>
        </div>
      </div>
    </div>
  </script>
  <script>
    $(function() {
      $.ajax({
        url: "{{ url('find-ranges') }}",
        type: "get",
        data: {ids: JSON.stringify($collection.reverse())},
        success: function(response) {
          if (response != undefined && response.length > 0) {
            $.each(response, function(i, e) {
              var h = $("#collezione-item").html().replace("%src%", $("base").attr("href")+'/library/product/'+e.id).replace("%img%", $("base").attr("href")+'/'+e.thumbnail).replace("%name%", e.name).replace("%id%", e.id);
              $("#mi-collezione").append(h);
            });
          } else {
            $("#mi-collezione").append("<p>You have not collected anything yet. Click <a href='{{ route('search') }}'>here</a> to view products.</p>");
          }
        }
      });
    });
  </script>
  <style>
    #search-form .end,
    #search-filter a {
        display: none !important;
    }
    .card-content {
        padding: 0.5rem;
    }
    [data-toggle="offCanvas"] {
        display: none !important;
    }
    .colleziones {
      position: relative;
    }
    .colleziones button {
      position: absolute;
      top: 0.3rem;
      right: 1.3rem;
      color: #fff;
      padding: 0.2rem 0.4rem;
      border: 1px solid transparent;
    }
    .colleziones button:hover {
      border: 1px solid #fff;
      border-radius: 2px;
    }
  </style>
@endsection
