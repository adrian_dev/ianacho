@extends('library')
@section('content')
  @include("library.partials.search-form")

  @if (isset($item))
    <?php $gallery = unserialize($item->product->gallery); ?>
    <?php $gallery = !empty($gallery) ? array_slice($gallery, 0, 3) : array(); ?>
    <div class="row" id="product-information">
      <div class="column medium-4" id="orbiter">
        <div class="orbit" data-orbit data-timer-delay="10000">
          <div class="orbit-wrapper">
            <div class="orbit-controls">
              <div class="orbit-previous"><i class="fa fa-chevron-left"></i></div>
              <div class="orbit-next"><i class="fa fa-chevron-right"></i></div>
            </div>
          </div>
          <ul class="orbit-container">
            <li class="is-active orbit-slide">
              <figure class="orbit-figure">
                <a href="{{ asset($item->getImage()) }}" data-fancybox="gallery" data-options='{"hash":false}'>
                  <img class="orbit-image" src="{{ asset('images/blank.png') }}" style="background:url('{{ asset($item->getImage()) }}') no-repeat center;background-size:cover;">
                </a>
              </figure>
            </li>
            @if (!empty($gallery))
              @foreach ($gallery as $i => $pic)
                <li class="orbit-slide">
                  <figure class="orbit-figure">
                    <a href="{{ asset($pic["image"]) }}" data-fancybox="gallery">
                      <img class="orbit-image" src="{{ asset('images/blank.png') }}" style="background:url('{{ asset($pic["image"]) }}') no-repeat center;background-size:cover;">
                    </a>
                  </figure>
                </li>
              @endforeach
            @endif
          </ul>
          <nav class="orbit-bullets">
            <button class="is-active" data-slide="0" style="width:40px;height:40px;">
              <img src="{{ asset($item->getThumbnail()) }}" width="60" height="auto">
            </button>
            @if (!empty($gallery))
              @foreach ($gallery as $i => $pic)
                <button data-slide="{{ $i + 1 }}" style="width:40px;height:40px;">
                  <img src="{{ asset($pic["thumbnail"]) }}" width="60" height="auto">
                </button>
              @endforeach
            @endif
          </nav>
        </div>
      </div>
      <div class="column medium-8">
        <h4>{{ ucwords($item->product->getName()) }}</h4>
        <h2 style="overflow-wrap:break-word;">
          {{ ucwords(str_replace("_", " ",$item->getName())) }}
        </h2>
      </div>
      <div class="column medium-8">
        <ul class="tabs" data-tabs id="product-tabs">
          <li class="tabs-title is-active"><a href="#panel1" aria-selected="true">DETAILS</a></li>
          <li class="tabs-title"><a href="#panel2">SUPPLIER</a></li>
        </ul>
        <div class="tabs-content" data-tabs-content="product-tabs">
          <div class="tabs-panel is-active" id="panel1">
            <table class="unstriped">
              <tr><td width="150"><b>Brand:</b></td><td>{{ $item->product->getBrand() }}</td></tr>
              <tr><td><b>Category:</b></td><td>{{ $item->product->getCategoryValue() }}</td></tr>
              @if (auth()->check())
                <tr>
                  <td><b>Indicative Price:</b></td>
                  <td>
                    {!! !empty($item->product->getMinPrice()) ? '$'.$item->product->getMinPrice() : '<em>Contact Supplier</em>' !!}
                    {{ !empty($item->product->getMaxPrice()) ? ' - '.$item->product->getMaxPrice() : '' }}
                  </td>
                </tr>
                <tr><td><b>Lead Time:</b></td><td>{{ $item->product->getLeadTime() }} Week/s</td></tr>
              @endif
              <tr>
                <td colspan="2" class="product-description">{!! $item->product->getDescription() !!}</td>
              </tr>
              <tr>
                <td><b>Tags:</b></td>
                <td>
                  @foreach ($item->product->tags as $tag)
                    <a href="{{ route('search', ['q' => $tag->record->getPhrase()]) }}" class="button secondary small">
                      <i class="fa fa-tag"></i>
                      {{ $tag->record->getPhrase() }}
                    </a>
                  @endforeach
                </td>
              </tr>
            </table>
          </div>
          <div class="tabs-panel" id="panel2">
            <h5>
              <a href="{{ $item->product->supplier->getWebsite() }}" target="_blank">
                {{ $item->product->supplier->getName() }}
              </a>
              <a href="{{ $item->product->supplier->getWebsite() }}" class="float-right" target="_blank">
                {{--  <img src="{{ asset($item->product->supplier->getLogo()) }}">  --}}
                <img src="http://productivity2.mmoser.com/library/public/{{ $item->product->supplier->getLogo() }}">
              </a>
            </h5>
            <div>{!! nl2br($item->product->supplier->getAddress()) !!}</div>
            <div class="row expanded medium-up-2">
              @foreach ($item->product->supplier->contacts as $contact)
                <div class="column">
                  <div class="card">
                    <div class="card-divider">{{ $contact->getName() }}</div>
                    <div class="card-content">
                      <div>{{ $contact->getPhone() }}</div>
                      <a href="mailto:{{ $contact->getEmail() }}">
                        {{ $contact->getEmail() }}
                      </a>
                    </div>
                  </div>
                </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
      @if (!empty($related))
        <div class="column">
          <h4>
            RELATED PRODUCTS
            <a class="float-right button hollow" href="{{ route('search', ['q' => $item->product->getName()]) }}">
              MORE <i class="fa fa-chevron-right"></i>
            </a>
          </h4>
          <div class="row expanded small-up-2 medium-up-6" data-equalizer>
            @foreach ($related as $row)
              <div class="column item">@include("library.partials.item", ["item" => $row])</div>
            @endforeach
          </div>
        </div>
      @endif
      @if (!empty($more))
        <div class="column">
          <hr />
          <h4>
            MORE FROM <strong>{{ strtoupper($item->product->supplier->getName()) }}</strong>
            <a class="float-right button hollow" href="{{ url('catalog', ['id' => $item->product->supplier->getID()]) }}">
              VIEW CATALOG <i class="fa fa-chevron-right"></i>
            </a>
          </h4>
          <div class="row expanded small-up-2 medium-up-6" data-equalizer>
            @foreach ($more as $row)
              <div class="column item">@include("library.partials.item", ["item" => $row])</div>
            @endforeach
          </div>
        </div>
      @endif
    </div>
  @endif
  <style>#search-form .end, #search-filter a {display:none !important;}</style>
@endsection
