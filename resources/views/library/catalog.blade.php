@extends("library")

@section("content")
@include("library.partials.search-form")
<section id="supplier-catalog">
  @if (isset($supplier))
    <div class="row" style="margin-top:2rem;margin-bottom:5rem;">
      <div class="column"><h3>Supplier Catalog</h3></div>
      <div class="column" style="padding-bottom:2rem;">
        <img src="{{ asset($supplier->getLogo() ?: 'images/logo_placeholder.png') }}" class="float-right" style="max-height:150px;">
        <h2>
          <a href="{{ strpos($supplier->getWebsite(),'http://') === false ? 'http://'.$supplier->getWebsite() : $supplier->getWebsite() }}">
            {{ $supplier->getName() }}
          </a>
        </h2>
        <p>{!! nl2br($supplier->getAddress()) !!}</p>
      </div>
      <div class="column">
        <div class="row medium-up-2">
          @if (!empty($supplier->contacts()->where("status", 1)->get()))
            @foreach ($supplier->contacts()->where("status", 1)->get() as $contact)
              <div class="column">
                <div class="card">
                  <div class="card-divider">{{ $contact->getName() }}</div>
                  <div class="card-content" style="padding:1rem;">
                    <div>{{ $contact->getPhone() }}</div>
                    <div><a href="mailto:{{ $contact->getEmail() }}">{{ $contact->getEmail() }}</a></div>
                  </div>
                </div>
              </div>
            @endforeach
          @endif
        </div>
      </div>
    </div>
  @endif

  @if (!$products->isEmpty())
    <div class="row" style="padding-bottom:5rem;">
      <?php $b = ''; $c = ''; ?>
      @foreach ($products as $product)
        @if ($b != $product->getBrand())
          <?php $b = $product->getBrand(); ?>
          <div class="column"><h4><a href="{{ route('search', ['q' => $b]) }}">{{ $b }}</a></h4></div>
        @endif

        @if ($c != $product->getCategoryValue())
          <?php $c = $product->getCategoryValue(); ?>
          <div class="column"><h5><a href="{{ route('search', ['q' => $c]) }}">{{ $c }}</a></h5></div>
        @endif
        <div class="column">
          <h3 style="word-break:break-all;">{{ $product->getName() }}</h3>
        </div>
        <div class="column" style="padding-bottom:3rem;">
          <section class="items">
            <div class="row medium-up-7 small-up-2" data-equalizer>
              @foreach ($product->_range() as $item)
                <div class="column item">
                  @include("library.partials.item", ["item" => $item])
                </div>
              @endforeach
            </div>
          </section>
        </div>
      @endforeach
    </div>
  @else
    <h3 class="row column" style="padding-bottom:5rem;">No products yet.</h3>
  @endif
</section>
<style>#search-form .end, #search-filter a, .card-supplier, .card-category, .card-brand {display:none !important;}.card-title a {font-weight:500;}</style>
@endsection