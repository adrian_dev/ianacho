@extends("library")
@section("content")
  @include("library.partials.search-form")

  @if (isset($items) && !empty($items))
    <section id="search-result" class="items">
      <div class="row medium-up-5 large-up-7 small-up-2 expanded" data-equalizer style="background:#efefef;">
        @foreach ($items as $item)
          <div class="column item">
            @include("library.partials.item", ["item" => $item])
          </div>
        @endforeach
      </div>
      <div class="row column text-center expanded" style="background:#efefef;">{!! $pagination->appends(request()->input()) !!}</div>
    </section>
  @endif
  <style>
    .pagination li {
      display: inline-block;
      border: 1px solid #ccc;
      border-radius: 0.2rem;
    }
    .pagination .active {
      font-weight: 700;
      border: none;
      padding: 0 1rem;
    }
  </style>
@endsection
