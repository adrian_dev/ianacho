@extends("library")
@section("content")
  <section id="latest" class="items">
    <div class="row" style="padding-top:2rem;">
      <div class="column medium-10">
        <h3>New and Updated</h3>
      </div>
      <div class="column medium-2 text-right">
        <a href="{{ route('search') }}" class="button">View All <i class="fa fa-chevron-right"></i></a>
      </div>
    </div>
    <div class="row medium-up-5 small-up-2" data-equalizer>
      @if (!$latest->isEmpty())
        @foreach($latest as $product)
          <?php $range = $product->range()->where("status", 1)->first(); ?>
          <div class="column item">
            @include('library.partials.item', ['item' => $range])
          </div>
        @endforeach
      @endif
    </div>
    <div class="row" style="margin-top:2rem;">
      <div class="column medium-10">
        <h3>Suppliers</h3>
      </div>
      <div class="column medium-2 text-right">
        <a href="{{ route('suppliers') }}" class="button">View All <i class="fa fa-chevron-right"></i></a>
      </div>
    </div>
    <div class="row medium-up-5 small-up-2" data-equalizer style="padding-bottom:4rem;">
      @if (!$suppliers->isEmpty())
        @foreach($suppliers as $supplier)
          <div class="column item">
            <div class="card" data-equalizer-watch>
              <a href="{{ url('catalog', ['id' => $supplier->getID()]) }}" style="margin:0.5rem;display:inline-block;background:url('{{ asset($supplier->getLogo() ?: 'images/logo_placeholder.png') }}') no-repeat center;background-size:100%;">
                <img src="{{ asset('images/blank.png') }}" height="350">
              </a>
              <div class="card-content">
                <a href="{{ url('catalog', ['id' => $supplier->getID()]) }}">{{ $supplier->getName() }}</a>
              </div>
            </div>
          </div>
        @endforeach
      @endif
    </div>
  </section>
@endsection
