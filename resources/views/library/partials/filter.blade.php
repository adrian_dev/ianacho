<section>
  <div class="row">
    <div class="column medium-10">
      <form method="get" action="{{ route('search') }}">
        <input type="search" name="q" value="{{ request()->input('q') }}" placeholder="search">
        <button type="submit"><i class="fa fa-search"></i></button>
      </form>
    </div>
    <div class="column medium-2">

    </div>
  </div>
</section>