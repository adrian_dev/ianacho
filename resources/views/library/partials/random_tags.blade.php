@if (isset($tags) && !empty($tags))
  <div id="random-tags" class="column medium-10 medium-offset-1 end">
    <h5 class="text-center">Popular Searches</h5>

    <div class="text-center">
      @foreach ($tags as $tag)
        <a href="{{ route('search', ['q' => $tag->getPhrase()]) }}">{{ $tag->getPhrase() }}</a>
      @endforeach
    </div>
  </div>
@endif