<form method="get" id="search-form" action="{{ route('search') }}">
  <div class="row expanded">
    <div class="column medium-8">
      <input type="search" id="q" name="q" value="{{ request()->input('q') }}" placeholder="Search" autofocus>
      <button type="submit"><i class="fa fa-search"></i></button>
    </div>
    <div class="column medium-1 small-6" id="search-filter">
      <a href="javascript:void(0);" onclick="$('#filter-fields').toggleClass('hide');$(this).children('i').toggleClass('fa-chevron-down').toggleClass('fa-chevron-up');">
        FILTER
        <i class="fa fa-chevron-down"></i>
      </a>
    </div>
    <div class="column medium-3 small-6 text-right">
      <a href="javscript:void(0);" style="margin-top:0.5rem;display:inline-block;" data-toggle="offCanvas">FAVORITES <i class="fa fa-heart"></i></a>
    </div>
    <div class="column hide" id="filter-fields">
      <div class="row">
        <div class="column medium-2">
          <h5>Green Mark:</h5>
          <label for="green-1">
            <input type="radio" name="green_mark" id="green-1" value="" {{ !request()->has('green_mark') ? 'checked' : '' }}>
            Any
          </label>
          <label for="green-2">
            <input type="radio" name="green_mark" id="green-2" value="0" {{ request()->has('green_mark') && request()->input('green_mark') == 0 ? 'checked' : '' }}>
            No
          </label>
          <label for="green-3">
            <input type="radio" name="green_mark" id="green-3" value="1" {{ request()->has('green_mark') && request()->input('green_mark') == 1 ? 'checked' : '' }}>
            Yes
          </label>
        </div>
        <div class="column medium-3">
          <h5>Indicative Price:</h5>

          <div class="slider" data-slider data-initial-start="{{ request()->input('price_min') ?: 0 }}" data-initial-end="{{ request()->input('price_max') ?: 1000 }}" data-start="0" data-end="1000">
            <span class="slider-handle" data-slider-handle role="slider" tabindex="1" aria-controls="price-min"></span>
            <span class="slider-fill" data-slider-fill></span>
            <span class="slider-handle" data-slider-handle role="slider" tabindex="1" aria-controls="price-max"></span>
          </div>
          <span>$</span>
          <input type="number" name="price_min" id="price-min" max="999" />
          <span>-</span>
          <input type="number" name="price_max" id="price-max" max="1000" />
        </div>
        <div class="column medium-3">
          <h5>Lead Time:</h5>
          <div class="slider" data-slider data-initial-start="{{ request()->input('lead-time') ?: 12 }}" data-end="12">
            <span class="slider-handle" data-slider-handle role="slider" tabindex="1" aria-controls="lead-time"></span>
            <span class="slider-fill" data-slider-fill></span>
          </div>
          <input type="text" name="lead_time" id="lead-time" value="12" />
          Weeks
        </div>
        <div class="column medium-3 medium-offset-1 end">
          <button type="submit" class="button expanded">APPLY FILTERS</button>
        </div>
      </div>
    </div>
    <div class="column end">
      <strong style="font-size:1.4rem; margin-right:1rem;">Search Results</strong>
      <a href="javascript:void(0);" data-toggle="sort-dropdown">
        SORT <i class="fa fa-sort"></i>
      </a>
      <div class="dropdown-pane bottom" id="sort-dropdown" data-dropdown>
        <label for="sort-date">
          <input type="radio" name="sort" id="sort-date" value="date" onchange="$('#search-form').submit();" {{ request()->has('sort') && request()->input('sort') == 'date' ? 'checked' : '' }} />
          Latest First
        </label>
        <label for="sort-name">
          <input type="radio" name="sort" id="sort-name" value="name" onchange="$('#search-form').submit();" {{ request()->has('sort') && request()->input('sort') == 'name' ? 'checked' : '' }} />
          Name
        </label>
        <!--label for="sort-relevance">
          <input type="radio" name="sort" id="sort-relevance" value="relevance" onchange="$('#search-form').submit();" {{ !request()->has('sort') ? 'checked' : '' }} />
          Relevance
        </label-->
      </div>
    </div>
  </div>
</form>

<div class="off-canvas-wrapper">
  <div class="off-canvas position-right" id="offCanvas" data-off-canvas style="z-index:3;">
    <h6 style="padding:1rem;"><strong>FAVORITES</strong> <a href="{{ url('collection') }}" id="all-collection" class="float-right">ALL <i class="fa fa-chevron-right"></i></a></h6>
    <div id="some-collection" class="row expanded small-up-2"></div>
  </div>
</div>