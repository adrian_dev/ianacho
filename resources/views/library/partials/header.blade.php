<header>
  <div class="row expanded">
    <div class="medium-2 columns small-4">
      <div class="mainmenu">
        <a data-toggle="menu">
          <span class="menutext hide-for-small-only">MENU</span>
          <i class="fa fa-bars"></i>
        </a>

        <div class="dropdown-pane bottom" id="menu" data-dropdown data-close-on-click="true">
          <ul class="lists">
            <li><a href="{{ route('home') }}">Home</a></li>
            <li><a href="{{ route('search') }}">All Products</a></li>
            <li><a href="{{ route('suppliers') }}">Suppliers</a></li>
            <li><a href="{{ route('collection') }}">My Collection</a></li>
            @if (auth()->check())
              <li><a href="{{ route('requests') }}">My Requests</a></li>
            @endif
            <hr />
            @if (auth()->check())
              <li><a href="{{ route('admin.logout') }}">Logout</a></li>
            @else
              <li><a href="{{ route('admin.login') }}">Login</a></li>
            @endif
          </ul>
        </div>
      </div>
    </div>

    <div class="medium-2 medium-offset-3 columns small-3 small-offset-5">
      <a href="{{ route('home') }}">
        <img src="{{ asset('img/ipenny-text.svg') }}" />
      </a>
    </div>

    <div class="medium-2 medium-offset-3 columns hide-for-small-only end">
      <img src="{{ asset('img/mmoser-logo.svg') }}" />
    </div>

    @if (url()->current() == route('home'))
      @include ("library.partials.search")

      @if (isset($randomTags))
        @include ("library.partials.random_tags", ["tags" => $randomTags])
      @endif
    @endif
  </div>
</header>