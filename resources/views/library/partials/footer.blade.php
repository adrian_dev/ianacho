<footer>
  <div class="row expanded" style="padding:2.5rem 0;">
    <div class="column small-2">
      <img src="{{ asset('img/ipenny-logo-holes.svg') }}">
    </div>
    <div class="column small-10 medium-6">
      <img src="{{ asset('img/ipenny-small-text.png') }}">
      <p><small>The Materials Library is a place where you can browse for product brochures Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small></p>
      <img src="{{ asset('img/sg-map.gif') }}">
    </div>
    <div class="column medium-2">
      <ul class="no-bullet">
        <li><b>NAVIGATION</b></li>
        <li><a href="{{ route('home') }}"><small>Home</small></a></li>
        <li><a href="{{ route('search') }}"><small>Search</small></a></li>
        <li><a href="{{ route('suppliers') }}"><small>Suppliers</small></a></li>
        <li><a href="{{ route('collection') }}"><small>My Collection</small></a></li>
      </ul>
    </div>
    <div class="column medium-2">
      <ul class="no-bullet">
        <li><b>LEARN MORE</b></li>
        <li><a><small>Knowledge Center</small></a></li>
        <li><a><small>M Moser Learning</small></a></li>
        <li><a><small>Marcomms</small></a></li>
        <li><a><small>Design</small></a></li>
      </ul>
    </div>
    <div class="column medium-10 medium-offset-1">
      <hr />
    </div>
    <div class="column medium-8 medium-offset-2 text-center end">
      <p>© M Moser Associates Singapore</p>
      <p><a href="http://www.mmoser.com">mmoser.com</a></p>
    </div>
  </div>
</footer>
<style>
  footer {
    color: #fff;
  }
  footer a {
    color: #fff;
  }
</style>