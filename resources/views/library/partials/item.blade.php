@if(isset($item))
  <div class="card">
    <div class="card-divider">
      <a href="{{ asset($item->getImage()) }}"
         class="product-view"
         data-caption="{{ $item->getName() }}"
         data-fancybox="group"
         data-type="image">
          <i class="fa fa-search-plus"></i>
      </a>
      <a href="javscript:void(0);" class="product-collect float-right" data-id="{{ $item->getID() }}">
        <i class="fa fa-heart-o"></i>
      </a>
      @if(auth()->check())
        <a href="" class="product-request float-right">
          <i class="fa fa-plus"></i>
        </a>
      @endif
    </div>

    <a href="{{ url('library/product', ["id" => $item->getID()]) }}">
      <img src="{{ !empty($item->getThumbnail()) ? asset($item->getThumbnail()) : 'http://via.placeholder.com/350x350' }}" style="min-height:3rem;" />
    </a>

    <div class="card-content" data-equalizer-watch>
      @if (strtotime($item->created_at) > strtotime('-7 day'))
        <em class="new">NEW</em>
      @elseif(strtotime($item->updated_at) > strtotime('-7 day'))
        <em class="updated">UPDATED</em>
      @endif

      <div class="card-title">
        <a href="{{ url('library/product', ["id" => $item->getID()]) }}">
          {{ ucwords(str_replace(["_", "%20"], " ", strlen($item->getName()) > 25 ? substr(rawurldecode($item->getName()), 0, 25).'&hellip;' : $item->getName())) }}
        </a>
      </div>
      <div class="card-supplier">
        <a href="{{ url('catalog', ['id' => $item->product->supplier->getID()]) }}">
          {{ str_ireplace(['pte ltd', '(singapore)', '(s)', 'singapore'], '', $item->product->supplier->getName()) }}
        </a>
      </div>
      <div class="card-category">{{ $item->product->getCategoryValue() }}</div>
      <div class="card-brand">{{ $item->product->getBrand() }}</div>
      <div class="card-price">
        @if (auth()->check())
          <small>{{ $item->product->getMinPrice() > 0 ? '$'.$item->product->getMinPrice() : '' }}</small>
        @endif
        @if (auth()->check())
          <small>{{ $item->product->getMaxPrice() > 0 ? ' - $'.$item->product->getMaxPrice() : '' }}</small>
        @endif
      </div>
    </div>
  </div>
@endif