<div id="search" class="column medium-8 medium-offset-2" style="margin-top:2rem;">
  <form method="get" action="{{ route('search') }}">
    <input type="search" name="q" placeholder="Search for a product" value="{{ request()->input('q') }}">
    <button type="submit"><i class="fa fa-search"></i></button>
  </form>
</div>
