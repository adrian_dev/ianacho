@extends('library')
@section('title', 'Suppliers')
@section('content')
  @include("library.partials.search-form")
  <h1 class="text-center">Suppliers</h1>
  @if (isset($suppliers) && !empty($suppliers))
  <div id="suppliers-list" class="row small-up-2 medium-up-4 large-up-6" data-equalizer>
    @foreach ($suppliers as $supplier)
    <div class="column text-center" data-equalizer-watch>
      <a href="{{ route('catalog', ['id' => $supplier->getID()]) }}" title="{{ $supplier->getName() }}">
        <img src="{{ asset($supplier->getLogo() ?: 'images/logo_placeholder.png') }}" alt="{{ $supplier->getName() }}" />
      </a>
      <small style="color:#888;"><strong>{{ $supplier->getName() }}</strong></small>
    </div>
    @endforeach
  </div>
  @endif
  <script>
    $(function() {
      $.each($("#suppliers-list .column"), function(i, elem) {
        var $h = $(this).height();
        var $s = $(this).children('small').height();
        var $p = $(this).children('a').children('img').height();

        var $height = Math.floor((($h - $s) - $p) / 2);
        if ($height > 0) {
          $(this).children('a').children('img').css('padding-top', $height+'px');
        }
      });
    });
  </script>
  <style>#search-form .end, #search-filter a {display:none !important;}</style>
@endsection