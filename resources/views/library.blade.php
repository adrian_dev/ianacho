<!DOCTYPE html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield("title")</title>
    <base href="{{ url('/') }}" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/vendor/foundation.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/vendor/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/styles.css') }}">
    <link rel="stylesheet" href="{{ asset('css/vendor/jquery.fancybox.min.css') }}">
    <script type="text/javascript" src="{{ asset('js/vendor/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/jquery.fancybox.min.js') }}"></script>
  </head>
  <body id="@yield('id')">
    @include ("library.partials.header")

    <main>@yield("content")</main>

    @include ("library.partials.footer")

    <script type="text/javascript" src="{{ asset('js/vendor/what-input.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/foundation.min.js') }}"></script>
    <script>$(document).foundation();</script>
    <script type="text/template" id="collected-card">
      <div class="column">
        <a href="%src%"><img src="%img%" /></a>
        <button data-id="%id%"><i class="fa fa-times"></i></button>
      </div>
    </script>
    <script>
    var $collection = JSON.parse(localStorage.getItem("myCollection"));
    
    if ($collection == null) {
      $collection = [];
      localStorage.setItem("myCollection", JSON.stringify($collection));
    } else {
      refreshCollection();
    }

    function refreshCollection() {
      $.each($(".product-collect"), function(i, e) {
        if ($collection.indexOf($(e).data("id")) >= 0) {
          $(e).children(".fa").removeClass("fa-heart-o").addClass("fa-heart");
        } else {
          $(e).children(".fa").removeClass("fa-heart").addClass("fa-heart-o");
        }
      });
    }

    function collect(id) {
      $collection.push(parseInt(id));
      localStorage.setItem("myCollection", JSON.stringify($collection));
    }

    function uncollect(id) {
      var index = $collection.indexOf(parseInt(id));
      if (index > -1) {
        $collection.splice(index, 1);
      }
      localStorage.setItem("myCollection", JSON.stringify($collection));
    }

    $(function() {
      $(".product-collect").click(function() {
        var fa = $(this).children(".fa");

        if ($(fa).hasClass("fa-heart-o")) {
          collect($(this).data("id"));
        } else {
          uncollect($(this).data("id"));
        }

        $(fa).toggleClass("fa-heart-o").toggleClass("fa-heart");
      });

      $("[data-toggle='offCanvas']").click(function() {
        refreshSomeCollection();
      });
    });

    function refreshSomeCollection() {
      $.ajax({
        url: "{{ url('find-ranges') }}",
        type: "get",
        data: {ids: JSON.stringify($collection.reverse())},
        success: function(response) {
          $("#some-collection").html("");

          if (response != undefined && response.length > 0) {
            $.each(response.slice(0, 6), function(i, e) {
              $("#some-collection").append($("#collected-card").html().replace("%src%", $("base").attr("href")+'/product/'+e.id).replace("%img%", $("base").attr("href")+'/'+e.thumbnail).replace("%id%", e.id));
            });

            $("#some-collection .column button").click(function() {
              uncollect($(this).data("id"));
              refreshSomeCollection(response);
              refreshCollection();
            });
          }
        }
      });
    }
    </script>
    <style>
      #some-collection .column {
        margin-bottom: 1.2rem;
        position: relative;
      }

      #some-collection .column button {
        position: absolute;
        top: 0;
        right: 1rem;
        color: rgba(255, 255, 255, 0.5);
        width: 1rem;
        height: 1rem;
      }

      #some-collection .column button:hover {
        color: rgba(255, 255, 255, 1);
      }
    </style>
  </body>
</html>