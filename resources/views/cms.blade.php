<!DOCTYPE html>
<html>
  <head>
  	<meta charset=utf-8 />
    <meta name="viewport" content="width=device-width, initial-scale=1">
  	<title>@yield('title')</title>

    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('favicon/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('favicon/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @yield('meta')
    <base href="{{ route('admin.dashboard') }}" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  	<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/admin-base.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/cms.css') }}" />
  	<script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
  	<script type="text/javascript" src="{{ asset('js/jquery-ui.js') }}"></script>
    <script src="{{ asset('js/foundation.js') }}"></script>
    <script src="{{ asset('js/what-input.js') }}"></script>
  	<!--[if IE]>
  		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  	<![endif]-->
  </head>
  <body class="pages home">
    <div id="upperblock">
      @include('admin.widgets.header')
    </div>
    <main>
      <div id="content">
        @if(session()->has("message"))
          <div class="row">
            <div class="column">
              <div class="callout success">
                <i class="fa fa-check-square-o"></i> {!! session("message") !!}
              </div>
            </div>
          </div>
        @elseif (session()->has("error"))
          <div class="row">
            <div class="column">
              <div class="callout alert">
                <i class="fa fa-times-circle-o"></i> {{ session("error") }}
              </div>
            </div>
          </div>
        @endif

        @yield('content')
      </div>
    </main>
    <footer id="footer">
      <div class="footer-bottom row">
        <div class="column">
          <p>&copy; M Moser Associates Singapore</p>
          <p><a href="http://www.mmoser.com">mmoser.com</a></p>
        </div>
      </div>
    </footer>
    <script type="text/javascript" src="{{ asset('js/admin.js') }}"></script>
  </body>
</html>
