@extends('ipenny')
@section('title', 'Quote Request')
@section('content')
  <div id="requests" class="row">
    <div class="column"><h1>Requests to Suppliers</h1></div>
    @foreach ($suppliers as $supplier)
      <div class="column">
        <div class="row">
          <div class="column">
            <h3>{{ $supplier->getName() }}</h3>
            <a href="mailto:{{ $supplier->contacts()->first()->getEmail() }}?Subject=Product%20Inquiry&Body={{ rawurlencode('Hi '.$supplier->contacts()->first()->getName().",\n\nWe would like some information regarding some products listed in the link.\n".route('supplier.requests')) }}" class="button small hollow secondary">
              <i class="fa fa-envelope-o"></i>
              Email Requests to Supplier
            </a>
          </div>
        </div>
        <div class="row small-up-2 medium-up-3 large-up-5" data-equalizer data-equalize-on="large">
          @foreach ($products[$supplier->getID()] as $product)
            @include('creatives.widgets.card', [
              'product' => $product,
              'query' => '',
              'replacement' => ''])
          @endforeach
        </div>
        <hr />
      </div>
    @endforeach
  </div>
  <style>
  .listview {
    display: none;
  }
  .unfavorite {
    display: none !important;
  }
  </style>
@endsection
