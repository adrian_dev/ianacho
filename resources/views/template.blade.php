<!DOCTYPE HTML>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @yield('meta')
    <base href="{{ url('/') }}" />
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/supplier.css') }}">
    <script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/foundation.js') }}"></script>
  </head>
  <body class="pages">
    @include('supplier.widgets.header')
    <main role="main" id="content">
      <div class="row expanded">
        @yield('content')
      </div>
      <br /><br /><br />
      <div class="row">
        <div class="column text-right">
          <a href="mailto:AdrianV@mmoser.com" class="button small secondary hollow">Feedback?</a>
        </div>
      </div>
    </main>
    <footer id="footer">
      @include('supplier.widgets.footer')
    </footer>
    <script type="text/javascript" src="{{ asset('js/supplier.js') }}"></script>
    @yield('scripts')
  </body>
</html>
