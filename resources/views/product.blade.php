<!DOCTYPE HTML>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    @yield('meta')
    <base href="{{ url('/') }}" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/foundation.min.css') }}">
    @yield('css')
    <script type="text/javascript" src="{{ asset('js/vendor/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/foundation.min.js') }}"></script>
    @yield('js')
    <style>
    html {
      font-family: 'Open Sans', sans-serif;
    }
    </style>
  </head>
  <body>
    <header class="row expanded">
      <div class="column large-2 small-2">
        <a href="javascript:void(0);" data-toggle="main-menu">
          <span class="hide-for-small-only">MENU</span>
          <i class="fa fa-bars"></i>
        </a>

        <div id="main-menu" class="dropdown-pane bottom" data-dropdown data-v-offset="10" data-h-offset="0" data-close-on-click="true">
          <ul class="lists">
            <li><a href="{{ route('profile') }}">Profile</a></li>
            <li><a href="{{ route('new') }}">Add New Product</a></li>
            <li><a href="{{ route('supplier.requests') }}">Requests</a></li>
            <hr />
            @if (auth('supplier')->check())
              <li><a href="{{ route('logout') }}">Logout</a></li>
            @else
              <li><a href="{{ route('login') }}">Login</a></li>
            @endif
          </ul>
        </div>
      </div>
      <div class="column large-2 large-offset-3 small-3 small-offset-7 end">
        <a href="{{ route('profile') }}">
          <img src="{{ asset('img/ipenny-text.svg') }}" />
        </a>
      </div>
      <div class="column large-3 large-offset-2 text-right end hide-for-small-only">
        <img src="{{ asset('img/mmoser-logo.svg') }}" />
      </div>
    </header>
    <main>
      @if(session()->has("message"))
        <div class="row">
          <div class="column">
            <div class="callout success">{{ session("message") }}</div>
          </div>
        </div>
      @endif

      @yield('content')
    </main>
    <footer>
      <div class="row expanded">
        <div class="column large-1">
          <img src="{{ asset('img/ipenny-logo-holes.svg') }}">
        </div>
        <div class="column large-8">
          <img class="ipenny-small-text" src="{{ asset('img/ipenny-small-text.png') }}">
          <p>
            The Materials Library is a digital catalog for supplier products.
            It allows for easy viewing of products just by searching for terms.
          </p>
          <img src="{{ asset('img/sg-map.gif') }}">
        </div>
        <div class="column large-3">
          <ul class="lists">
            <li><a href="{{ route('profile') }}">Profile</a></li>
            <li><a href="{{ route('new') }}">Add New Product</a></li>
            <li><a href="{{ route('supplier.requests') }}">Requests</a></li>
            @if (auth('supplier')->check())
              <li><a href="{{ route('logout') }}">Logout</a></li>
            @else
              <li><a href="{{ route('login') }}">Login</a></li>
            @endif
          </ul>
        </div>
        <div class="column large-8 text-center large-centered">
          <hr />
          <p><small>&copy; M Moser Associates Singapore</small></p>
          <p><small><a href="http://www.mmoser.com/" target="_blank">mmoser.com</a></small></p>
        </div>
      </div>
    </footer>
    <script>
    $(document).foundation();
    </script>
    @yield('scripts')
  </body>
</head>
