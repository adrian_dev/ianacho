<!DOCTYPE HTML>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>iPenny - @yield('title')</title>
    @yield('meta')
    <base href="{{ url('/') }}" />
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/creatives.css') }}">
    <script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
  </head>
  <body class="pages home">
    <div id="upperblock">
      @include('creatives.widgets.header')

      @include('creatives.widgets.searchbar')

      <div id="popularsearch">
        <h4>Popular Searches:</h4>
        <ul class="inline-list"> @if(isset($randomTags)) @foreach($randomTags as $tag)
          <li><a href="{{ route('archive', ['q' => urlencode($tag->getPhrase())]) }}">{{ $tag->getPhrase() }}</a>
          </li> @endforeach @endif </ul>
      </div>
      <!-- #searchbar -->
    </div>

    <div id="content">
      @yield('content')
    </div> <!-- end #content -->

    <div id="footer">
      @include('creatives.widgets.footer')
    </div> <!-- #footer -->

    <script src="{{ asset('js/what-input.js') }}"></script>
    <script src="{{ asset('js/foundation.js') }}"></script>
    <script src="{{ asset('js/ipenny.js') }}"></script>
  </body>
</html>
