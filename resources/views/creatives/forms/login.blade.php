@extends('ipenny')
@section('title', 'Login')
@section('content')
  <div class="row">
    <div class="column large-6 large-offset-3">
      <form method="post" action="{{ route('creatives.auth') }}">
        {{ csrf_field() }}
        <label>Username:</label>
        <input type="text" name="username" />
        <label>Password:</label>
        <input type="password" name="password" />
        <input type="submit" value="Login" class="button expanded" />
      </form>
    </div>
  </div>
@endsection
