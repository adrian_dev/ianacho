@extends('ipenny')

@section('title', 'Products')

@section('content')
  <div class="row expanded">
    @include('creatives.widgets.filters', ['filters' => $filters])

    <div class="large-9 columns">
      <section id="searchresults" class="sections">
        <div class="row">
          <div class="large-6 columns">
            <h2><!--span>{{ $products->count() }}</span--> Products found</h2>
          </div>

          <div class="large-6 columns">
            <div id="sortsec">
              <ul>
                <li class="sort">
                  <a href="javascript:void(0);" data-toggle="sort-dropdown"><span>Sort</span></a>
                </li>
                <li class="grid">
                  <a href="javascript;void(0);"><span>View</span></a>
                </li>
              </ul>

              <div class="dropdown-pane" id="sort-dropdown" data-dropdown data-v-offset="10" data-h-offset="0" data-close-on-click="true">
                <ul class="lists">
                  <li><a href="javascript:void(0);" data-sort="new">New</a></li>
                  <li><a href="javascript:void(0);" data-sort="price-desc">Price high to low</a></li>
                  <li><a href="javascript:void(0);" data-sort="price-asc">Price low to high</a></li>
                  <li><a href="javascript:void(0);" data-sort="rating">Rating</a></li>
                  <li><a href="javascript:void(0);" data-sort="name">Name</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>

        <div id="prod-grid" class="row small-up-2 medium-up-3 large-up-4" data-equalizer data-equalize-on="large">
          @foreach ($products as $product)
            @include('creatives.widgets.card', ['product' => $product, 'query' => '', 'replacement' => ''])
          @endforeach
        </div>

        <br />
        <div class="columns text-right">{{ $products->links() }}</div>
      </section>
      <!-- end new prod -->
    </div>
  </div>
@endsection
