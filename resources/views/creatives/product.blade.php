@extends('ipenny')

@section('title', 'Product')

@section('content')
  <div class="row expanded">
    <div class="large-5 columns">
      <section id="thumbsection">
        <div class="img-full">
          <img class="orbit-image" src="{{ asset($product->getImage()) }}" />
        </div>
      </section>
      <!-- thumbsection -->
    </div>
    <div class="large-7 columns">
      <section id="detailsection">
        <div class="addtofavorite">
          @if(auth()->check())
            <a href="javascript:void(0);" cclass="favbutton {{ $fav }}" data-id="{{ $product->getID() }}">
              <span></span>
            </a>
          @else
            <a href="{{ route('creatives.login') }}" class="favbutton"><span></span></a>
          @endif
        </div>
        <div class="product-name">
          <h1>
            {{ $product->getName() }}
            <span>
              by
              <i>{{ $product->supplier->getName() }}</i>
            </span>
          </h1>
        </div>
        <div class="star-rating">
          <span><i class="fa fa-star" aria-hidden="true"></i></span>
          <span><i class="fa fa-star" aria-hidden="true"></i></span>
          <span><i class="fa fa-star" aria-hidden="true"></i></span>
          <span><i class="fa fa-star-o" aria-hidden="true"></i></span>
          <span><i class="fa fa-star-o" aria-hidden="true"></i></span>
        </div>

        @if($product->getPrice() > 0)
          <div class="price">{{ $product->getPrice() }}</div>
        @endif

        <div class="add-to-collection">
          @if(auth()->check())
            <button onclick="toggleRequest('{{ $product->getID() }}');" id="product-requester">
          @else
            <button onclick="top.location.href='{{ route('admin.login') }}'" id="product-requester">
          @endif
            @if(isset($inRequest) && $inRequest)
              <i class="fa fa-minus" aria-hidden="true"></i>
              Remove from Request
            @else
              <i class="fa fa-plus" aria-hidden="true"></i>
              Add to Request
            @endif
          </button>
        </div>
        <ul class="tabs" data-tabs id="detail-tabs">
          <li class="tabs-title is-active">
            <a href="#panel1" aria-selected="true">Overview</a>
          </li>
          <li class="tabs-title">
            <a href="#panel2">Vendor</a>
          </li>
          <li class="tabs-title">
            <a href="#reviews">Reviews</a>
          </li>
        </ul>
        <div class="tabs-content" data-tabs-content="detail-tabs">
          <div class="tabs-panel is-active" id="panel1">
            <p class="description">{{ $product->getDescription() }}</p>

            <ul class="descriptionlist">
              <li><span class="lable">Brand:     </span> {{ $product->getBrand() }}</li>
              <li><span class="lable">Range:     </span> {{ $product->getRange() }}</li>
              <li><span class="lable">Code:      </span> {{ $product->getCode() }}</li>
              <li><span class="lable">Dimensions: </span> {{ $product->getDimensions() }}</li>
              <li><span class="lable">Green mark:</span> {{ $product->getGreenMark() == 1 ? 'Yes' : 'No' }}</li>
            </ul>

            <ul class="inline-list tags">
              @foreach ($product->tags as $tag)
                <li>
                  <span class="label secondary">
                    <a href="{{ route('archive', ['q' => $tag->getPhrase()]) }}">
                      {{ $tag->getPhrase() }}
                    </a>
                  </span>
                </li>
              @endforeach
            </ul>
          </div>
          <div class="tabs-panel" id="panel2">
            <div class="row">
              <div class="column">
                <div class="row">
                  <div class="column">
                    <div id="vendorLogo"> <img src="{{ asset($product->supplier->getLogo()) }}"> </div>
                    <ul id="vendor-details">
                      <li>Name: <span class="detail">{{ $product->supplier->getName() }}</span></li>
                      <li>Address: <span class="detail">{{ $product->supplier->getAddress() }}</span></li>
                      <li>Website: <span class="detail"><a href="{{ $product->supplier->getWebsite() }}" target="_blank">{{ $product->supplier->getWebsite() }}</a></span></li>
                      @foreach ($product->supplier->contacts as $contact)
                        <li>Sales Person: <span class="detail">{{ $contact->getName() }}</span></li>
                        <li>Phone: <span class="detail">{{ $contact->getPhone() }}</span></li>
                        <li>Email: <span class="detail"><a href="mailto:{{ $contact->getEmail() }}">{{ $contact->getEmail() }}</a> </span></li>
                      @endforeach
                    </ul>
                  </div>
                </div>
                <div class="row">
                  <div class="column">
                    <ul id="vndactions" class="inline-list">
                      <li><a class="button"><i class="fa fa-envelope-o" aria-hidden="true"></i> <span>Request for Sample</span></a>
                      </li>
                      <li><a class="button"><i class="fa fa-search" aria-hidden="true"></i> <span>View all Products</span></a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="tabs-panel" id="reviews">
            <h5>Ratings &amp; Reviews of <strong>Leaves Living</strong></span></h5>
            <div class="submit-review"> <a data-open="writeReview" class="button">Write your review...</a> </div>
            <div class="reveal" id="writeReview" data-reveal>
              <h4>Leaves Living <span>by Tonin Casa</span></h4>
              <div id="clickToRate">
                <div class="rating"> <span><i class="fa fa-star-o" aria-hidden="true"></i></span> <span><i class="fa fa-star-o" aria-hidden="true"></i></span> <span><i class="fa fa-star-o" aria-hidden="true"></i></span> <span><i class="fa fa-star-o" aria-hidden="true"></i></span> <span><i class="fa fa-star-o" aria-hidden="true"></i></span> </div>
                <div class="labelRate"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Click to rate</div>
              </div>
              <form id="reviewWindow">
                <label> Review Title (optional)
                  <input type="text" placeholder="Type review title"="100"> </label>
                <label> Review Description
                  <textarea placeholder="None"></textarea>
                </label>
                <input class="button" type="submit" value="Submit review"> </form>
              <button class="close-button" data-close aria-label="Close modal" type="button"> <span aria-hidden="true">&times;</span> </button>
            </div>
            <section class="review-entries">
              <div class="row">
                <div class="large-3 columns">
                  <div class="star-rating"> <span><i class="fa fa-star" aria-hidden="true"></i></span> <span><i class="fa fa-star" aria-hidden="true"></i></span> <span><i class="fa fa-star-o" aria-hidden="true"></i></span> <span><i class="fa fa-star-o" aria-hidden="true"></i></span> <span><i class="fa fa-star-o" aria-hidden="true"></i></span> </div>
                  <div class="reviewer">Travis Fimmel</div>
                </div>
                <div class="large-9 columns">
                  <p>The space between life and death, that’s where we are the most alive</p>
                </div>
              </div>
            </section>
            <section class="review-entries">
              <div class="row">
                <div class="large-3 columns">
                  <div class="star-rating"> <span><i class="fa fa-star" aria-hidden="true"></i></span> <span><i class="fa fa-star" aria-hidden="true"></i></span> <span><i class="fa fa-star" aria-hidden="true"></i></span> <span><i class="fa fa-star" aria-hidden="true"></i></span> <span><i class="fa fa-star-o" aria-hidden="true"></i></span> </div>
                  <div class="reviewer">Katheryn Winnick</div>
                </div>
                <div class="large-9 columns">
                  <p>I can share your burden. I am small, but I have broad shoulders and I am not afraid</p>
                </div>
              </div>
            </section>
            <section class="review-entries">
              <div class="row">
                <div class="large-3 columns">
                  <div class="star-rating"> <span><i class="fa fa-star" aria-hidden="true"></i></span> <span><i class="fa fa-star" aria-hidden="true"></i></span> <span><i class="fa fa-star" aria-hidden="true"></i></span> <span><i class="fa fa-star-o" aria-hidden="true"></i></span> <span><i class="fa fa-star-o" aria-hidden="true"></i></span> </div>
                  <div class="reviewer">Clive Standen </div>
                </div>
                <div class="large-9 columns">
                  <p>He appeared to me. Here, in this room. He blessed me and made the sign of the cross. But he was silent, and his image faded almost as soon as I saw it.</p>
                </div>
              </div>
            </section>
          </div>
        </div>
      </section>
      <!-- detailsection -->
    </div>
  </div>

  @if(isset($supplierProducts) && !$supplierProducts->isEmpty())
    @include('creatives.widgets.list', ['sectionName' => 'More from '.$product->supplier->getName(), 'products' => $supplierProducts])
  @endif

  @if(isset($relatedProducts) && !$relatedProducts->isEmpty())
    @include('creatives.widgets.list', ['sectionName' => 'Related', 'products' => $relatedProducts])
  @endif
@endsection
