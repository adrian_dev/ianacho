@extends('welcome')
@section('title', 'Home')
@section('content')

@if(!empty($newProducts))
  <section id="newproducts" class="sections">
    <div class="row">
      <div class="column">
        <h2>New Products</h2>
      </div>
    </div>

    <div class="row small-up-2 medium-up-3 large-up-5" data-equalizer data-equalize-on="large">
      @foreach($newProducts as $product)
        @include('creatives.widgets.card', ['product' => $product, 'query' => '', 'replacement' => ''])
      @endforeach
    </div>
  </section>
  <!-- end new prod -->
@endif

<section id="vendorblock" class="sections">
  <div class="row">
    <div class="column">
      <h2>Suppliers</h2>
    </div>
  </div>
  <div class="row small-up-2 medium-up-3 large-up-5" data-equalizer data-equalize-on="large">
    @foreach ($activeSuppliers as $supplier)
      <div class="column">
        <div class="card">
          <div class="thumb">
            <a href="{{ route('archive', ['supplier[]' => $supplier->getID()]) }}">
              <img src="{{ asset($supplier->getLogo()) }}" alt="{{ strip_tags($supplier->getName()) }}">
            </a>
          </div>
          <div class="details" data-equalizer-watch>
            <a href="{{ route('archive', ['supplier[]' => $supplier->getID()]) }}">
              <div class="vendor-name">{{ $supplier->getName() }}</div>
            </a>
          </div>
        </div>
      </div>
    @endforeach
  </div>
</section>
@endsection
