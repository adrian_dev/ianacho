@extends('ipenny')
@section('title', 'Search')
@section('content')
  <div class="row expanded">
    @include('creatives.widgets.filters')
    <div class="large-9 columns">
      <div id="prod-grid" class="row small-up-2 medium-up-3 large-up-4 listed unlisted">
        @foreach ($products as $product)
          @include('creatives.widgets.card', ['product' => $product])
        @endforeach
      </div>
    </div>
  </div>
@endsection
