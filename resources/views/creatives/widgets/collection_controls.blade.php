@if (Auth::check())
  <div class="medium-6 columns">
    <div id="userblock">
      <ul>
        <li>
          <a href="javascript:void(0);" data-open="collectionModal" onclick="getMyProducts();getMyRequests();">
            <span>Collections </span>
            <i class="fa fa-clone" aria-hidden="true"></i>
          </a>
        </li>
        <li>
          <a href="{{ Auth::user()->level > 0 ? route('collection') : route('admin.dashboard') }}">
            <span>{{ Auth::user()->name }}</span>
            <i class="fa fa-user-o" aria-hidden="true"></i>
          </a>
        </li>
      </ul>
    </div>
  </div>

  @include('creatives.widgets.collection')
@endif
