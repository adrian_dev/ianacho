<div class="column" id="product-{{ $product->getID() }}">
  <div class="card">
    @include('creatives.widgets.buttons', ['id' => $product->getID(), 'favorite' => !$product->isFavorite()->get()->isEmpty()])

    <div class="thumb">
      @if (strtotime($product->created_at) > strtotime('-7 day'))
        <div class="newtag">NEW</div>
      @endif

      <a href="{{ route('detail', ['id' => $product->getID()]) }}">
        <img src="{{ asset($product->getThumbnail()) }}" alt="{{ strip_tags($product->getName()) }}">
      </a>
    </div>

    <div class="details">
      <div class="hiddenInListview" data-equalizer-watch>
        <div class="cards-title">
          <a href="{{ route('detail', ['id' => $product->getID()]) }}">
            {{ $product->getName() }}
          </a>
        </div>
        <div class="vendor-name">
          <a href="{{ route('archive', ['supplier[]' => $product->supplier->getID()]) }}">
            {{ $product->supplier->getName() }}
          </a>
        </div>
      </div>

      <div class="listview">
        <div class="product-name">
          <h1>
            {!! str_ireplace($query, $replacement, $product->getName()) !!}
            <span>by <i>{{ $product->supplier->getName() }}</i></span>
          </h1>
        </div>

        @include('creatives.widgets.rating')

        <div class="price">{{ $product->getPrice() }}</div>

        <p class="description">
          {{ $product->getDescription() }}
          @foreach ($product->tags as $tag)
            <span>{!! str_ireplace($query, $replacement, $tag->getPhrase()) !!}</span>
          @endforeach
        </p>

        <div class="add-to-collection">
          <button>
            <i class="fa fa-plus" aria-hidden="true"></i> Add to request
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
