<div class="large-3 columns">
  <form method="get" id="archive-filter" action="{{ route('archive') }}">
    <input type="hidden" name="q" value="{{ Request::has('q') ? Request::input('q') : '' }}" />
    <input type="hidden" name="sort" value="{{ Request::has('sort') ? Request::input('sort') : 'new' }}" id="sort-results" />
    <input type="submit" class="hide" />
    <aside id="sidebar">
      <h4>
        <span>Filter Results</span>
        <span id="toggler">
          <a href="javascript:void(0);">
            <i class="fa fa-chevron-down"></i>
          </a>
        </span>
      </h4>
      <div id="filters">
        <div class="filter">
          <h5>SUPPLIERS</h5>

          <ul style="max-height:300px;overflow-y:auto;">
            @foreach ($filters['suppliers'] as $supplier)
              <li>
                <input type="checkbox" name="supplier[]" id="supplier-{{ $supplier->getID() }}" value="{{ $supplier->getID() }}" {{ (Request::has('supplier') && in_array($supplier->getID(), Request::input('supplier'))) ? 'checked' : '' }} />
                <label for="supplier-{{ $supplier->getID() }}">{{ $supplier->getName() }}</label>
              </li>
            @endforeach
          </ul>
        </div>

        <div class="filter">
          @foreach ($filters['tags'] as $key => $tags)
            <h5>{{ strtoupper($key) }}</h5>

            <ul style="max-height:240px;overflow-y:auto;">
              @foreach ($tags as $tag)
                <li>
                  <input type="checkbox" id="tag-{{ $tag->getID() }}" name="tag[]" value="{{ $tag->getID() }}" {{ (Request::has('tag') && in_array($tag->getID(), Request::input('tag'))) ? 'checked' : '' }} />
                  <label for="tag-{{ $tag->getID() }}">{{ $tag->getPhrase() }}</label>
                </li>
              @endforeach
            </ul>
            <br />
          @endforeach
        </div>

        <div class="filter">
          <h5>GREEN MARK</h5>
          <ul>
            <li>
              <input id="green1" type="radio" name="green_mark" value="1" {{ Request::has('green_mark') && Request::input('green_mark') == 1 ? 'checked' : '' }} />
              <label for="green1">Yes</label>
            </li>
            <li>
              <input id="green2" type="radio" name="green_mark" value="0" {{ Request::has('green_mark') && Request::input('green_mark') == 0 ? 'checked' : '' }} />
              <label for="green2">No</label>
            </li>
            <li>
              <input id="green3" type="radio" name="green_mark" value="" {{ !Request::has('green_mark') ? 'checked' : '' }} />
              <label for="green3">Any</label>
            </li>
          </ul>
        </div>

        <div class="filter">
          <h5>LEAD TIME (weeks)</h5>

          <div class="row">
            <div class="small-6 columns">
              <div class="slider" data-slider data-initial-start="{{ Request::has('lead_time') ? Request::input('lead_time') : 12 }}" data-step="1" data-start="1" data-end="12">
                <span class="slider-handle"  data-slider-handle role="slider" tabindex="1" aria-controls="lead-time"></span>
                <span class="slider-fill" data-slider-fill></span>
              </div>
            </div>
            <div class="small-6 columns">
              <div class="input-group">
                <input type="text" id="lead-time" class="input-group-field" value="{{ Request::has('lead_time') ? Request::input('lead_time') : 12 }}" />
                <a href="javascript:void(0);" class="input-group-button button" onclick="$('#archive-filter').submit();">GO</a>
              </div>
            </div>
          </div>
        </div>

        <div class="filter">
          <h5>PRICE (SGD)</h5>

          <div class="row">
            <div class="columns">
              <div class="slider" data-slider data-initial-start="{{ Request::has('price_min') ? Request::input('price_min') : 0 }}" data-initial-end="{{ Request::has('price_max') ? Request::input('price_max') : 10000 }}" data-start="0" data-end="10000" data-step="100">
                <span class="slider-handle" data-slider-handle role="slider" tabindex="1" aria-controls="price-min"></span>
                <span class="slider-fill" data-slider-fill></span>
                <span class="slider-handle" data-slider-handle role="slider" tabindex="1" aria-controls="price-max"></span>
              </div>
            </div>
            <div class="columns small-5">
              <input type="text" name="price_min" id="price-min" value="{{ Request::has('lead_time') ? Request::input('lead_time') : 0 }}" />
            </div>
            <div class="columns small-7 end">
              <div class="input-group">
                <input type="text" name="price_max" id="price-max" class="input-group-field" value="{{ Request::has('price_max') ? Request::input('price_max') : 10000 }}" />
                <a href="javascript:void(0);" class="input-group-button button" onclick="$('#archive-filter').submit();">GO</a>
              </div>
            </div>
          </div>
        </div>
        <a href="{{ route('archive') }}" class="button expanded">RESET FILTERS</a>
      </div>
    </aside>
  </form>
</div>
<br />
<br />
<br />
