<div id="searchbar" class="srchbar">
  <div class="row medium-uncollapse large-collapse">
    <div class="medium-6 columns">
      <form id="mainform" method="get" action="{{ route('archive') }}">
        <input id="maininputform" type="text" name="q" value="{{ app('request')->input('q') }}" placeholder="Search through 200 + products in this library" autofocus="autofocus">
        <i class="fa fa-search" aria-hidden="true"></i>
      </form>
    </div>

    @if(auth()->check())
      @include('creatives.widgets.collection_controls')
    @else
      <div class="medium-6 columns">
        <div id="userblock">
          <ul>
            <li>
              <a href="{{ route('admin.login') }}">Login</a>
            </li>
            <li>
              <a href="{{ url('register') }}">
                Register
                <i class="fa fa-user-o" aria-hidden="true"></i>
              </a>
            </li>
          </ul>
        </div>
      </div>
    @endif
  </div>
</div>
