<div class="reveal"
     id="collectionModal"
     data-reveal data-animation-in="slide-in-right"
     data-animation-out="slide-out-right">
  <div id="favesect">
    <div class="row">
      <div class="column">
        <h2>Favorites</h2>
        <p class="lead">
          These are the products you added to your favorites.
          <br />
          <a href="{{ route('collection') }}">View all your favorites here...</a>
        </p>
      </div>
    </div>

    <div class="faveblock">
      <div id="collection-kit" class="row small-up-2 medium-up-3 large-up-4"></div>
    </div>
  </div>

  <div class="row"><div class="column"><hr /></div></div>

  <div id="requestsect">
    <div class="row">
      <div class="column">
        <h2>Vendor Requests</h2>
        <p class="lead">
          These are groups of products that you can send to the vendor.
          <a href="{{ route('creatives.requests') }}">View all your requests here...</a>
        </p>
      </div>
    </div>
    <div id="request-kit" class="row small-up-2 medium-up-3 large-up-4"></div>
  </div>
  <button class="close-button" data-close aria-label="Close modal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
