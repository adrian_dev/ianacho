<div class="hidden">
  <ul class="toolicons">
    <li class="quickview">
      <a href="{{ route('preview', ['id' => $id]) }}">
        <i class="fa fa-search-plus" aria-hidden="true"></i>
      </a>
    </li>
    <li class="favorites {{ $favorite ? 'on' : '' }}">
      <a href="javascript:void(0);" onclick="toggleFavorite(this);" data-id="{{ $id }}">
        <i class="fa fa-heart-o" aria-hidden="true"></i>
      </a>
    </li>
    <li class="collection">
      <a href="javascript:void(0);" data-id="{{ $id }}">
        <i class="fa fa-plus" aria-hidden="true"></i>
      </a>
    </li>
    <li class="unfavorite">
      <a href="javascript:void(0);" onclick="unFavorite({{ $id }});">
        <i class="fa fa-remove" aria-hidden="true"></i>
      </a>
    </li>
  </ul>
</div>
