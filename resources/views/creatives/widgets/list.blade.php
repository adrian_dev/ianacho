<br />
<section class="sections">
  <div class="row">
    <div class="column"><h2>{{ $sectionName }}</h2></div>
  </div>
  <div class="row small-up-2 medium-up-3 large-up-5" data-equalizer data-equalize-on="large">
    @if(isset($products))
      @foreach($products as $product)
        @include('creatives.widgets.card', [
          'product' => $product,
          'query' => '',
          'replacement' => ''])
      @endforeach
    @endif
  </div>
</section>
