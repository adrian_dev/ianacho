<header id="topheader">
  <div class="row expanded">
    <div class="medium-4 columns">
      <div class="mainmenu">
        <a id="togglenav" data-toggle="maindropmenu">
          <span class="menutext">MENU</span>
          <span class="menuicon">
            <span class="navlines"></span>
          </span>
        </a>

        <div class="dropdown-pane bottom"
          id="maindropmenu"
          data-dropdown
          data-v-offset="10"
          data-h-offset="0"
          data-close-on-click="true">

          <ul class="lists">
            <li><a href="{{ route('home') }}">Home</a></li>
            <li><a href="{{ route('archive') }}">Products</a></li>
            <!--li><a href="vendors.html">Vendors</a></li-->
          </ul>

          <hr />

          <ul class="lists">
            @if(auth()->check())
              @if (auth()->user()->level == 0)
                <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
              @endif
              <li><a href="{{ route('collection') }}">My Favorites</a></li>
              <li><a href="{{ route('creatives.requests') }}">My Requests</a></li>
              <li><a href="{{ route('creatives.logout') }}">Logout</a></li>
            @else
              <li><a href="{{ route('creatives.login') }}">Login</a></li>
            @endif
          </ul>
        </div>
      </div>
    </div>

    <div class="medium-4 columns">
      <div class="ipennylogo">
        <a href="{{ route('home') }}">
          <img src="{{ asset('img/ipenny-text.svg') }}" />
        </a>
      </div>
    </div>

    <div class="medium-4 columns">
      <div class="mmoserlogo">
        <img src="{{ asset('img/mmoser-logo.svg') }}" />
      </div>
    </div>
  </div>
</header>
