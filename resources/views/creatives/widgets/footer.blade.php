<div class="row">
  <div class="large-7 columns">
    <div class="ipennylogo">
      <img src="{{ asset('img/ipenny-logo-holes.svg') }}">
    </div>

    <div class="ipennyinfo">
      <img class="ipenny-small-text" src="{{ asset('img/ipenny-small-text.png') }}">
      <p>The Materials Library is a place where you can browse for product brochures Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>

      <img src="{{ asset('img/sg-map.gif') }}">
    </div>
  </div>

  <div class="large-5 columns">
    <div class="footer-navigation">
      <div class="row">
        <div class="large-6 columns">
          <h6>NAVIGATION</h6>
          <ul>
            <li><a href="#">Home</a>
            </li>
            <li><a href="#">New Products</a>
            </li>
            <li><a href="#">Vendors</a>
            </li>
            <li><a href="#">Favorites</a>
            </li>
            <li><a href="#">Collection</a>
            </li>
          </ul>
        </div>

        <div class="large-6 columns">
          <h6>Learn more</h6>
          <ul>
            <li><a href="#">Knowledge Center</a>
            </li>
            <li><a href="#">M Moser Learning</a>
            </li>
            <li><a href="#">MarComms Page</a>
            </li>
            <li><a href="#">Design Page</a>
            </li>
            <li><a href="#">Technology Page</a>
            </li>
            <li><a href="#">Solutions Page</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="footer-bottom row">
  <div class="column">
    <hr>
    <p>&copy; M Moser Associates Singapore</p>
    <p><a href="http://www.mmoser.com">mmoser.com</a>
    </p>
  </div>
</div>
