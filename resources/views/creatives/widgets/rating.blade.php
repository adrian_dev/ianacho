<div class="star-rating">
  <span><i class="fa fa-star" aria-hidden="true"></i></span>
  <span><i class="fa fa-star" aria-hidden="true"></i></span>
  <span><i class="fa fa-star" aria-hidden="true"></i></span>
  <span><i class="fa fa-star-o" aria-hidden="true"></i></span>
  <span><i class="fa fa-star-o" aria-hidden="true"></i></span>
</div>
