<div class="large">
  <div class="large-4 columns">
    <div class="prodimage">
      <img src="{{ asset($product->getImage()) }}" width="100%" />
      <div class="addtofavorite">
        <a title="Add to Favorites" class="favbutton unfav"><span></span></a>
      </div>
    </div>
  </div>
  <div class="large-8 columns">
    <h4>
      {{ $product->getName() }}
      <span>by {{ $product->supplier->getName() }}</span>
    </h4>
    @include('creatives.widgets.rating')
    <div class="price">{{ $product->getPrice() }}</div>
    <p class="description">{{ $product->getDescription() }}</p>
    <div class="add-to-collection">
      <button> <i class="fa fa-plus" aria-hidden="true"></i> Add to request</button>
    </div>
    <ul class="descriptionlist">
      <li><span class="lable">Brand:     </span> {{ $product->getBrand() }}</li>
      <li><span class="lable">Code:      </span> {{ $product->getCode() }}</li>
      <li><span class="lable">Green mark:</span> {{ $product->getGreenMark() ? 'Yes' : 'No' }}</li>
      <li><span class="lable">Dimensions:</span> {{ $product->getDimensions() }}</li>
      <li><span class="lable">Range:     </span> {{ $product->getRange() }}</li>
    </ul>
    <p><a href="single.html">View more...</a> </p>
  </div>
</div>
