<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*temporary routes before cleanup*/
Route::get('/', ['as' => 'home', 'uses' => 'LibraryController@index']);
Route::get('search', ['as' => 'search', 'uses' => 'SearchController@index']);
Route::get('collection', ['as' => 'collection', 'uses' => 'LibraryController@collection']);
Route::get('suppliers', ['as' => 'suppliers', 'uses' => 'LibraryController@suppliers']);
Route::get('requests', ['as' => 'requests', 'uses' => 'LibraryController@requests']);
Route::get("catalog/{id}", ['as' => 'catalog', 'uses' => 'LibraryController@catalog']);
Route::get("library/product/{id}", ["as" => "library.product", "uses" => "LibraryController@product"]);

Route::get('archive', ['as' => 'archive', 'uses' => 'CreativeController@archive'])->middleware('local_access');
Route::get('product/{id}', ['as' => 'detail', 'uses' => 'CreativeController@product'])->middleware('local_access');
Route::get('preview/{id}', ['as' => 'preview', 'uses' => 'CreativeController@preview'])->middleware('local_access');
Route::get('login', ['as' => 'creatives.login', 'uses' => 'CreativeController@login'])->middleware('local_access');
Route::get('logout', ['as' => 'creatives.logout', 'uses' => 'CreativeController@logout'])->middleware('local_access');
Route::post('auth', ['as' => 'creatives.auth', 'uses' => 'CreativeController@authenticate']);

Route::get('register', function() {
  return view('admin.register');
});

Route::post('registration', ['as' => 'registration', 'uses' => 'AdminController@registration']);

Route::group(['prefix' => 'admin'], function() {
  Route::get('login', ['as' => 'admin.login', 'uses' => 'AdminController@login']);
  Route::post('authenticate', ['as' => 'admin.auth', 'uses' => 'AdminController@authenticate']);
  Route::get('logout', ['as' => 'admin.logout', 'uses' => 'AdminController@logout']);

  Route::group(['middleware' => 'admin'], function() {
    Route::any('/', ['as' => 'admin.dashboard', 'uses' => 'AdminController@index']);
    Route::get('edit/{id}', ['as' => 'admin.edit', 'uses' => 'AdminController@edit'])->where('id', '[0-9]+');
    Route::get('view/{id}', ['as' => 'admin.view', 'uses' => 'AdminController@view'])->where('id', '[0-9]+');
    Route::get('add', ['as' => 'admin.add', 'uses' => 'AdminController@add']);
    Route::post('save', ['as' => 'admin.save', 'uses' => 'AdminController@save']);
    Route::post('update', ['as' => 'admin.update', 'uses' => 'AdminController@update']);
    Route::get('email/{id}', ['as' => 'admin.email', 'uses' => 'AdminController@email'])->where('id', '[0-9]+');
    Route::get('send/{id}', ['as' => 'admin.send', 'uses' => 'AdminController@send']);
    Route::get('tag', ['as' => 'admin.tag', 'uses' => 'TagController@index']);
    Route::post('tag/add', ['as' => 'admin.tag.add', 'uses' => 'TagController@set']);
    Route::post('tag/remove', ['as' => 'admin.tag.remove', 'uses' => 'TagController@remove']);
    Route::post('tag/update', ['as' => 'admin.tag.update', 'uses' => 'TagController@update']);
    Route::get('logo/form/{id}', ['as' => 'admin.logo.form', 'uses' => 'AdminController@logoForm']);
    Route::post('logo/form/save', ['as' => 'admin.logo.save', 'uses' => 'AdminController@logoSave']);
    Route::get('product/{id}', ['as' => 'admin.product', 'uses' => 'AdminController@productNew']);
    Route::post('generate-password', ['as' => 'admin.generate.password', 'uses' => 'AdminController@requestPassword']);
    Route::get('product/{id}/details', ['as' => 'admin.product.details', 'uses' => 'ProductController@preview']);
    route::get('deactivate/{id}', ['as' => 'admin.desupplier', 'uses' => 'AdminController@deactivate']);
    route::get("supplier", ["as" => "admin.getsupplier", "uses" => "AdminController@get"]);
    route::post("supplier", ["as" => "admin.setsupplier", "uses" => "AdminController@set"]);
  });
});

Route::group(['prefix' => 'supplier'], function() {
  // Route::get('login', ['as' => 'login', 'uses' => 'SupplierController@login']);
  Route::get('login', function() { return view("maintenance"); });
  Route::post('authorize', ['as' => 'authorize', 'uses' => 'SupplierController@authenticate']);
  Route::get('logout', ['as' => 'logout', 'uses' => 'SupplierController@logout']);

  Route::group(['middleware' => 'supplier'], function() {
    Route::any('/', ['as' => 'profile', 'uses' => 'SupplierController@index']);
    Route::get('edit', ['as' => 'edit', 'uses' => 'SupplierController@edit']);
    Route::get('update-info', ['as' => 'update.info', 'uses' => 'SupplierController@touch']);
    Route::get('new', ['as' => 'new', 'uses' => 'SupplierController@add']);
    Route::post('save', ['as' => 'save', 'uses' => 'SupplierController@set']);
    Route::post('update', ['as' => 'update', 'uses' => 'SupplierController@update']);
    Route::get('view/{id}', ['as' => 'view', 'uses' => 'SupplierController@get']);
    Route::get('delete/{id}', ['as' => 'delete', 'uses' => 'SupplierController@remove']);
    Route::post('upload', ['as' => 'upload', 'uses' => 'SupplierController@upload']);
    Route::get('product/{id}', ['as' => 'product', 'uses' => 'SupplierController@productView']);
    Route::get('product/edit/{id}', ['as' => 'product.edit', 'uses' => 'SupplierController@productEdit']);
    Route::post('update-logo', ['as' => 'logo.update', 'uses' => 'SupplierController@updateLogo']);
    Route::get('requests', ['as' => 'supplier.requests', 'uses' => 'RequestController@supplierIndex']);

    Route::get('product', [
      'as' => 'supplier.product',
      'uses' => 'ProductController@add'
    ]);

    route::post("product/set", ["as" => "product.add", "uses" => "ProductController@set"]);
	  route::get("product/get/{id}", ["as" => "product.get", "uses" => "ProductController@get"]);

    route::get("product/delete/{id}", ["as" => "product.delete", "uses" => "ProductController@remove"]);
    route::get("contact/delete/{id}", ["as" => "contact.delete", "uses" => "SupplierController@removeContact"]);
  });
});

Route::get('maker', function() {
    return Hash::make('1111');
});

Route::get('test', function() {
  $suppliers = App\Supplier::all();
  return view('admin.test', ['suppliers' => $suppliers]);
});
Route::post('test',function(Illuminate\Http\Request $request) {
  $supplier = App\Supplier::find($request->id);

  $username = strtolower(str_replace(' ', '_', $supplier->getName()));
  $rawpswrd = $username;
  $password = Hash::make($rawpswrd);

  $supplier->setUsername($username);
  $supplier->setPassword($password);
  $supplier->save();

  return ['username' => $username, 'password' => $rawpswrd];
});

route::group(['prefix' => 'api'], function() {
  Route::group(['middleware' => 'admin', 'prefix' => 'favorites'], function() {
    Route::get('/', ['as' => 'favorites', 'uses' => 'FavoritesController@index']);
    Route::get('set/{id}', ['as' => 'favorites.add', 'uses' => 'FavoritesController@set']);
    Route::get('unset/{id}', ['as' => 'favorites.delete', 'uses' => 'FavoritesController@delete']);
    Route::get('products', ['uses' => 'FavoritesController@products']);
  });
  Route::group(['middleware' => 'admin', 'prefix' => 'requests'], function() {
    Route::get('products', ['as' => 'my-requests', 'uses' => 'RequestController@products']);
    Route::get('set/{id}', ['as' => 'requests.add', 'uses' => 'RequestController@set']);
    Route::get('unset/{id}', ['as' => 'requests.delete', 'uses' => 'RequestController@remove']);
  });

  route::group(["middleware" => "supplier", "prefix" => "supplier"], function() {
    route::get("brands", ["as" => "request.brands", "uses" => "RequestController@getBrands"]);
    route::get("categories", ["as" => "request.categories", "uses" => "RequestController@getCategories"]);
  });
});

/*Upload API*/
route::group(['prefix' => 'upload'], function() {
  route::group(['middleware' => 'supplier', 'prefix' => 'product'], function() {
    route::post('range', ['as' => 'upload.range', 'uses' => 'UploadController@range']);
    route::post('gallery', ['as' => 'upload.gallery', 'uses' => 'UploadController@gallery']);
  });
});

route::get('update-totals', function() {
  $suppliers = App\Supplier::where("status", 1)->get();

  if (!$suppliers->isEmpty()) {
    foreach ($suppliers as $supplier) {
      echo $result = App\SupplierProduct::where("supplier_id", $supplier->getID())
        ->where("status", 1)
        ->count();
      $supplier->setTotalProducts($result);
      $supplier->save();
    }
  }
});

route::get("build-indexes", "SearchController@build");

route::get('find-ranges',function(Illuminate\Http\Request $request) {
  $result = App\ProductRange::whereIn("id", json_decode($request->ids))
    ->get(["id", "name", "thumbnail"])
    ->toArray();

  return $result;
});

Route::any("finder", "FindController@search");
